<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Payment Gateway for eCommerce | PyThru</title>	
	<meta name="description" content="Make smooth online payments possible on your eCommerce or marketplace platforms with PyThru payment gateway and payouts.">
	<meta name="keywords" content="Payment Gateway for eCommerce, Payouts, online payments, payment processing, Payment links, Payment Pages, Payment Button">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/solutions.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="hero">
						<div class="content">
							<h1 class="c-h1">Best Payments Solution <br class="hidden-xs">for <span> eCommerce</span></h1>
							<p class="pt10 pb30">Powerpack finance processing for your online store with <br class="hidden-xs">PyThru. Let your eCommerce business grow…</p>
						</div> 
						<?php include 'include/common-signup.php';?>
					</div>
				</div>
				<div class="col-md-offset-1 col-md-6">
					<div class="hero-graphic hidden-sm hidden-xs">
						<img src="img/ecommerce/hero.svg" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-bottom-padding">
		<div class="container">
			<div class="row">
				<div class="text-center ff-title">
					<h2><b>Best</b> eCommerce Features with PyThru</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<div class="ff">
						<img src="img/ecommerce/collect-payment.svg">
						<h3>Collect Payments</h3>
						<p>Different payment gateway methods to accept online payments from Customers</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="ff">
						<img src="img/ecommerce/make-payout.svg">
						<h3>Make Payouts</h3>
						<p>Make timely & automated individual and bulk payments to vendors, third parties, etc.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="ff">
						<img src="img/ecommerce/business-banking.svg">
						<h3>Business Banking</h3>
						<p>Get Current account, enable Accounting & Bookkeeping, generate eWay Bill, etc</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row point-pad">
				<div class="left-right-section">
					<div class="col-md-6 col-sm-6">
						<div class="lrs-img">
							<img src="img/ecommerce/dashboard.svg" class="img-responsive">
						</div>
					</div>
					<div class="col-md-5 col-sm-6">
						<div class="lrs-pad m-center">
							<div>
								<span class="com-span">Data Overview</span>
								<h2 class="c-h2">Resourceful Merchant Dashboard</h2>	
								<p class="com-p pt10">Get real-time Analytics and Reports for better business decisions; and Data and Functions for easy operations.</p>	
							</div>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="ff-p-bg">
			<div class="container">
				<div class="row sec-padding">
					<div class="text-center ff-title1">
						<h2><b>Pricing</b> Plans</h2>
					</div>
					<div class="fp-main">
						<div class="col-md-4 col-sm-6">
							<div class="fp">
								<div class="fp-card b1">
									<div class="fp-detail">
										<div class="col-md-4">
											<b>2.5%</b>
										</div>
										<div class="col-md-8">
											<h4>Standard Pricing</h4>
											<p>Credit & Deibt card, UPI, wallets, Net banking & QR</p>
										</div>
										<div class="col-md-offset-2 col-md-9">
											<hr class="fp-line">
										</div>
										<div class="col-md-offset-2 col-md-4 col-xs-4">
											<div class="fp-price">
												<b>₹ 0.00</b>
												<p>Setup fees</p>
											</div>
										</div>
										<div class="col-md-6 col-xs-8">
											<div class="fp-price">
												<b>₹ 0.00</b>
												<p>No annual maintainance</p>
											</div>
										</div>
									</div>
								</div>	
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="fp">
								<div class="fp-card b2">
									<div class="fp-detail">
										<div class="col-md-4">
											<b>Talk</b>
											<span>with us</span>
										</div>
										<div class="col-md-8">
											<h4>Business Plan</h4>
											<p>To discuss the Customized Pricing </p>
										</div>
										<div class="col-md-offset-2 col-md-9">
											<hr class="fp-line">
										</div>
										<div class="fp-btn">
											<a href="">Contact Us</a>
										</div>
									</div>
								</div>	
							</div>
						</div>
						<div class="col-md-4 col-sm-12">
							<div class="fp-content">
								<h3>Best services with <br class="hidden-xs">two different pricing plans </h3>
								<p>Discuss yours as per <br class="hidden-xs">your business requirements</p>
								<div class="fp-btn1">
									<a href="">Know More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="text-center ff-title pb20">
					<h2><b>End-to-End </b> eCommerce Payments Solution</h2>
				</div>
			</div>
			<div class="row">
				<div class="tick">
					<div class="col-md-offset-2 col-md-3 col-sm-4">
						<h4>Easy Refunds</h4>
					</div>
					<div class="col-md-3 col-sm-4">
						<h4>Fast Settlements</h4>
					</div>
					<div class="col-md-3 col-sm-4">
						<h4>100+ Payment Modes</h4>
					</div>
					<div class="col-md-offset-3 col-md-3 col-sm-offset-2 col-sm-4">
						<h4>Multi-Currency Support</h4>
					</div>
					<div class="col-md-3 col-sm-4">
						<h4>Developer Friendly</h4>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row bg-black">
				<div class="col-md-2 hidden-xs">
					<div class="top-img">
						<img src="img/ecommerce/lock.svg" class="img-responsive">
					</div>
				</div>
				<div class="col-md-8">
					<div class="ss">
						<h2>Secure and Protected</h2>
						<p>PyThru safety and security features <br class="hidden-xs">protect you and your clients</p>
					</div>
					<div class="ss-point">
						<div class="col-md-offset-2 col-md-4 col-sm-6">
							<h4>PCI DSS Compliant</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Encryption & SSL</h4>
						</div>
						<div class="col-md-offset-2 col-md-4 col-sm-6">
							<h4>Risk Verification</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Fraud Prevention</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-offset-1 col-md-4 col-sm-5">
	            	<div class="qute m-center">
	            		<img src="img/ecommerce/qute.svg">
	            		<h2>This is what our <br class="hidden-xs">Merchants says...</h2>
	            	</div>
	            </div>
	            <div class="col-md-6 col-sm-7">
	                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	                    <!-- Wrapper for slides -->
	                    <div class="carousel-inner">
	                        <div class="item active">
	                            <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="img/home/man.svg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                    	<div class="content">
                                    		<h3>PyThru is the Best Decision</h3>
	                                    	<p>It is amazing to have a customized checkout in our brand color, name, and logo. The payment gateway with PyThru has been the best decision for our business.</p>
	                                        <h4>Sonu Kumar</h4>
	                                        <span>Doon Cars</span>
	                                    </div>
                                    </div>	                               
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="img/home/man.svg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                    	<div class="content">
                                    		<h3>Strongly Recommend</h3>
	                                    	<p>Provides lots of convenience and ease in all segments from onboarding to settlements. The integration part was smooth and effortless with no issues. I strongly recommend PyThru for your business payments.</p>
	                                        <h4>Sawan Kumar</h4>
											<span>Etnasasta</span>
	                                    </div>
                                    </div>	                               
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="img/home/man.svg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                    	<div class="content">
                                    		<h3>High Success Rate</h3>
	                                    	<p>Accepting and making payments was a task but the payment gateway and Payouts with PyThru have enhanced our business operations. It provides a very high transaction success rate.</p>
	                                        <h4>Sharwan Kumar</h4>
	                                        <span>The Various Store</span>
	                                    </div>
                                    </div>	                               
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="img/home/man.svg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                    	<div class="content">
                                    		<h3>Easy Integration</h3>
	                                    	<p>The PyThru Dashboard helps us with analytics and various functions including checking the transactions, settlements, etc. Issuing refunds is also so convenient. The checkout experience is smooth with easy integration.</p>
	                                    	<br>
	                                        <h4>Bikram Biswas</h4>
	                                        <span>Velvery Private Limited</span>
	                                    </div>
                                    </div>	                               
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-md-offset-1 col-md-5">
		                <div class="controls testimonial_control pull-right">
		                    <a class="left bi bi-chevron-left btn-default testimonial_btn fw900" href="#carousel-example-generic" data-slide="prev"></a>

		                    <a class="right bi bi-chevron-right btn-default testimonial_btn fw900" href="#carousel-example-generic" data-slide="next"></a>
		                </div>
		            </div>
	            </div>
	        </div>
	    </div>
	</section>

	<section class="sec-padding">
	    <div class="container">
	        <div class="row">
	        	<div class="ff-box-main">
	        		<div class="col-md-offset-1 col-md-5 col-sm-6">
	        			<div class="ff-box">
	        				<div class="ff-box-img">
		        				<img src="img/ecommerce/red-circle.svg">
		        				<h3>Risk Management <br class="hidden-xs">software</h3>
	        				</div>
	        				view product
	        			</div>
	        		</div>
	        		<div class="col-md-5 col-sm-6">
	        			<div class="ff-box">
	        				<div class="ff-box-img">
		        				<img src="img/ecommerce/blue-circle.svg">
		        				<h3>Risk Management <br class="hidden-xs">software</h3>
	        				</div>
	        				view product
	        			</div>
	        		</div>
	        	</div>
	        </div>
	    </div>
	</section>

	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>

	
</body>
</html>