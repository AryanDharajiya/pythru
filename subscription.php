<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

    <title>PyThru Subscriptions | Best Recurring Billing Solution in India</title> 
    <meta name="description" content="PyThru Subscriptions for the automatic recurring billing and payments in India.">
    <meta name="keywords" content="Subscription Payments, Recurring Payments, Recurring Billing, PyThru Subscriptions, Subscription Payments Solution, Recurring Billing Platform in India">

    <?php include 'include/css.php';?>  
	<link rel="stylesheet" href="css/subscription.css">
</head>
<body>

	<?php include 'include/header.php';?>

    <div class="bg-c">
        <div class="bg">
            <div class="container sec-padding">
                <div class="row sec-padding">
                    <div class="col-md-5">
                        <div class="hero">
                            <div class="content">
                                <h1 class="c-h1">PyThru <span>Subscription </span>Payments for your Business</h1>
                                <p class="pt10 pb20 content-p">Accept automatic Recurring Payments with PyThru Subscription Plans and provides an uninterrupted experience to your customers.</p>
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-offset-1 col-md-6">
                        <div class="hero-graphic hidden-sm hidden-xs">
                            <img src="img/subscription/hero.svg" class="img-responsive">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="why-us why-us-pad">
                            <div class="col-md-6">
                                <div class="w-content">
                                    <h3>Get Started with PyThru Subscriptions</h3>
                                    <p>Accept online payments for your Subscription-based business with this modern and advanced payment technology.</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="w-btn">
                                    <a href="">
                                        <span>Sign up</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="container sec-padding">
            <div class="row mpt30p">
                <div class="col-md-4 col-sm-4">
                    <div class="rb">
                        <img src="img/subscription/create.svg">
                        <h3>Create</h3>
                        <p>Generate a custom Subscription Plan through your Dashboard or API</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="rb">
                        <img src="img/subscription/send.svg">
                        <h3>Send</h3>
                        <p>Share and notify the customer about the Subscription Plan with the Link</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="rb">
                        <img src="img/subscription/accept.svg">
                        <h3>Accept</h3>
                        <p>After the authentication, start accepting the recurring payments</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="sec-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="lrs-img">
                        <img src="img/subscription/features.svg" class="img-responsive">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="lrs-pad">
                        <div>
                            <span class="com-span">PyThru for U</span>
                            <h2 class="c-h2">Set the Recurring Plans for your Customers</h2>   
                            <p class="com-p pt10">Opt for the plan suitable for your subscription requirements. PyThru Subscriptions require no coding and provides webhook support and easy integration.</p>   
                        </div>
                        <div class="f-desc">
                            <h3>Static (Fixed)</h3>
                            <p>To charge a recurring fixed amount after a specific period </p>

                            <h3>Variable</h3>
                            <p>Charge a usage-based amount on the next billing period</p>

                            <h3>Combined</h3>
                            <p>A mixed plan with a fixed amount as well as a variable amount</p>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </section>

    <section class="sec-ct-padding">
        <div class="bg1">
            <div class="bg-black">
                <div class="container sec-padding">
                    <div class="row"> 
                        <div class="col-md-3 col-sm-12">
                            <div class="s3c">
                                <img src="img/invoice/dot-frame.svg" class="hidden-xs hidden-sm">
                                <h2>3 Major <br class="hidden-xs hidden-sm"> Features of  <br class="hidden-xs hidden-sm">PyThru Subscription</h2>
                            </div>
                        </div>  
                        <div class="col-md-9">
                            <div class="col-md-4 col-sm-4">
                                <div class="s3-content">
                                    <img src="img/invoice/gst-complaint.svg">
                                    <h3>Offer Trial</h3>
                                    <p>Facilitate the trial period to the customers & automatically charge them when the trial ends</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="s3-content">
                                    <img src="img/invoice/customizes-invoice.svg">
                                    <h3>Charges</h3>
                                    <p>Charge a one-time fee at the start of the subscription. Add extra charges for extra services use.</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="s3-content">
                                    <img src="img/invoice/multi-currency.svg">
                                    <h3>Track and Analyse</h3>
                                    <p>Dashboard analysis and tracking of the customers with respective subscription plans via API</p>
                                </div>
                            </div>
                        </div>                    
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row sec-cb-pad">
                    <div class="col-md-5">
                        <div class="tc">
                            <h2><span>Connect</span> with <br class="hidden-xs">our team</h2>
                        </div>
                    </div>
                    <div class="col-md-7 pt30">
                        <div class="col-md-6">
                            <div class="tc-input">
                                <input type="email" name="email" placeholder="Enter email">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="tc-btn">
                                <a href="">Send mail</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </section>

    <section class="accordion-section clearfix mt-3 sec-padding" aria-label="Question Accordions">
      <div class="container">   
          <div class="col-md-offset-1 col-md-10">  
              <h2 class="text-center faq-title">FAQs</h2>
              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                  <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
                    <h3 class="panel-title">
                      <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
                        What is a PyThru Subscription?
                      </a>
                    </h3>
                  </div>
                  <div id="collapse0" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading0" aria-expanded="true">
                    <div class="panel-body px-3 mb-4">
                      <p>When an option is provided to the customer to give the authorization to deduct the billing amount automatically for the continuation of the services or product usage, is called the Subscription.</p>
                      <p>With PyThru you can provide this facility of recurring payments and send the notifications for all the related events to the customers with webhooks, also, manage the same with the APIs via PyThru Dashboard.</p>
                      </ul>
                    </div>
                  </div>
                </div>
                
                <div class="panel panel-default">
                  <div class="panel-heading p-3 mb-3" role="tab" id="heading1">
                    <h3 class="panel-title">
                      <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                        Which Payment Methods are supported for Subscription?
                      </a>
                    </h3>
                  </div>
                  <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                    <div class="panel-body px-3 mb-4">
                      <p>Credit Cards, Debit Cards, UPI, NACH or E-mandate, and UPI AutoPay</p>
                      <p><b>Credit cards:</b> MasterCard and Visa</p>
                      <p><b>Debit cards:</b> Mastercard and Visa </p>
                      <p><b>NACH or E-mandate:</b> This can be registered with the Netbanking or Debit cards. </p>
                    </div>
                  </div>
                </div>
                
                <div class="panel panel-default">
                  <div class="panel-heading p-3 mb-3" role="tab" id="heading2">
                    <h3 class="panel-title">
                      <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                        Can I make the changes to the Subscription?
                      </a>
                    </h3>
                  </div>
                  <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                    <div class="panel-body px-3 mb-4">
                      <p>Yes, you can cancel or make changes to the Subscription Plans. </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>      
      </div>
    </section>

    <section class="sec-padding"></section>   

    <?php include 'include/footer.php';?>

    <?php include 'include/js.php';?>

</body>
</html>