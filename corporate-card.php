<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Get Corporate Credit Cards | PyThru Business Banking</title>	
	<meta name="description" content="Get a smart credit card for your business with instant approval, flexible credit limits, lucrative benefits, and smart repayment options. Manage business expenses, payouts, banking, and accounting seamlessly.">
	<meta name="keywords" content="Corporate Card, Business Credit Card">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/banking.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="cc-bg">
		<div class="sec-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						<div class="hero-cc sec-padding">
							<h1>The Business <span>Credit Card</span> with Pythru</h1>
							<p>Easiest and smartest credit card for your business <br class="hidden-xs">expenses with smooth banking and <br class="hidden-xs">accounting management</p>
						</div>
					</div>
					<div class="col-md-7">
						<div class="hero-graphic hidden-sm hidden-xs">
							<img src="img/corporate-card/hero.jpg" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-bottom-padding">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<div class="km m-tc">
						<h2>Our Smart Credit Card <br class="hidden-xs">with Smart Features</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="gf">
						<div class="gfi1">
							<img src="img/corporate-card/get-started.svg">
						</div>
						<div class="gfc gfc1">
							<h3>Get started in minutes with the  virtual card</h3>
						</div>
					</div>
				</div>	
				<div class="col-md-4 col-sm-6">
					<div class="gf">
						<div class="gfi1">
							<img src="img/corporate-card/no-requre.svg">
						</div>
						<div class="gfc gfc1">
							<h3>No personal guarantee or collateral required</h3>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="gf">
						<div class="gfi1">
							<img src="img/corporate-card/customize.svg">
						</div>
						<div class="gfc gfc1">
							<h3>Accepted VISA card universally</h3>
						</div>
					</div>
				</div>	
				<div class="col-md-4 col-sm-6">
					<div class="gf">
						<div class="gfi1">
							<img src="img/corporate-card/credit-limit.svg">
						</div>
						<div class="gfc gfc1">
							<h3>Get credit limit growth with your business growth</h3>
						</div>
					</div>
				</div>		
				<div class="col-md-4 col-sm-6">
					<div class="gf">
						<div class="gfi1">
							<img src="img/corporate-card/rewards.svg">
						</div>
						<div class="gfc gfc1">
							<h3>With simplified rewards and cash back advantages</h3>
						</div>
					</div>
				</div>		
				<div class="col-md-4 col-sm-6">
					<div class="gf">
						<div class="gfi1">
							<img src="img/corporate-card/easy-hasslefree.svg">
						</div>
						<div class="gfc gfc1">
							<h3>Easy and hassle free options for the repayment </h3>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</section>

	<section class=" sec-padding">      
        <div class="bg-black">
            <div class="container sec-padding">
                <div class="row"> 
                    <div class="col-md-3 col-sm-12">
                        <div class="s3c">
                            <img src="img/payroll/dot-frame.svg" class="hidden-xs hidden-sm">
                            <h2 class="m-tc">Get Started!</h2>
                        </div>
                    </div>  
                    <div class="col-md-9">
                        <div class="col-md-4 col-sm-4">
                            <div class="cmgs">
                            	<div class="cmgsi">
                            		<img src="img/corporate-card/sign-up.svg">
                            		<span></span>
                            		<h3>Step 1</h3>
                            	</div>
                            	<div class="cmgsc1">
                            		<h3>Sign Up</h3>
                            		<p>Share the required contact and business details</p>
                            	</div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="cmgs">
                            	<div class="cmgsi">
                            		<img src="img/corporate-card/kyc.svg">
                            		<span></span>
                            		<h3>Step 2</h3>
                            	</div>
                            	<div class="cmgsc1">
                            		<h3>Complete KYC</h3>
                            		<p>Get your KYC done online and get the Pythru account</p>
                            	</div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="cmgs">
                            	<div class="cmgsi">
                            		<img src="img/corporate-card/start-banking.svg">
                            		<span></span>
                            		<h3>Step 3</h3>
                            	</div>
                            	<div class="cmgsc1">
                            		<h3>Start Banking</h3>
                            		<p>We will verify the business and get you the best credit limit</p>
                            	</div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>           
    </section>

    <section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<div class="km m-tc">
						<h2 class="pt0">Comes with integrated tools<br class="hidden-xs"> and insights</h2>
						<p class="pt5">Enables business growth with our Corporate Credit Card <br class="hidden-xs">all-in-one platform </p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="tpc">
						<div class="tpc-img">
							<img src="img/corporate-card/control-exponse.svg">
							<h3>Control expenses</h3>
						</div>
						<p>Manage spendings and expenses in real-time with custom set limits on the dashboard</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="tpc">
						<div class="tpc-img">
							<img src="img/corporate-card/manage-payment.svg">
							<h3>Manage Payments</h3>
						</div>
						<p>Prioritize and Categorize the payments automatically and enable smooth tracking.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="tpc">
						<div class="tpc-img">
							<img src="img/corporate-card/team-expense.svg">
							<h3>Team expenses</h3>
						</div>
						<p>Monitor and control the team spendings with the budget and set limits on the dashboard</p>
					</div>
				</div>
				<div class="col-md-offset-2 col-md-4 col-sm-6">
					<div class="tpc">
						<div class="tpc-img">
							<img src="img/corporate-card/subscription-payment.svg">
							<h3>Subscriptions payments</h3>
						</div>
						<p>Make the subscription payments such as Google Ads and manage them easily</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="tpc">
						<div class="tpc-img">
							<img src="img/corporate-card/banking-and-accounting.svg">
							<h3>Banking & Accounting</h3>
						</div>
						<p>Integration with the banking and accounting Pythru software for smooth management</p>
					</div>
				</div>				
			</div>
		</div>
	</section>

	<section class="sec-padding">
    	<div class="bg-blue">
    		<div class="container">
    			<div class="row">
    				<div class="cu-main">
    					<div class="col-md-5">
    						<div class="cu-main-text">
    							<h2 class="pb0">Pythru Corporate <br class="hidden-xs">Card</h2>
    							<p class="text-white pb30">Best one in India for your GST Return Filings</p>
    							<a href="#">Get Started <i class="bi bi-arrow-right"></i></a>
    						</div>
    					</div>
    					<div class="col-md-7">
    						<div class="cu-img hidden-xs">
    							<img src="img/payroll/image 37.svg" class="img-responsive">
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>

    <section class="sec-padding">
    	<div class="container">
    		<div class="row">
    			<div class="text-center km mb0">
					<h2>The Card created for the businesses</h2><h2>				
    			</h2></div>
    		</div>
    		<div class="row">
    			<div class="col-md-offset-1 col-md-10">
					<div class="tick">
						<div class="col-md-6 col-sm-12">
							<h4>Direct Funds loading from the dashboard</h4>
						</div>
						<div class="col-md-6 col-sm-12">
							<h4>Option to pay the minimum due amount</h4>
						</div>
						<div class="col-md-6 col-sm-12">
							<h4>Separate reporting of business expenses from personal ones</h4>
						</div>
						<div class="col-md-6 col-sm-12">
							<h4>Prioritize, categorize and add expenses for the essential services</h4>
						</div>
					</div>
				</div>
			</div>
    	</div>
    </section>

	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>
	
</body>
</html>