<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Online Payment Gateway | Free Payment Gateway in India</title>	
	<meta name="description" content="Accept online payments for Indian businesses. Offer multiple payment modes and secure online payment options on your website and mobile application.">
	<meta name="keywords" content="Payment Gateway, Online Payment Gateway in India, Online Payment Method">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/payment-gateway.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="hero">
						<div class="content">
							<h1 class="c-h1">Payment Gateway <br class="hidden-xs">for <span>Business </span><br class="hidden-xs">with Us</h1>
							<p class="pt10 pb30">Easy onboarding, Affordable TDR, and 100+ online <br class="hidden-xs">payment modes</p>
						</div> 
						<?php include 'include/common-signup.php';?>
					</div>
				</div>
				<div class="col-md-7">
					<div class="hero-graphic hidden-sm hidden-xs">
						<img src="img/payment-gateway/hero.svg" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-bottom-padding">
		<div class="container">	
			<div class="head-border">
				<div class="row head-point-pad">
					<div class="col-md-12">
						<div class="head-point-main">						
							<div class="col-md-4 col-sm-6">
								<div class="head-point">
									<img src="img/payment-gateway/business.svg">
									<p>Online and Offline <br class="hidden-xs">Businesses</p>
								</div>
							</div>
							<div class="head-point-vl hidden-xs hidden-sm"></div>
							<div class="col-md-4 col-sm-6">
								<div class="head-point">
									<img src="img/payment-gateway/ngo.svg">
									<p>NGOs, Trusts and <br class="hidden-xs">Non-Profit Organisations</p>
								</div>
							</div>
							<div class="head-point-v2 hidden-xs hidden-sm"></div>
							<div class="col-md-4 col-sm-6">
								<div class="head-point">
									<img src="img/payment-gateway/commercial.svg">
									<p>Social Commerce and <br class="hidden-xs">Freelancers</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding bg1">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<h2 class="c-h2 mt0">Awesome Pythru Features</h2>	
					<p class="com-p pt10">Experience the best payments acceptance services with Pythru <br class="hidden-xs">payment gateway through its available features</p>	
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="features">
						<img src="img/payment-gateway/f1.svg">
						<p>Easy Online <br class="hidden-xs">Onboarding</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="features">
						<img src="img/payment-gateway/f2.svg">
						<p>Multiple Online <br class="hidden-xs">Payment Modes</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="features">
						<img src="img/payment-gateway/f3.svg">
						<p>Affordable Transaction <br class="hidden-xs">Rates (TDR)</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="features">
						<img src="img/payment-gateway/f4.svg">
						<p>Accept International <br class="hidden-xs">Payments</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="features">
						<img src="img/payment-gateway/f5.svg">
						<p>Easy Settlement <br class="hidden-xs">Process</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="features">
						<img src="img/payment-gateway/f6.svg">
						<p>Best Security with <br class="hidden-xs">PCI DSS</p>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="left-right-section">
					<div class="col-md-5 col-sm-6">
						<div class="lrs-pad">
							<div>
								<span class="com-span">Best Customer Experience</span>
								<h2 class="c-h2">Smooth Checkout</h2>	
								<p class="com-p pt10">Offer your customers a fast payment checkout flow <br class="hidden-xs">with a customizable look and feel as per your brand</p>	
							</div>
							<div class="points">
								<h4>User-friendly interface</h4>
								<h4>Best success rate</h4>
								<h4>No useless redirects</h4>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="lrs-img">
							<img src="img/payment-gateway/checkout1.png" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
			<div class="row point-pad">
				<div class="left-right-section">
					<div class="col-md-7 col-sm-6 hidden-xs">
						<div class="lrs-img">
							<img src="img/payment-gateway/dashboard.png" class="img-responsive">
						</div>
					</div>
					<div class="col-md-5 col-sm-6">
						<div class="lrs-pad">
							<div>
								<span class="com-span">Analytics and Records</span>
								<h2 class="c-h2">Resourceful Merchant Dashboard</h2>	
								<p class="com-p pt10">Get real-time records of your transactions through Pythru payment gateway with analytics and graphical display</p>	
							</div>
							<div class="points">
								<h4>Downloadable reports</h4>
								<h4>Settlement Records</h4>
								<h4>Other functions</h4>
							</div>
						</div>
					</div>	
					<div class="col-md-6 col-sm-6 visible-xs">
						<div class="lrs-img">
							<img src="img/payment-gateway/dashboard.svg" class="img-responsive">
						</div>
					</div>				
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="pf-dashboard pf-dashboard-gradient-bg bg-rotate">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						<div class="mt100">
							<span class="com-span color-blue">Easy to Integrate</span>
							<h2 class="c-h2 color-white">Hassle-free Integration</h2>	
							<p class="com-p pt10 color-white mb20">The technical aspects and integration of Pythru payment gateway <br class="visible-sm">is effortless with its available open-source resources</p>
							<div class="ig-main">
								<div class="ig">
									<img src="img/payment-gateway/tech-doc.svg">
									<p>Complete Technical Documentation</p>
								</div>
								<div class="ig">
									<img src="img/payment-gateway/plugins.svg">
									<p>Plugins and SDKs</p>
								</div>
								<div class="ig">
									<img src="img/payment-gateway/tech-help.svg">
									<p>Technical help</p>
								</div>
							</div>
							<div class="gt-btn">
								<a href="" class="btn2 bg-blue"><i class="bi bi-file-earmark-medical mr10"></i> Go to Docs <i class="bi bi-chevron-double-right ml10"></i></a>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="integration-img">
							<img src="img/payment-gateway/integration.svg" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="col-md-offset-1 col-md-10 col-sm-12">
				<div class="box-border">
					<div class="row">					
						<div class="col-md-4 box-right">
							<div class="box1">
								<div class="fc">
									<h4>Robust APIs</h4>
									<h4>Best Payment Gateway</h4>
									<h4>Easy Onboarding</h4>
									<h4>Smooth Payouts</h4>
								</div>
							</div>
						</div>
						<div class="col-md-8 box-left">
							<div class="box2">
								<h2 class="c-h2 mt0">Trust PyThru for your <br class="hidden-xs">business finances</h2>	
								<p class="com-p pt10">The technical aspects and integration of Pythru payment gateway is effortless with its available open-source resources</p>
								<div class="signup iwunset">
									<input type="email" name="email" placeholder="Email address" required="" class="ibox-shadow">
									<a href=""><img src="img/home/Group 3.svg" class="bg-blue"></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="accordion-section clearfix mt0 sec-padding" aria-label="Question Accordions">
	  <div class="container">	
		  <div class="col-md-offset-1 col-md-10">  
			  <h2 class="text-center faq-title">FAQs</h2>
			  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
						What is Pythru Payment Gateway?
					  </a>
					</h3>
				  </div>
				  <div id="collapse0" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading0" aria-expanded="true">
					<div class="panel-body px-3 mb-4">
					  <p>Pythru lets Indian businesses accept online payments securely with multiple payment modes options. It can be integrated with the website/app or used as payment links.</p>
					  </ul>
					</div>
				  </div>
				</div>
				
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading1">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
						What payment modes does Pythru offer?
					  </a>
					</h3>
				  </div>
				  <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
					<div class="panel-body px-3 mb-4">
					  <p>It offers 100+ payment method options for debit cards, credit cards, wallets, net banking, UPI, and EMI</p>
					</div>
				  </div>
				</div>
				
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading2">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
						How to use Pythru Payment Gateway?
					  </a>
					</h3>
				  </div>
				  <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
					<div class="panel-body px-3 mb-4">
					  <p>It can be integrated into the website or mobile application, it can be added as a Payment Button on the website or sent through payment links via Whatsapp, email, DMs, etc. You may discuss your business needs with its team or directly sign up with Pythru.</p>
					  <p>Then you may try the test mode or directly get registered with the required details and documents. These will go through the verification process and if the verification is successful, your account will be activated.</p>
					</div>
				  </div>
				</div>
				
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading3">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
						What are the pricing plans?
					  </a>
					</h3>
				  </div>
				  <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
					<div class="panel-body px-3 mb-4">
					  <p>For more details visit the <a href="pricing">Pricing</a> page.</p>
					</div>
				  </div>
				</div>
			  </div>
		  </div>	  
	  </div>
	</section>
	
	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>

	
</body>
</html>