<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Easy Payouts for your Business Payments | PyThru Payouts</title>	
	<meta name="description" content="Pay timely payouts in bulk. send thousands of payouts instantly to multiple parties in the bank account at affordable rates.">
	<meta name="keywords" content="Payouts, Payout Links, Business Payments, Smooth Payments, Payment Gateway Service Provider">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/payout.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="hero">
						<div class="content pb10">
							<h1 class="c-h1">Instant and Hasslefree <span>Payouts </span> with PyThru</h1>
							<p class="pt10 pb30">Send easy and secure payments to bank account,<br class="hidden-xs"> UPI ID, and wallets</p>
						</div> 
						<?php include 'include/common-signup.php';?>
					</div>
				</div>
				<div class="col-md-7">
					<div class="hero-graphic hidden-sm hidden-xs">
						<img src="img/payout/hero.svg" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="sub-point">
						<img src="img/payout/add.svg">
						<span>Stage 1</span>
						<h3>Add</h3>
						<p>Merchant adds the money to his Payout account</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="sub-point">
						<img src="img/payout/verify.svg">
						<span>Stage 2</span>
						<h3>Verify</h3>
						<p>Merchant verifies the beneficiary details on PyThru</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="sub-point">
						<img src="img/payout/beneficiary.svg">
						<span>Stage 3</span>
						<h3>Beneficiary</h3>
						<p>Add the verified beneficiary details for payment</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="sub-point">
						<img src="img/payout/payout.svg">
						<span>Stage 4</span>
						<h3>Payout</h3>
						<p>Sends the Payouts to the beneficiary from dashboard</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="bg1 pad-sec-tb">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<span class="com-span bg-white">Smart Payment</span>
					<h2 class="c-h2">Why use PyThru Payout?</h2>	
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/payout/payout-links.svg">
						<h3>Payout links</h3>
						<p>Send payments with Payout links. No Payee details required</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/payout/easy-activation.svg">
						<h3>Easy activation</h3>
						<p>Activate Payouts easily with an approved merchant account</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/payout/bulk-payouts.svg">
						<h3>Bulk Payouts</h3>
						<p>Upload a single file and make Payouts to multiple Payee in bulk</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/payout/add-instant-payee.svg">
						<h3>Add Instant Payee</h3>
						<p>Verify and add the beneficiary account of the Payee instantly</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/payout/secure.svg">
						<h3>Secure</h3>
						<p>PyThru is a PCI DSS secure platform and enables safe Payouts</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/payout/simple-dashboard.svg">
						<h3>Simple Dashboard</h3>
						<p>Analyze, track and manage the Payouts with smart dashboard</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="pf">
						<img src="img/payout/automate-payout.svg">
						<h3>Automate your Payouts <br class="hidden-xs">using the PyThru Payout <br class="hidden-xs">APIs</h3>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="pf">
						<img src="img/payout/instant-refund.svg">
						<h3>Make the Instant Refund <br class="hidden-xs">possible to the beneficiary <br class="hidden-xs">with Payout links</h3>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="pf">
						<img src="img/payout/reward.svg">
						<h3>Easy payment of game <br class="hidden-xs">rewards, employee & <br class="hidden-xs">vendor payments, etc</h3>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-top-padding">
		<div class="custom-bg custom-bg-gradient bg-rotate">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-sm-6">
						<div class="mt100">
							<span class="com-span color-blue">Affordable</span>
							<h2 class="c-h2 color-white">Pricing Plans</h2>	
							<p class="com-p pt10 color-white mb20">We provide the best services in two different pricing plans as per your business requirements</p>
							<div class="points">
								<h4>No Maintenances Charge</h4>
								<h4>No Setup fees</h4>
								<h4>No Hidden Charges</h4>
							</div>
						</div>
					</div>
					<div class="col-md-offset-1 col-md-6 col-sm-6">
						<div class="plan">
							<div class="row main-plan ex-lr-margin">
								<div class="col-md-3 text-center mt15">
									<span>3%</span>
								</div>
								<div class="col-md-9">
									<h3>Standard Plan</h3>
									<p>Indian Debit Card, Net Banking, UPI, Walletsincluding Freecharge,Mobikwik, etc.</p>
								</div>							
							</div>		
							<div class="row sub-plan ex-lr-margin">
								<div class="col-md-8">
									<h3>Business Plan</h3>
									<h4>To discuss the <br class="hidden-xs">Customized Pricing</h4>
								</div>
								<div class="col-md-4">
									<a href="" class="cbtn3 cbtn3">Enquire Now</a>								
								</div>
							</div>					
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-top-padding bot-mar">
		<div class="container">
			<div class="row">
				<div class="col-md-7 col-sm-6">
					<div class="content m-center">
						<span class="com-span">Best Solution</span>
						<h2 class="c-h1">Make easy payments or send <br class="hidden-xs"><span>Payout Links</span> to the vendors, customers, etc.</h2>
					</div>
				</div>
				<div class="col-md-5 col-sm-6">
					<div class="bot-sp">	
						<a href="" class="cbtn-signup">Sign Up</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include 'include/footer.php';?>

	<?php include 'include/js.php';?>
	
</body>
</html>