<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Payment Gateway for SaaS and IT platforms | PyThru</title>	
	<meta name="description" content="Enable hassle-free and secure payments for SaaS products and services with PyThru. Make them available on-demand everywhere anytime.">
	<meta name="keywords" content="Payment Gateway, Payment Processing, Payment Gateway for SaaS, Payouts, online payments, Payment links, Payment Pages, Payment Button">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/solutions.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="hero">
						<div class="content">
							<h1 class="c-h1">Best Payments Solution for<br class="hidden-xs">SaaS  <span> Platforms</span></h1>
							<p class="pt10 pb30">Render the multi-currency support and secure payments processing with PyThru
							and automate payments for your IT & SaaS company</p>
						</div> 
						<?php include 'include/common-signup.php';?>
					</div>
				</div>
				<div class="col-md-offset-1 col-md-6">
					<div class="hero-graphic hidden-sm hidden-xs">
						<img src="img/saas/hero.svg" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-bottom-padding">
		<div class="container">
			<div class="row">
				<div class="text-center ff-title">
					<h2><b>SaaS </b>specific Features of PyThru Payment Gateway</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<div class="ff">
						<img src="img/saas/billing.svg">
						<h3>Subscription Billing</h3>
						<p>Enable the Subscription plan to accept automatic recurring payments and increase revenue</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="ff">
						<img src="img/saas/pricing-plan.svg">
						<h3>Custom Pricing Plans</h3>
						<p>Customize billing plans as per your need based on duration, quantity, or one-time payments </p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="ff">
						<img src="img/saas/invoice-and-accounting.svg">
						<h3>Invoice & Accounting</h3>
						<p>Enable discounts, trials, negotiation, or charges and send invoices. Automate the Accounting & reconciliation.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row point-pad">
				<div class="left-right-section">
					<div class="col-md-6 col-sm-6">
						<div class="lrs-img">
							<img src="img/saas/global-payment-accept.svg" class="img-responsive">
						</div>
					</div>
					<div class="col-md-5 col-sm-6">
						<div class="lrs-pad m-center">
							<div>
								<span class="com-span">International Gateway</span>
								<h2 class="c-h2">Accept Payments Globally</h2>	
								<p class="com-p pt10">Render your IT and SaaS products worldwide with our multi-currency support and real-time currency conversion for settlements in INR</p>	
							</div>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="ff-p-bg">
			<div class="container">
				<div class="row sec-padding">
					<div class="text-center ff-title1">
						<h2><b>Pricing</b> Plans</h2>
					</div>
					<div class="fp-main">
						<div class="col-md-4 col-sm-6">
							<div class="fp">
								<div class="fp-card b1">
									<div class="fp-detail">
										<div class="col-md-4">
											<b>2.5%</b>
										</div>
										<div class="col-md-8">
											<h4>Standard Pricing</h4>
											<p>Credit & Deibt card, UPI, wallets, Net banking & QR</p>
										</div>
										<div class="col-md-offset-2 col-md-9">
											<hr class="fp-line">
										</div>
										<div class="col-md-offset-2 col-md-4 col-xs-4">
											<div class="fp-price">
												<b>₹ 0.00</b>
												<p>Setup fees</p>
											</div>
										</div>
										<div class="col-md-6 col-xs-8">
											<div class="fp-price">
												<b>₹ 0.00</b>
												<p>No annual maintainance</p>
											</div>
										</div>
									</div>
								</div>	
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="fp">
								<div class="fp-card b2">
									<div class="fp-detail">
										<div class="col-md-4">
											<b>Talk</b>
											<span>with us</span>
										</div>
										<div class="col-md-8">
											<h4>Business Plan</h4>
											<p>To discuss the Customized Pricing </p>
										</div>
										<div class="col-md-offset-2 col-md-9">
											<hr class="fp-line">
										</div>
										<div class="fp-btn">
											<a href="">Contact Us</a>
										</div>
									</div>
								</div>	
							</div>
						</div>
						<div class="col-md-4 col-sm-12">
							<div class="fp-content">
								<h3>Best services with <br class="hidden-xs">two different pricing plans </h3>
								<p>Discuss yours as per <br class="hidden-xs">your business requirements</p>
								<div class="fp-btn1">
									<a href="">Know More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="text-center ff-title pb20">
					<h2><b>More </b> with PyThru for SaaS</h2>
				</div>
			</div>
			<div class="row">
				<div class="tick">
					<div class="col-md-offset-2 col-md-3 col-sm-4">
						<h4>Smooth Onboarding</h4>
					</div>
					<div class="col-md-3 col-sm-4">
						<h4>Fast Settlements</h4>
					</div>
					<div class="col-md-3 col-sm-4">
						<h4>100+ Payment Modes</h4>
					</div>
					<div class="col-md-offset-3 col-md-3 col-sm-offset-2 col-sm-4">
						<h4>Smart Payment Links</h4>
					</div>
					<div class="col-md-3 col-sm-4">
						<h4>Resourceful Dashboard</h4>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row bg-black">
				<div class="col-md-2 hidden-xs">
					<div class="top-img">
						<img src="img/saas/lock.svg" class="img-responsive">
					</div>
				</div>
				<div class="col-md-8">
					<div class="ss">
						<h2>Secure and Protected</h2>
						<p>PyThru safety and security features <br class="hidden-xs">protect you and your clients</p>
					</div>
					<div class="ss-point">
						<div class="col-md-offset-2 col-md-4 col-sm-6">
							<h4>PCI DSS Compliant</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Encryption & SSL</h4>
						</div>
						<div class="col-md-offset-2 col-md-4 col-sm-6">
							<h4>Risk Verification</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Fraud Prevention</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-offset-1 col-md-4 col-sm-5">
	            	<div class="qute m-center">
	            		<img src="img/saas/qute.svg">
	            		<h2>Here’s how our <br class="hidden-xs">Merchants feel…</h2>
	            	</div>
	            </div>
	            <div class="col-md-6 col-sm-7">
	                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	                    <!-- Wrapper for slides -->
	                    <div class="carousel-inner">
	                        <div class="item active">
	                            <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="img/home/man.svg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                    	<div class="content">
                                    		<h3>Smooth Experience</h3>
	                                    	<p>PyThru is smooth to integrate with its ready plugins. The customer support is excellent and so the overall experience has been smooth too.</p>
	                                    	<br>
	                                        <h4>Sonu Kumar</h4>
	                                        <span>Doon Cars</span>
	                                    </div>
                                    </div>	                               
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="img/home/man.svg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                    	<div class="content">
                                    		<h3>Positive customer experience</h3>
	                                    	<p>We did not face any issue in using PyThru. The checkout experience is very good and our customers are highly satisfied with the payments process with multiple payment modes.</p>
	                                        <h4>Sawan Kumar</h4>
	                                        <span>Etnasasta</span>
	                                    </div>
                                    </div>	                               
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="img/home/man.svg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                    	<div class="content">
                                    		<h3>Increase in Sales</h3>
	                                    	<p>Using PyThru has increased the sales on our platform and also earned us long-term loyal customers. We love the dashboard features and functions that have made our service delivery easy.</p>
	                                        <h4>Sharwan Kumar</h4>
	                                        <span>The Various Store</span>
	                                    </div>
                                    </div>	                               
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="img/home/man.svg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                    	<div class="content">
                                    		<h3>Affordable Transaction Rates</h3>
	                                    	<p>The high transaction success rate is one of the reasons along with many others such as a simple and easy checkout for a positive customer experience. Transaction rates are highly competitive.</p>
	                                        <h4>Bikram Biswas</h4>
	                                        <span>Velvery Private Limited</span>
	                                    </div>
                                    </div>	                               
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-md-offset-1 col-md-5">
		                <div class="controls testimonial_control pull-right">
		                    <a class="left bi bi-chevron-left btn-default testimonial_btn fw900" href="#carousel-example-generic" data-slide="prev"></a>

		                    <a class="right bi bi-chevron-right btn-default testimonial_btn fw900" href="#carousel-example-generic" data-slide="next"></a>
		                </div>
		            </div>
	            </div>
	        </div>
	    </div>
	</section>

	<section class="sec-padding">
	    <div class="container">
	        <div class="row">
	        	<div class="ff-box-main">
	        		<div class="col-md-offset-1 col-md-5 col-sm-6">
	        			<div class="ff-box">
	        				<div class="ff-box-img">
		        				<img src="img/saas/red-circle.svg">
		        				<h3>Risk Management <br class="hidden-xs">software</h3>
	        				</div>
	        				view product
	        			</div>
	        		</div>
	        		<div class="col-md-5 col-sm-6">
	        			<div class="ff-box">
	        				<div class="ff-box-img">
		        				<img src="img/saas/blue-circle.svg">
		        				<h3>Risk Management <br class="hidden-xs">software</h3>
	        				</div>
	        				view product
	        			</div>
	        		</div>
	        	</div>
	        </div>
	    </div>
	</section>

	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>

	
</body>
</html>