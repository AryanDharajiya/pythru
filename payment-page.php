<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'include/meta.php';?> 

  <title>Secure Hosted Payment Pages - PCI DSS compliant Payment Pages</title> 
  <meta name="description" content="Create Custom Payment Pages and accept online payment through credit card, debit card, net banking, wallets, and UPI in a secure way in no time.">
  <meta name="keywords" content="Online Payment Page, Hosted Payment Pages, Secure Payment Pages, PCI compliant Payment Pages">

  <?php include 'include/css.php';?>   
  <link rel="stylesheet" href="css/payment-page.css">
</head>
<body>

  <?php include 'include/header.php';?>

  <section class="sec-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="hero">
            <div class="content pb10">
              <h1 class="c-h1">PyThru Custom <span>Payment Pages</span> to accept online payments</h1>
            </div> 
            <div class="head-point">
              <div class="col-md-4 dis">
                <span><i class="bi bi-check"></i> Free to create</span>
                <span><i class="bi bi-check"></i> Easy to use</span>
              </div>
              <div class="col-md-6 dis">
                <span><i class="bi bi-check"></i> Affordable TDR</span>
                <span><i class="bi bi-check"></i> No coding</span>
              </div>          
            </div>
            <div class="clearfix"></div>
            <div class="mt50"></div>
            <?php include 'include/common-signup.php';?>
          </div>
        </div>
        <div class="col-md-6">
          <div class="hero-graphic hidden-sm hidden-xs">
            <img src="img/payment-page/hero.svg" class="img-responsive">
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="sec-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6">
          <div class="sub-point">
            <img src="img//payment-page/signup.svg">
            <span>Stage 1</span>
            <h3>Sign up</h3>
            <p>Register and get your PyThru Merchant Account activated</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="sub-point">
            <img src="img//payment-page/createpage.svg">
            <span>Stage 2</span>
            <h3>Create Page</h3>
            <p>Create the instant customised payment page for your business</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="sub-point">
            <img src="img//payment-page/acceptpayment.svg">
            <span>Stage 3</span>
            <h3>Accept Payments</h3>
            <p>Share the link or embed the payment button for the page</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="bg1 pad-sec-tb">
    <div class="container">
      <div class="row">
        <div class="text-center">
          <span class="com-span bg-white">Smart Payments</span>
          <h2 class="c-h2 pb10">Best PyThru Payment Page Features</h2> 
          <p class="ex-p pb10">You can accept the payments in a smart way with or without <br class="hidden-xs"> website with PyThru Payment Pages.</p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 col-sm-6">
          <div class="why-use">
            <img src="img/payment-page/zero-coding.svg">
            <h3>Zero Coding</h3>
            <p>It requires no coding or integration to create a customised payment page</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="why-use">
            <img src="img/payment-page/customization.svg">
            <h3>Customization</h3>
            <p>As per your purpose, you can customize the look, details and URL of the payment page</p>
          </div>
        </div>
        <div class="clearfix visible-sm"></div>
        <div class="col-md-4 col-sm-6">
          <div class="why-use">
            <img src="img/payment-page/analytics.svg">
            <h3>Analytics</h3>
            <p>Merchant Dashboard helps with the real-time analytics and reporting</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="why-use">
            <img src="img/payment-page/mobile-friendly.svg">
            <h3>Mobile friendly</h3>
            <p>Compatible with all devices and creates great mobile experience to accept payments</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="why-use">
            <img src="img/payment-page/security.svg">
            <h3>Security</h3>
            <p>Payment pages have complete security compliances and certifications</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="why-use">
            <img src="img/payment-page/payment-modes.svg">
            <h3>Payment Modes</h3>
            <p>The customer has 100+ available payment options to make payment</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- <section class="bg2">
    <div class="container">
      <div class="row">
        <div class="vid">
          <div class="col-md-5 col-sm-6">
            <div class="contant sec-padding">
              <span class="com-span">Tutorial</span>
              <h2 class="text-white">Watch this Video <br class="hidden-xs">  & Create <br class="hidden-xs">Payment Page</h2>
              <p class="text-white">Learn how to create PyThru Payment Pages <br class="hidden-xs">to accept online payments</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6">
            <div class="vb">
              <a id="play-video" class="video-play-button" href="#">
                <span></span>
              </a>

              <div id="video-overlay" class="video-overlay">
                <a class="video-overlay-close">&times;</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> -->


  <section class="sec-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-7 col-sm-6">
          <div class="ed">
            <img src="img/payment-page/payment-purpose.svg" class="img-responsive">
          </div>
        </div>
        <div class="col-md-5 col-sm-6">
          <div class="lrs-pad">
            <div>
              <span class="com-span">Big New Innovation</span>
              <h2 class="c-h2">Payment Page & Purpose</h2>  
              <p class="com-p pt10 pb30">We support various industries and business models to accept online payments. Get yours registered now!</p> 
            </div>
            <div class="points1">
              <a href="" class="custom-btn1">Sign Up</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="sec-top-padding">
    <div class="custom-bg custom-bg-gradient bg-rotate">
      <div class="container">
        <div class="row">
          <div class="col-md-5 col-sm-6">
            <div class="mt100">
              <span class="com-span color-blue">Easiest Integration</span>
              <h2 class="c-h2 color-white">Pricing Plans</h2> 
              <p class="com-p pt10 color-white mb20">We provide the best services in two different pricing plans as per your business requirements</p>
              <div class="ig-main">
                <div class="ig">
                  <img src="img/common/no-hiddenfee.svg">
                  <p>No Hidden Fees</p>
                </div>
                <div class="ig">
                  <img src="img/common/no-maintanance.svg">
                  <p>No Maintanance</p>
                </div>
                <div class="ig">
                  <img src="img/common/no-setupcharge.svg">
                  <p>No setup charges</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-offset-1 col-md-6 col-sm-6">
            <div class="plan">
              <div class="row main-plan ex-lr-margin">
                <div class="col-md-3 text-center mt15">
                  <span>3%</span>
                </div>
                <div class="col-md-9">
                  <h3>Standard Plan</h3>
                  <p>Indian Debit Card, Net Banking, UPI, Walletsincluding Freecharge,Mobikwik, etc.</p>
                </div>              
              </div>    
              <div class="row sub-plan ex-lr-margin">
                <div class="col-md-8">                  
                  <h4>Feel Free to contact <br class="hidden-xs">us for</h4>
                  <h3 class="mt10">Business Plan</h3>
                </div>
                <div class="col-md-4">
                  <a href="" class="cbtn3 cbtn3">Contact Us</a>               
                </div>
              </div>          
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="sec-top-padding bot-mar">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <div class="content m-center">
            <span class="com-span">PyThru for U</span>
            <h2 class="c-h1">No Website No App Still <br class="hidden-xs"><span>No Worries</span></h2>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="bot-sp">  
            <a href="" class="cbtn-signup">Sign Up</a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="accordion-section clearfix mt0 sec-padding" aria-label="Question Accordions">
    <div class="container"> 
      <div class="col-md-offset-1 col-md-10">  
        <h2 class="text-center faq-title">FAQs</h2>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
          <h3 class="panel-title">
            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
           What is the Pythru Payment Page?
            </a>
          </h3>
          </div>
          <div id="collapse0" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading0" aria-expanded="true">
          <div class="panel-body px-3 mb-4">
            <p>Pythru Payment Page is a customized page through which you can showcase your business brand and accept online payments. You can create the payment page and provide the details about your business and also accept the payments with multiple payment modes facilitated by Pythru.</p>
            </ul>
          </div>
          </div>
        </div>
        
        <div class="panel panel-default">
          <div class="panel-heading p-3 mb-3" role="tab" id="heading1">
          <h3 class="panel-title">
            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
            How to create a Pythru Payment Page?
            </a>
          </h3>
          </div>
          <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
          <div class="panel-body px-3 mb-4">
            <p>You can easily create and customize the payment page with the available templates or you can also make your own without any technical requirements or integration i.e. zero coding. You can go online with your payment page without integration.</p>
          </div>
          </div>
        </div>
        
        <div class="panel panel-default">
          <div class="panel-heading p-3 mb-3" role="tab" id="heading2">
          <h3 class="panel-title">
            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
            How secure are the Payment Pages?
            </a>
          </h3>
          </div>
          <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
          <div class="panel-body px-3 mb-4">
            <p>Payment Pages are part of the payments suite of the Pythru Payment Gateway Products. It is also secured with the PCI DSS i.e. all the Pythru security compliances are relevant to the Pythru Payment Pages also.</p>
          </div>
          </div>
        </div>
        
        <div class="panel panel-default">
          <div class="panel-heading p-3 mb-3" role="tab" id="heading3">
          <h3 class="panel-title">
            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
            Are the automated receipts sent?
            </a>
          </h3>
          </div>
          <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
          <div class="panel-body px-3 mb-4">
            <p>Yes, the automated payment receipts will be sent to the customers by default. You can turn off this if you do not wish to send the automated receipts. </p>
          </div>
          </div>
        </div>
        </div>
      </div>    
    </div>
  </section>
  
  <?php include 'include/footer.php';?>

  <?php include 'include/js.php';?>

  <!-- <script type="text/javascript">
    $('#play-video').on('click', function(e){
      e.preventDefault();
      $('#video-overlay').addClass('open');
      $("#video-overlay").append('<iframe width="560" height="315" src="https://www.youtube.com/embed/ngElkyQ6Rhs" frameborder="0" allowfullscreen></iframe>');
    });

    $('.video-overlay, .video-overlay-close').on('click', function(e){
      e.preventDefault();
      close_video();
    });

    $(document).keyup(function(e){
      if(e.keyCode === 27) { close_video(); }
    });

    function close_video() {
      $('.video-overlay.open').removeClass('open').find('iframe').remove();
    };
  </script> -->

  
</body>
</html>