<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Generate Payment Link - PyThru Payment Link</title>	
	<meta name="description" content="Accept secure online payments with PyThru Payment Link. Generate and share payment links on Facebook, WhatsApp, SMS, etc and offer multiple payment modes.">
	<meta name="keywords" content="Online Payment Link, Generate Payment Link, Share Payment Link, Free Payment Link, Accept Online Payments">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/payment-link.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="hero">
						<div class="content pb10">
							<h1 class="c-h1">Accept fast payments with <span>Payment Links</span></h1>
						</div> 
						<div class="head-point">
							<div class="col-md-4 dis">
								<span><i class="bi bi-check"></i> Free to create</span>
								<span><i class="bi bi-check"></i> Easy to use</span>
							</div>
							<div class="col-md-6 dis">
								<span><i class="bi bi-check"></i> Affordable TDR</span>
								<span><i class="bi bi-check"></i> No website needed</span>
							</div>					
						</div>
						<div class="clearfix"></div>
						<div class="mt50"></div>
						<?php include 'include/common-signup.php';?>
					</div>
				</div>
				<div class="col-md-7">
					<div class="hero-graphic hidden-sm hidden-xs">
						<img src="img/payment-link/hero.svg" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="sub-point">
						<img src="img//payment-link/create-link.svg">
						<span>Step 1</span>
						<h3>Create Link</h3>
						<p>Fill in the required details and generate the payment link</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="sub-point">
						<img src="img//payment-link/share-link.svg">
						<span>Step 2</span>
						<h3>Share Link</h3>
						<p>Copy and send the link via email, Whatsapp, text, etc.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="sub-point">
						<img src="img//payment-link/get-paid.svg">
						<span>Step 3</span>
						<h3>Get Paid</h3>
						<p>Customer makes payment with their preferred payment mode</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="bg1 pad-sec-tb">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<span class="com-span bg-white">Smart Payment</span>
					<h2 class="c-h2">Why use PyThru Payment Links?</h2>	
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/payment-link/share.svg">
						<h3>Quick to Share</h3>
						<p>Easily send payment links and accept easy payments</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/payment-link/manage.svg">
						<h3>Easy to Manage</h3>
						<p>Merchant Dashboard helps with smooth managing of the links</p>
					</div>
				</div>
				<div class="clearfix visible-sm"></div>
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/payment-link/modes.svg">
						<h3>Multiple Payment Modes</h3>
						<p>The customer has 100+ available payment options to make payment</p>
					</div>
				</div>
				<div class="clearfix hidden-sm"></div>
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/payment-link/business.svg">
						<h3>Offline Business</h3>
						<p>No website or mobile application? Get paid via our Payment Links</p>
					</div>
				</div>
				<div class="clearfix visible-sm"></div>
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/payment-link/safe-secure.svg">
						<h3>Safe & Secure</h3>
						<p>The payment links are equally secure to get paid from the customers</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/payment-link/cc.svg">
						<h3>Customize Checkout</h3>
						<p>As per your brand, you can customize the color, logo, etc for the payment link</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="point">
				<div class="row">
					<div class="col-md-7">
						<div class="col-md-6 col-sm-6">
							<div class="pp">
								<img src="img/payment-link/ecomerce.svg">
								<h4>eCommerce</h4>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="pp">
								<img src="img/payment-link/individual.svg">
								<h4>Individual</h4>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="pp">
								<img src="img/payment-link/education.svg">
								<h4>Education</h4>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="pp">
								<img src="img/payment-link/social.svg">
								<h4>Social/Freelancer</h4>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="pp">
								<img src="img/payment-link/it-saas.svg">
								<h4>IT / SaaS</h4>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 ex-ipad-pad">
							<div class="pp">
								<img src="img/payment-link/entertainment.svg">
								<h4>Entertainment</h4>
							</div>
						</div>
					</div>
					<div class="content">
						<div class="col-md-5">
							<span class="com-span">Big New Innovation</span>
							<h2 class="c-h2">PyThru Supported Industry</h2>	
							<p class="com-p pt10 pb30">We support various industries and business models to accept online payments. Get yours onboard now!</p>
							<a href="" class="custom-btn1">Sign Up</a>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-top-padding">
		<div class="custom-bg custom-bg-gradient bg-rotate">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-sm-6">
						<div class="mt100">
							<span class="com-span color-blue">Affordable</span>
							<h2 class="c-h2 color-white">Pricing Plans</h2>	
							<p class="com-p pt10 color-white mb20">We provide the best services in two different pricing plans as per your business requirements</p>
							<div class="ig-main">
								<div class="ig">
									<img src="img/common/no-hiddenfee.svg">
									<p>No Hidden Fees</p>
								</div>
								<div class="ig">
									<img src="img/common/no-maintanance.svg">
									<p>No Maintanance</p>
								</div>
								<div class="ig">
									<img src="img/common/no-setupcharge.svg">
									<p>No setup charges</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-offset-1 col-md-6 col-sm-6">
						<div class="plan">
							<div class="row main-plan ex-lr-margin">
								<div class="col-md-3 text-center mt15">
									<span>3%</span>
								</div>
								<div class="col-md-9">
									<h3>Standard Plan</h3>
									<p>Indian Debit Card, Net Banking, UPI, Walletsincluding Freecharge,Mobikwik, etc.</p>
								</div>							
							</div>		
							<div class="row sub-plan ex-lr-margin">
								<div class="col-md-8">
									<h3>Business Plan</h3>
									<h4>To discuss the <br class="hidden-xs">Customized Pricing</h4>
								</div>
								<div class="col-md-4">
									<a href="" class="cbtn3 cbtn3">Enquire Now</a>								
								</div>
							</div>					
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-top-padding bot-mar">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="content m-center">
						<span class="com-span">Best Solution</span>
						<h2 class="c-h1">If you Don’t Have <br class="hidden-xs"><span>Any Website</span></h2>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="bot-sp">	
						<a href="" class="cbtn-signup">Sign Up</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="accordion-section clearfix mt0 sec-padding" aria-label="Question Accordions">
	  <div class="container">	
		  <div class="col-md-offset-1 col-md-10">  
			  <h2 class="text-center faq-title">FAQs</h2>
			  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
						What is a Pythru Payment Link?
					  </a>
					</h3>
				  </div>
				  <div id="collapse0" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading0" aria-expanded="true">
					<div class="panel-body px-3 mb-4">
					  <p>Pythru Payment Link is used to accept easy online payments from your customers for the products and services sold. You can accept full or partial online payments from a single customer or in bulk without a website or mobile application with these payment links. You can also accept international payments with our payment links (after the international services are activated on your merchant account).</p>
					  </ul>
					</div>
				  </div>
				</div>
				
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading1">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
						How to accept online payments with Pythru Payment Links?
					  </a>
					</h3>
				  </div>
				  <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
					<div class="panel-body px-3 mb-4">
					  <p>You can send the payment links to the customers through SMS, WhatsApp, email, etc. The customer will open the link and proceed with the payment with their preferred payment mode. After the customer makes a successful payment, you will receive it directly in your bank account within your predefined settlement cycle period.</p>
					</div>
				  </div>
				</div>
				
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading2">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
						How to get started with the Pythru Payment Links?
					  </a>
					</h3>
				  </div>
				  <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
					<div class="panel-body px-3 mb-4">
					  <p>After you register with the Pythru merchant account, it will be activated post successful verification. And with the account activation, you can also start using the Pythru payment links. Payment links are part of the payments suite of the payment gateway products.</p>
					</div>
				  </div>
				</div>
				
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading3">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
						How to create the Pythru Payment Link?
					  </a>
					</h3>
				  </div>
				  <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
					<div class="panel-body px-3 mb-4">
					  <p>It is very easy to create, send and make payments with our payment link. After you log in to the dashboard tool, you need to go to the Payment Link section. Further, add the customer number & email, and amount and share the payment link. You can also set the expiry and the notification method of the link.</p>
					</div>
				  </div>
				</div>
				
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading4">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">
						How to create the payment links for bulk payments?
					  </a>
					</h3>
				  </div>
				  <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
					<div class="panel-body px-3 mb-4">
					  <p>Our Batch Feature enables the Bulk Payment Link payment acceptance. For this, you can add the customer details, amount, and purpose of the link in the XLSX or CSV file and upload it - sending the bulk payment request in no time.</p>
					</div>
				  </div>
				</div>
			  </div>
		  </div>	  
	  </div>
	</section>
	
	<?php include 'include/footer.php';?>

	<?php include 'include/js.php';?>
	
</body>
</html>