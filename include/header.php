<div class="header" id="home">
    <div class="container">
        <div class="header-bottom">
            <nav id="navigation" class="navigation navigation-landscape">
                <div class="nav-header">
                    <a class="nav-brand" href="/">
                        <img class="logo-default logo-color" src="img/logo.svg" />
                        <img class="logo-default logo-white" src="img/logo-white.svg" />
                    </a>
                    <div class="nav-toggle"></div>
                </div>
                <div class="nav-menus-wrapper">
                <ul class="nav-menu">
                <li class="">
                <a href="#">Products<span class="submenu-indicator"></span></a>

                <ul class="nav-dropdown nav-submenu nav-fea right-unset d-none nav-w1">
                    <div class="row">
                        <div class="col-md-3">
                            <ul class="nav nav-pills">
                                <li class="active"><a data-toggle="pill" href="#menu0">Payment Gateway</a></li>
                                <li><a data-toggle="pill" href="#menu1">Banking</a></li>
                                <li><a data-toggle="pill" href="#menu2">Accounting</a></li>
                            </ul>
                        </div>
                        <div class="col-md-9 cp-lr-nv">
                            <div class="tab-content">
                                <div id="menu0" class="tab-pane fade in active">
                                    <div class="col-md-4">
                                        <li>
                                            <a href="payment-gateway" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/payment-gateway.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4 class="link">Payment Gateway</h4>
                                                    <p>Accept payments on Website or APP</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="payment-page" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/payment-page.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4>Payment Pages</h4>
                                                    <p>Create a custom Page and get paid</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="payment-link" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/payment-link.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4>Payment Links</h4>
                                                    <p>Send links & accept payments</p>
                                                </div>
                                            </a>
                                        </li>
                                    </div>
                                    <div class="col-md-4">
                                        <li>
                                            <a href="payment-button" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/payment-button.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4>Payment Button</h4>
                                                    <p>Add Payment Button on Website</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="instant-settlement" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/instant-settlement.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4>Instant Settlement</h4>
                                                    <p>Get your settlements in minutes</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="invoice" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/invoice.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4>Invoice</h4>
                                                    <p>Get paid with GST compliant Invoices</p>
                                                </div>
                                            </a>
                                        </li>
                                    </div>
                                    <div class="col-md-4">
                                        <li>
                                            <a href="subscription" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/subscription.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4>Subscription</h4>
                                                    <p>Accept recurring sub payments</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="upi-autopay" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/upi-autopay.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4>Upi Autopay</h4>
                                                    <p>Collect recurring payments with UPI</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="international-merchant" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/international-merchant.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4>International Merchant</h4>
                                                    <p>Accept international payments</p>
                                                </div>
                                            </a>
                                        </li>
                                    </div>
                                </div>
                                <div id="menu1" class="tab-pane fade">
                                    <div class="col-md-5">
                                        <li>
                                            <a href="payout" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/payout.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4 class="link">Payout</h4>
                                                    <p>Automate Instant Payouts anytime, anywhere</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="current-account" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/current-account.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4 class="link">Current Account</h4>
                                                    <p>Open Current Account for Business Finances</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="virtual-account" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/virtual-account.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4 class="link">Virtual Account</h4>
                                                    <p>Track Payments with Virtual Accounts</p>
                                                </div>
                                            </a>
                                        </li>
                                    </div>
                                    <div class="col-md-5">
                                        <li>
                                            <a href="corporate-card" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/corporate-card.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4 class="link">Corporate Cards</h4>
                                                    <p>Get Business Credit Card and Enjoy Benefits</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="payroll" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/payroll.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4 class="link">Payroll</h4>
                                                    <p>Automate and Manage Payroll</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="cash-management" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/cash-management.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4 class="link">Cash Management</h4>
                                                    <p>Manage receivables, payables, & much more</p>
                                                </div>
                                            </a>
                                        </li>
                                    </div>
                                </div>
                                <div id="menu2" class="tab-pane fade">
                                    <div class="col-md-5">
                                        <li>
                                            <a href="accounting-automation" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/accounting-automation.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4 class="link">Accounting Automation</h4>
                                                    <p>Automate and manage Business Accounting</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="gst" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/gst.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4 class="link">GST</h4>
                                                    <p>Pay error-free GST in no time</p>
                                                </div>
                                            </a>
                                        </li>
                                    </div>
                                    <div class="col-md-5">
                                        <li>
                                            <a href="tax-payment" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/tax-payment.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4 class="link">Tax Payment</h4>
                                                    <p>Pay business taxes for TDS/TCS in no time</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="multibank-connect" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/multibank.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4 class="link">Multibank Connect</h4>
                                                    <p>The smart neo banking solution</p>
                                                </div>
                                            </a>
                                        </li>                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ul>
                </li>
                <li class="">
                <a href="#">Resource<span class="submenu-indicator"></span></a>
                <ul class="nav-dropdown nav-submenu1 nav-fea1 right-unset d-none nav-w2">
                    <div class="row">
                        <div class="col-md-12 cp-lr-nv">
                            <div class="tab-content">
                                <div class="tab-pane fade in active">
                                    <div class="col-md-6">
                                        <li>
                                            <a href="freelancer" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/freelancer.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4>Freelancer</h4>
                                                    <p>Get paid for your freelancing business</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="e-commerce" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/e-commerce.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4>E-commerce</h4>
                                                    <p>Grow your eCommerce business</p>
                                                </div>
                                            </a>
                                        </li>
                                    </div>
                                    <div class="col-md-6">
                                        <li>
                                            <a href="education" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/education.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4>Education</h4>
                                                    <p>Accept fees and make disbursements</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="saas" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/saas.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4>Saas</h4>
                                                    <p>Automate payments and subscriptions</p>
                                                </div>
                                            </a>
                                        </li>
                                    </div>
                                    <div class="col-md-6 cpadnav-last">
                                        <li>
                                            <a href="bfsi" class="nav-sub-a">
                                                <div>
                                                    <img src="img/navigation/bfsi.svg" />
                                                </div>
                                                <div class="nav-sub-content">
                                                    <h4>BFSI</h4>
                                                    <p>Smart payments for your financial platform</p>
                                                </div>
                                            </a>
                                        </li>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ul>
                </li>
                <li><a href="#">Developers</a></li>
                <li><a href="pricing">Pricing</a></li>
                <li class="cus-nav-li">
                <a href="#">
                    <span>Sign In <i class="bi bi-chevron-right"></i></span>
                </a>
                </li>
                </ul>
                </div>
            </nav>
        </div>
    </div>
</div>
