<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>PyThru Fraud Alert Guide</title>	
	<meta name="description" content="Learn how to report the fraud at PyThru with this guide for Fraud alerts">
	<meta name="keywords" content="Fraud Alert, Fraud Prevention, Payment Gateway frauds, online payment frauds">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/privacy.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="p-title">
					<h1>Fraud Alert</h1>
				</div>
				<div class="p-content">
					<b>DISCLAIMER: MISUSE OF OUR "PYTHRU" BRAND NAME TO CHEAT CUSTOMERS</b>
					<p>We hereby bring back your attention that unauthorized individuals or organizations are falsely using the name of " PyThru" in communications sent from fake email ids with the intention of cheating people and extorting money in lieu of service fees/charges/taxes etc. we might wish to further emphasize that PyThru doesn't charge any amount to customers or other persons on the pretext of initiating a refund.</p>
					<p>This scam has just been delivered to our notice then we've started alerting the general public to assist ensure people don't provide their tip or take actions supported these fraudulent communications. Please remember that the official email ids of PyThru will always have the endings @pythru.com, @bigobrain.co. Any communication originating from email IDs with the other endings must be deemed to be fraudulent.</p>
					<p>PyThru has already lodged a complaint with the West Bengal Police about the said fraudulent actions by hitherto unknown individuals/entities. We shall not accept any liability towards the representation made in these communications. Moreover, PyThru isn't responsible for any kind of loss or damage which will occur as a consequence of such fraudulent activities.</p>
					<p>Scams may take a myriad of forms, additionally to those described above. Thus, the necessity for caution and vigilance can't be over-emphasized. We thereby advise you to take care of unsolicited emails, phone calls and other communication that invite your personal or financial information, or request for any payment in our name.</p>
					<p>In case you're conscious of any inappropriate use of our name or If you've got any doubts about the authenticity of any correspondence purportedly from, for or on behalf of PyThru, Please click here to report our team immediately or send an e-mail to <a href="mailto:risk@pythru.com">risk@pythru.com</a> and <a href="mailto:legal@pythru.com">legal@pythru.com</a></p>
				</div>
			</div>
		</div>
	</section>


	<?php include 'include/footer.php';?>

	<?php include 'include/js.php';?>
	
</body>
</html>