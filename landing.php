<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Best Payment Gateway and Business Payments Processing| Accept Easy Online Payments</title>	
	<meta name="description" content="Free payment gateway solution with affordable transaction rates. Register for the PyThru product suitable for your business finance needs.">
	<meta name="keywords" content="Business Payments Processing, Business Finances, Smooth Payments, Payment Gateway Service Provider, Payment Methods">

	<?php include 'include/css.php';?>  
	<link rel="stylesheet" href="css/landing.css">
</head>
<body>
	<?php include 'include/header.php';?>		

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="hero">
						<div class="fl">
							<span>PYTHRU PAYMENT GATEWAY</span>
						</div>
						<div class="content">
							<h1 class="c-h1">Best Solution <br class="hidden-xs">to Process your <br class="hidden-xs"><span> Business Finances </span></h1>
						</div> 
						<div class="signup mt50">
							<a href="" class="cbtn cbtn1">Enquire Now</a>
							<a href="" class="cbtn cbtn2">Sign Up</a>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="hero-graphic hidden-sm hidden-xs">
						<img src="img/landing/hero.svg" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
		  <div class="row">
		    <div class="text-center">
		      <span class="com-span bg-white">Features</span>
		      <h2 class="c-h2 pb10">Why PyThru Payment Gateway?</h2> 
		    </div>
		  </div>
		  <div class="row">
		  	<div class="col-md-4 col-sm-6">
		  		<div class="why-us">
		  			<img src="img/landing/onboarding.svg">
		  			<h4>Smooth Onboarding</h4>
		  			<p>Sign up and activate a merchant account in no time</p>
		  		</div>
		  	</div>
		  	<div class="col-md-4 col-sm-6">
		  		<div class="why-us">
		  			<img src="img/landing/settlement.svg">
		  			<h4>Fast Settlements</h4>
		  			<p>Get fast and smooth settlements of your payments</p>
		  		</div>
		  	</div>
		  	<div class="col-md-4 col-sm-6">
		  		<div class="why-us">
		  			<img src="img/landing/dashboard.svg">
		  			<h4>Resourceful Dashboard</h4>
		  			<p>Analytics and reporting become easy with the dashboard</p>
		  		</div>
		  	</div>
		  	<div class="col-md-4 col-sm-6">
		  		<div class="why-us">
		  			<img src="img/landing/security.svg">
		  			<h4>Best Security</h4>
		  			<p>The best security compliances including the PCI DSS</p>
		  		</div>
		  	</div>
		  	<div class="col-md-4 col-sm-6">
		  		<div class="why-us">
		  			<img src="img/landing/international.svg">
		  			<h4>International Reach</h4>
		  			<p>Get the global business with the international payments</p>
		  		</div>
		  	</div>
		  	<div class="col-md-4 col-sm-6">
		  		<div class="why-us">
		  			<img src="img/landing/integration.svg">
		  			<h4>Easy Integration</h4>
		  			<p>The ready plugins and SDKs enable the smooth integration</p>
		  		</div>
		  	</div>
		  </div>
		</div>
	</section>

	<section class="sec-padding bg1 check-mar">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="lrs-pad">
						<div>
							<span class="com-span">Easy customization</span>
							<h2 class="c-h2">Easy and Smooth <br class="hidden-xs">Checkout</h2>	
							<p class="com-p pt10">Process smooth payments with the <br class="hidden-xs">PyThru’s fast and customizable checkout</p>	
							<p class="com-p pt10">High transaction success rate <br class="hidden-xs">Friendly User-interface <br class="hidden-xs">No redirects</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="check-img">
						<img src="img/landing/checkout.svg" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="sec-padding">
		<div class="container">
			<div class="row point-pad">
				<div class="left-right-section">
					<div class="col-md-6 col-sm-6">
						<div class="lrs-img">
							<img src="img/landing/multi-payment-method.svg" class="img-responsive">
						</div>
					</div>
					<div class="col-md-offset-1 col-md-5 col-sm-6">
						<div class="lrs-pad">
							<div>
								<span class="com-span">Easy customization</span>
								<h2 class="c-h2">Multiple Payment Methods</h2>	
								<p class="com-p pt10">PyThru offers multiple payment mode options.</p>	
								<p class="com-p pt10">Let your customers pay with their preferred payment method</p>	
							</div>
							<div class="col-md-6">
								<div class="points">
									<h4>Debit cards</h4>
									<h4>Credit Cards</h4>
									<h4>UPI</h4>
								</div>
							</div>
							<div class="col-md-6">
								<div class="points">
									<h4>Mobile Wallets</h4>
									<h4>Net Banking</h4>
									<h4>Single QR</h4>
								</div>
							</div>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</section>

	<section class="sec-top-padding">
		<div class="custom-bg custom-bg-gradient bg-rotate">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-sm-6">
						<div class="mt100">
							<span class="com-span color-blue">Affordable</span>
							<h2 class="c-h2 color-white">Pricing Plans</h2>	
							<p class="com-p pt10 color-white mb20">We provide the best services in two different pricing plans as per your business requirements</p>
							<div class="ig-main">
				                <div class="ig">
				                  <img src="img/common/no-hiddenfee.svg">
				                  <p>No Hidden Fees</p>
				                </div>
				                <div class="ig">
				                  <img src="img/common/no-maintanance.svg">
				                  <p>No Maintanance</p>
				                </div>
				                <div class="ig">
				                  <img src="img/common/no-setupcharge.svg">
				                  <p>No setup charges</p>
				                </div>
				            </div>
						</div>
					</div>
					<div class="col-md-offset-1 col-md-6 col-sm-6">
						<div class="plan">
							<div class="row main-plan ex-lr-margin">
								<div class="col-md-3 text-center mt15">
									<span>3%</span>
								</div>
								<div class="col-md-9">
									<h3>Standard Plan</h3>
									<p>Indian Debit Card, Net Banking, UPI, Walletsincluding Freecharge,Mobikwik, etc.</p>
								</div>							
							</div>		
							<div class="row sub-plan ex-lr-margin">
								<div class="col-md-8">
									<h3>Business Plan</h3>
									<h4>To discuss the <br class="hidden-xs">Customized Pricing</h4>
								</div>
								<div class="col-md-4">
									<a href="contact" class="cbtn3 cbtn3">Contact Us</a>								
								</div>
							</div>					
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="accordion-section clearfix mt-3 sec-padding" aria-label="Question Accordions">
	  <div class="container">	
	  <div class="col-md-offset-1 col-md-10">  
		  <h2 class="text-center faq-title">FAQs</h2>
		  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panel-default">
			  <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
				<h3 class="panel-title">
				  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
					What is PyThru Payment Gateway?
				  </a>
				</h3>
			  </div>
			  <div id="collapse0" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading0" aria-expanded="true">
				<div class="panel-body px-3 mb-4">
				  <p>PyThru lets Indian businesses accept online payments securely with multiple payment mode options. It can be integrated with the website/app or used as payment links.</p>
				  </ul>
				</div>
			  </div>
			</div>
			
			<div class="panel panel-default">
			  <div class="panel-heading p-3 mb-3" role="tab" id="heading1">
				<h3 class="panel-title">
				  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
					How easy is it to build a website with Solodev CMS?
				  </a>
				</h3>
			  </div>
			  <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
				<div class="panel-body px-3 mb-4">
				  <p>Building a website is extremely easy. With a working knowledge of HTML and CSS you will be able to have a site up and running in no time.</p>
				</div>
			  </div>
			</div>
			
			<div class="panel panel-default">
			  <div class="panel-heading p-3 mb-3" role="tab" id="heading2">
				<h3 class="panel-title">
				  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
					What is the uptime for Solodev CMS?
				  </a>
				</h3>
			  </div>
			  <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
				<div class="panel-body px-3 mb-4">
				  <p>Using Amazon AWS technology which is an industry leader for reliability you will be able to experience an uptime in the vicinity of 99.95%.</p>
				</div>
			  </div>
			</div>
			
			<div class="panel panel-default">
			  <div class="panel-heading p-3 mb-3" role="tab" id="heading3">
				<h3 class="panel-title">
				  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
					Can Solodev CMS handle multiple websites for my company?
				  </a>
				</h3>
			  </div>
			  <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
				<div class="panel-body px-3 mb-4">
				  <p>Yes, Solodev CMS is built to handle the needs of any size company. With our Multi-Site Management, you will be able to easily manage all of your websites.</p>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	  
	  </div>
	</section>

	<?php include 'include/footer.php';?>

	<?php include 'include/js.php';?>
</body>
</html>