<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>PyThru Privacy Policy</title>	
	<meta name="description" content="Privacy of information is given immense importance at PyThru. PyThru Payment Gateway is affordable with easy integration & robust APIs.">
	<meta name="keywords" content="PyThru Payment Gateway, Easy Payment Gateway, Privacy, Confidentiality, Privacy Policy">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/privacy.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="p-title">
					<h1>Privacy Policy</h1>
				</div>
				<div class="p-content">
					<p>For the purpose of the Privacy Policy, wherever the context so requires “You” or “User” shall mean any natural or legal person who visits Our platform, either just for the purpose of browsing the Website or engages to use Our Payment Platform (“Services”). The term “We”, “Us”, “Our” shall mean PyThru Fintech Services Private Limited (“PyThru”).</p>

					<h2>Section 1 – What information do we collect?</h2>
					<p><b>Personal Data</b>: We may collect your information when You use Our Services. We may collect information such as like Name, Address, and Mobile Number, e-mail Address and transaction related information. These could typically be:</p>
					<ul>
						<li>Name</li>
						<li>Telephone Number</li>
						<li>Email Address</li>
						<li>Home Address</li>
						<li>Your payment details</li>
						<li>Your IP address and</li>
						<li>Any other personal information which you give us in connection with the Services.</li>
						<li>If you send us personal correspondence, such as emails or letters, we may collect such information into a file specific to you.</li>
					</ul>
					<p><b>URLs</b>: In general, you can use our Service or you can browse Our Website without telling us who you are or revealing any personal information about yourself. Once we collect your personal information, you are not anonymous to us. Where possible, we indicate which fields are required and which fields are optional. You always have the option to not provide information for the fields that are optional. We may automatically track certain information about you based upon your behavior on Our Website / Application. We use this information to do internal research on our users’ demographics, interests, and behavior to better understand, protect and serve our users. This information is compiled and analyzed on an aggregated basis. This information may include the URL that you just came from (whether this URL is on Our Website or not), which URL you next go to (whether this URL is on Our Website or not), your browser information, and Your IP address.</p>
					<p><b>Payment Information</b>: If you transact with Us, We collect some additional information, such as a billing address, a credit / debit card number and a credit / debit card expiration date and/ or other payment instrument details and tracking information from cheques or money orders.</p>

					<h2>Section 2: What do we do with your information?</h2>
					<p><b>2.1</b> We use personal information to provide the delivery of our products to you. To the extent we use Your personal information to market to You, We will provide You the ability to opt-out of such uses. We use Your personal information to resolve disputes; troubleshoot problems; help promote a safe transaction; collect money; measure consumer interest in Our products, inform You about online and offline offers, products and updates; customize Your experience; detect and protect Us against error, fraud and other criminal activity; enforce Our terms and conditions; and as otherwise described to You at the time of collection.</p>
					<p><b>2.2</b> In Our efforts to continually improve our product offerings, we collect and analyze demographic and profile data about Our Users’ activity on Our Website. We identify and use Your IP address to help diagnose problems with our server, and to administer Our Website. Your IP address is also used to help identify you and to gather broad demographic information.</p>

					<h2>Section 3 – Consent</h2>
					<p><b>3.1</b> When You provide Us with personal information to complete a transaction, verify Your credit card, place an order, arrange for a delivery or return a purchase, it is deemed that You have consented to the use of Your personal information by Us to carry out the transaction as requested.</p>
					<p><b>3.2</b> In the course of business, if we need Your personal information for any secondary reason, We shall specify the reason for requiring such information. Upon such request, you shall have the option to refrain from revealing such information to Us.</p>
					<p><b>3.3</b> If after you consent, either implied or explicit, for Your personal information to be used by Us, You change Your mind, You may withdraw Your consent for Us to contact You, for the continued collection, use or disclosure of Your information, at any time. Such withdrawal of consent shall be communicated to Us either through an email at <a href="mailto:support@pythru.com">support@pythru.com</a> or by contacting Us at Our corporate office.</p>

					<h2>Section 4 – Disclosure of personal information</h2>
					<p><b>4.1</b> We may share personal information with our other corporate entities and affiliates to help detect and prevent identity theft, fraud and other potentially illegal acts; correlate related or multiple accounts to prevent abuse of Our Website; and to facilitate joint or co-branded services where such services are provided by more than one corporate entity.</p>
					<p><b>4.2</b> We may disclose personal information of you if required to do so by law or in the good faith belief that such disclosure is reasonably necessary to respond to subpoenas, court orders, or other legal process. We may disclose personal information to law enforcement offices, third party rights owners, or others in the good faith belief that such disclosure is reasonably necessary to enforce Our Privacy Policy; respond to claims that an advertisement, posting or other content violates the rights of a third party; or protect the rights, property or personal safety of Our Users or the general public.</p>

					<h2>Section 5 – Third party services</h2>
					<p><b>5.1</b> We may engage the services of third parties to help Us serve Our customers better. In general, the third-party services providers engaged by Us will only collect, use and disclose Your information to the extent necessary to allow them to perform the services they provide to Us. </p>
					<p><b>5.2</b> However, certain third-party service providers, such as payment gateways and other payment transaction processors, have their own privacy policies in respect to the information.</p>
					<p><b>5.3</b> In particular, remember that certain providers may be located in or have facilities that are located a different jurisdiction than either You or Us. So if You elect to proceed with a transaction that involves the services of a third-party service provider, then Your information may become subject to the laws of the jurisdiction(s) in which that service provider or its facilities are located.</p>
					<p><b>5.4</b> Once You leave Our website or are redirected to a third-party website or application, You are no longer governed by this Privacy Policy or Our website’s Terms of Service.</p>
					<p><b>5.5</b> When You click on third party links on Our Website, We are not responsible for the privacy practices of other sites and for the loss or consequential damage that may be caused to You and encourage You to read their privacy statements.</p>

					<h2>Section 6 – Security</h2>
					<p><b>6.1</b> To protect Your personal information, We take reasonable precautions and follow industry best practices to make sure it is not inappropriately lost, misused, accessed, disclosed, altered or destroyed.</p>
					<p><b>6.2</b> If You provide Us with Your payment instrument information, the information is encrypted using secure socket layer technology (SSL) and stored with an AES256 bit encryption. Although, no method of transmission over the Internet or electronic storage is 100% secure, We follow all PCI-DSS requirements and implement additional generally accepted industry standards.</p>

					<h2>Section 7 - Changes to this privacy policy</h2>
					<p><b>7.1</b> We reserve the right to modify this privacy policy at any time, so please review it frequently. Changes and clarifications will take effect immediately upon their posting on the Website. If We make material changes to this policy, We will notify You here that it has been updated, so that You are aware of what information We collect, how We use it, and under what circumstances, if any, We use and/or disclose it.</p>
					<p><b>7.2</b> If Our Website is acquired or merged with another company, Your information may be transferred to the new owners so that We may continue to provide Services to You.</p>

					<h2>CONTACT INFORMATION</h2>
					<p>For any query, grievance, complaint or any other information that may be required by You, You may contact <br class="hidden-xs">Our Privacy Compliance Officer at <a href="mailto:support@pythru.com">support@pythru.com</a>.</p>
				</div>
			</div>
		</div>
	</section>


	<?php include 'include/footer.php';?>

	<?php include 'include/js.php';?>
	
</body>
</html>