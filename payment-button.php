<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Online Payments with PyThru Payment Button on websites and blogs</title>	
	<meta name="description" content="Create copy paste code, add payment button on website and accept online payments with credit card, debit card, net banking, wallets, and UPI. ">
	<meta name="keywords" content="Integrate Payment Button, Payment Button for Online Payments, Add Payment Button, Payment Button on website">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/payment-button.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="hero">
						<div class="content pb10">
							<h1 class="c-h1">Add Pythru <span>Payment Button</span> Button to Website</h1>
						</div> 
						<div class="head-point">
							<div class="col-md-4 dis">
								<span><i class="bi bi-check"></i> No integration</span>
								<span><i class="bi bi-check"></i> Easy to create</span>
							</div>
							<div class="col-md-6 dis">
								<span><i class="bi bi-check"></i> Mobile compatible</span>
								<span><i class="bi bi-check"></i> Affordable TDR</span>
							</div>					
						</div>
						<div class="clearfix"></div>
						<div class="mt50"></div>
						<?php include 'include/common-signup.php';?>
					</div>
				</div>
				<div class="col-md-7">
					<div class="hero-graphic hidden-sm hidden-xs">
						<img src="img/payment-button/hero.svg" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="sub-point">
						<img src="img//payment-button/create.svg">
						<span>Step 1</span>
						<h3>Create</h3>
						<p>No technical knowledge required to generate a customized button code</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="sub-point">
						<img src="img//payment-button/copy.svg">
						<span>Step 2</span>
						<h3>Copy</h3>
						<p>Simply select the generated code for the payment button and copy it</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="sub-point">
						<img src="img//payment-button/add.svg">
						<span>Step 3</span>
						<h3>Add</h3>
						<p>Put this code on your website or blog and collect seamless payments</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="bg1 pad-sec-tb">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<span class="com-span bg-white">One-click Payments</span>
					<h2 class="c-h2 pb10">The All-in-One PyThru Payment Button</h2>	
					<p class="ex-p pb10">With no extra charges accept single-time online payments or subscriptions <br class="hidden-xs">or single product payments in a smart way through your website or blog.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/payment-button/payment-gateway.svg">
						<h3>Payment Gateway</h3>
						<p>Secure checkout with multiple <br class="hidden-xs">payment modes with a single click on the button</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/payment-button/custom-design.svg">
						<h3>Customized Design</h3>
						<p>The payment button can be customized as per your brand for colors, styles, text, etc.</p>
					</div>
				</div>
				<div class="clearfix visible-sm"></div>
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/payment-button/payment-type.svg">
						<h3>Payment Types</h3>
						<p>To collect recurring payments, donations, fees, single-plan or product payment, etc</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-7 col-sm-6">
					<div class="ed">
						<img src="img/payment-button/embeded-code.svg" class="img-responsive">
					</div>
				</div>
				<div class="col-md-5 col-sm-6">
					<div class="lrs-pad">
						<div>
							<span class="com-span">Future of Banking</span>
							<h2 class="c-h2">Paste the Embed Code</h2>	
							<p class="com-p pt10">Generate-Copy-Add the HTML Code for the payment button to your website or blog in a hassle-free way</p>	
						</div>
						<div class="points1">
							<h4><5-minute task</h4>
							<h4>No coding required</h4>
							<h4>Customized Button</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-top-padding">
		<div class="custom-bg custom-bg-gradient bg-rotate">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-sm-6">
						<div class="mt100">
							<span class="com-span color-blue">Easiest Integration</span>
							<h2 class="c-h2 color-white">Pricing Plans</h2>	
							<p class="com-p pt10 color-white mb20">We provide the best services in two different pricing plans as per your business requirements</p>
							<div class="ig-main">
				                <div class="ig">
				                  <img src="img/common/no-hiddenfee.svg">
				                  <p>No Hidden Fees</p>
				                </div>
				                <div class="ig">
				                  <img src="img/common/no-maintanance.svg">
				                  <p>No Maintanance</p>
				                </div>
				                <div class="ig">
				                  <img src="img/common/no-setupcharge.svg">
				                  <p>No setup charges</p>
				                </div>
				            </div>
						</div>
					</div>
					<div class="col-md-offset-1 col-md-6 col-sm-6">
						<div class="plan">
							<div class="row main-plan ex-lr-margin">
								<div class="col-md-3 text-center mt15">
									<span>3%</span>
								</div>
								<div class="col-md-9">
									<h3>Standard Plan</h3>
									<p>Indian Debit Card, Net Banking, UPI, Walletsincluding Freecharge,Mobikwik, etc.</p>
								</div>							
							</div>		
							<div class="row sub-plan ex-lr-margin">
								<div class="col-md-8">									
									<h4>Feel Free to contact <br class="hidden-xs">us for</h4>
									<h3 class="mt10">Business Plan</h3>
								</div>
								<div class="col-md-4">
									<a href="" class="cbtn3 cbtn3">Contact Us</a>								
								</div>
							</div>					
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-top-padding bot-mar">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="content m-center">
						<span class="com-span">Best Solution</span>
						<h2 class="c-h1">If you Don’t Have <br class="hidden-xs"><span>Any Website</span></h2>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="bot-sp">	
						<a href="" class="cbtn-signup">Sign Up</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="accordion-section clearfix mt0 sec-padding" aria-label="Question Accordions">
    <div class="container"> 
      <div class="col-md-offset-1 col-md-10">  
        <h2 class="text-center faq-title">FAQs</h2>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
          <h3 class="panel-title">
            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
           What is a Payment Button?
            </a>
          </h3>
          </div>
          <div id="collapse0" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading0" aria-expanded="true">
          <div class="panel-body px-3 mb-4">
            <p>The Payment Button can be added to your website or blog to accept the online payments for your business with the multiple payment modes of Pythru. When the customer clicks on it to make the payment, a secure payment checkout page opens and the customer can proceed with the payment.</p>
            </ul>
          </div>
          </div>
        </div>
        
        <div class="panel panel-default">
          <div class="panel-heading p-3 mb-3" role="tab" id="heading1">
          <h3 class="panel-title">
            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
            How to create a Payment Button?
            </a>
          </h3>
          </div>
          <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
          <div class="panel-body px-3 mb-4">
            <p>It is very easy to create a Payment Button code without any technical knowledge. You can go to your dashboard tool, create the single-line code for the payment button, copy and paste it to your website or blog. </p>
          </div>
          </div>
        </div>
        
        <div class="panel panel-default">
          <div class="panel-heading p-3 mb-3" role="tab" id="heading2">
          <h3 class="panel-title">
            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
            Can I customize the Payment Button?
            </a>
          </h3>
          </div>
          <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
          <div class="panel-body px-3 mb-4">
            <p>Yes, the payment button can be customized as per your brand and website. You can create the button with custom colors and text.</p>
          </div>
          </div>
        </div>
        
        <div class="panel panel-default">
          <div class="panel-heading p-3 mb-3" role="tab" id="heading3">
          <h3 class="panel-title">
            <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
            Are the automated receipts sent?
            </a>
          </h3>
          </div>
          <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
          <div class="panel-body px-3 mb-4">
            <p>Yes, the automated payment receipts will be sent to the customers by default. You can turn off this if you do not wish to send the automated receipts. </p>
          </div>
          </div>
        </div>
        </div>
      </div>    
    </div>
  </section>
	
	<?php include 'include/footer.php';?>

	<?php include 'include/js.php';?>
	
</body>
</html>