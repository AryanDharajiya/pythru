<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>PyThru Online Payment Gateway Pricing | Payment Gateway Charges</title>	
	<meta name="description" content="Get PyThru at affordable payment gateway pricing. No other online payment gateway charges applied. Zero setup fees & zero maintenance charge.">
	<meta name="keywords" content="payment gateway charges, payment gateway pricing India, payment gateway pricing">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/pricing.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="main-bg">
		<div class="sec-padding ph-bg">
			<div class="container sec-padding">
				<div class="row">
					<div class="text-center">
						<span class="com-span fw800">Pricing</span>
						<h1 class="text-white pt10 fw800">Why PyThru Payment Gateway?</h1>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="pr-box">
				<div class="row">
					<div class="col-md-3">
						<ul class="nav nav-pills">
						    <li class="active"><a data-toggle="pill" href="#menu00">Payment Gateway</a></li>
						    <li><a data-toggle="pill" href="#menu11">Payouts</a></li>
						    <li><a data-toggle="pill" href="#menu22">Banking</a></li>
						    <li><a data-toggle="pill" href="#menu33">Accounting</a></li>
					  	</ul>
					</div>
					<div class="col-md-9">
						<div class="tab-content">
						    <div id="menu00" class="tab-pane fade in active">
						      <div class="p-box-main">
						      	<div class="row p-box-pad">
						      		<div class="col-md-4 col-sm-4">
						      			<div class="p-box-price">
						      				<h2 class="mmt30">2.0 % </h2>
						      			</div>
						      		</div>
						      		<div class="col-md-8 col-sm-8">
						      			<div class="p-box-content">
						      				<h3 class="dpt20">Credit card</h3>
						      				<div class="p-box-img">
						      					<ul>
                                                    <li>
                                                        <img src="img/pricing/visa.svg" alt="Visa">
                                                        <p>Visa</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/rupay.svg" alt="RuPay">
                                                        <p>RuPay</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/mastercard.svg" alt="Mastercard">
                                                        <p>Mastercard</p>
                                                    </li>
                                                </ul>
						      				</div>
						      			</div>
						      			<div class="p-box-prize">
						      				<h4><span>2.95 % </span> for Amex & Diners card</h4>
						      			</div>
						      		</div>
						      		<div class="clearfix visible-xs"></div>
						      		<div class="col-sm-12">
						      			<div class="p-box-border"></div>
						      		</div>
						      	</div>
						      	<div class="row p-box-pad">
						      		<div class="col-md-4 col-sm-4">
						      			<div class="p-box-price">
						      				<h2>1.75 % </h2>
						      			</div>
						      		</div>
						      		<div class="col-md-8 col-sm-8">
						      			<div class="p-box-content">
						      				<h3>Debit card</h3>
						      				<div class="p-box-img">
						      					<ul>
                                                    <li>
                                                        <img src="img/pricing/visa.svg" alt="Visa">
                                                        <p>Visa</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/rupay.svg" alt="RuPay">
                                                        <p>RuPay</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/mastercard.svg" alt="Mastercard">
                                                        <p>Mastercard</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/maestro.svg" alt="Maestro">
                                                        <p>Maestro</p>
                                                    </li>
                                                </ul>
						      				</div>
						      			</div>
						      		</div>
						      		<div class="col-sm-12">
						      			<div class="p-box-border"></div>
						      		</div>
						      	</div>
						      	<div class="row p-box-pad">
						      		<div class="col-md-4 col-sm-4">
						      			<div class="p-box-price">
						      				<h2>1.75 % </h2>
						      			</div>
						      		</div>
						      		<div class="col-md-8 col-sm-8">
						      			<div class="p-box-content">
						      				<h3>UPI</h3>
						      				<div class="p-box-img">
						      					<ul>
                                                    <li>
                                                        <img src="img/pricing/upi.svg" alt="UPI">
                                                        <p>UPI</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/paytm.svg" alt="Paytm">
                                                        <p>Paytm</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/google-pay.svg" alt="GooglePay">
                                                        <p>GooglePay</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/phonepe.svg" alt="Phonepe">
                                                        <p>Phonepe</p>
                                                    </li>
                                                </ul>
						      				</div>
						      			</div>
						      		</div>
						      		<div class="col-sm-12">
						      			<div class="p-box-border"></div>
						      		</div>
						      	</div>
						      	<div class="row p-box-pad">
						      		<div class="col-md-4 col-sm-4">
						      			<div class="p-box-price">
						      				<h2>1.75 % </h2>
						      			</div>
						      		</div>
						      		<div class="col-md-8 col-sm-8">
						      			<div class="p-box-content">
						      				<h3>Net Banking</h3>
						      				<div class="p-box-img">
						      					<ul>
                                                    <li>
                                                        <img src="img/pricing/sbi.svg" alt="Sbi">
                                                        <p>Sbi</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/icici.svg" alt="ICICI">
                                                        <p>ICICI</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/hdfc.svg" alt="HDFC">
                                                        <p>HDFC</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/kotak.svg" alt="Kotak">
                                                        <p>Kotak</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/yes.svg" alt="Yes Bank">
                                                        <p>Yes Bank</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/axis.svg" alt="Axis">
                                                        <p>Axis</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/punjab.svg" alt="Punjab">
                                                        <p>Punjab</p>
                                                    </li>                                                       
                                                    <li>
                                                        <img src="img/pricing/otherbank.svg" alt="Other Banks">
                                                        <p>Other</p>
                                                    </li>
                                                </ul>
						      				</div>
						      			</div>
						      		</div>
						      		<div class="col-sm-12">
						      			<div class="p-box-border"></div>
						      		</div>
						      	</div>
						      	<div class="row p-box-pad">
						      		<div class="col-md-4 col-sm-4">
						      			<div class="p-box-price">
						      				<h2>1.85 % </h2>
						      			</div>
						      		</div>
						      		<div class="col-md-8 col-sm-8">
						      			<div class="p-box-content">
						      				<h3>Wallets</h3>
						      				<div class="p-box-img pb20">
						      					<ul>
                                                    <li>
                                                        <img src="img/pricing/paytm.svg" alt="Paytm">
                                                        <p>Paytm</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/phonepe.svg" alt="Phonepe">
                                                        <p>Phonepe</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/mobiquick.svg" alt="Mobikwik">
                                                        <p>Mobikwik</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/ola.svg" alt="Olamoney">
                                                        <p>Olamoney</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/freecharge.svg" alt="Freecharge">
                                                        <p>Freecharge</p>
                                                    </li>
                                                    <li>
                                                        <img src="img/pricing/jiomoney.svg" alt="Jiomoney">
                                                        <p>Jiomoney</p>
                                                    </li>
                                                </ul>
						      				</div>
						      			</div>
						      		</div>
						      	</div>
						      </div>
						      <div class="mc-ex-main">
						      	<div class="mc-ex">
						      		<div class="row">
							      		<div class="col-md-4 col-sm-4 text-center">
							      			<h3>4.0 %</h3>
							      			<span>+ Tax</span>
							      		</div>
							      		<div class="col-md-8 col-sm-8">
							      			<h4>International Multicurrency</h4>
							      			<p>Active on request</p>
							      		</div>
							      	</div>
						      	</div>
						      </div>
						    </div>
						    <div id="menu11" class="tab-pane fade">
						      <h3>Coming Soon</h3>
						    </div>
						    <div id="menu22" class="tab-pane fade">
						      <h3>Coming Soon</h3>
						    </div>
						    <div id="menu33" class="tab-pane fade">
						      <h3>Coming Soon</h3>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="pr-bg">
			<div class="container sec-padding">
				<div class="row">
					<div class="col-md-12">
						<div class="c-plan text-center">
							<span class="com-span">Business Plan</span>
							<h3>Want to customize pricing <br class="hidden-xs">for your business??</h3>
							<a href="">Talk with us</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<div>
						<span class="com-span">Benifits of us</span>
						<h2 class="c-h2">PyThru is the best choise</h2>	
						<p class="com-p pt10">PyThru offers multiple payment mode options. <br class="hidden-xs">Let your customers pay with their preferred payment method</p>	
					</div>
				</div>
				<div>
					<div class="col-md-12 col-sm-12">
						<div class="col-md-offset-3 col-md-3 col-sm-offset-2 col-sm-4">
							<div class="points">
								<h4>No setup fees</h4>
								<h4>Dedicated Manager</h4>
							</div>
						</div>
						<div class="col-md-offset-1 col-md-3 col-sm-offset-1 col-sm-4">
							<div class="points">
								<h4>24x7 Support</h4>
								<h4>Easy to Integrate</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>

	
</body>
</html>