<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

    <title>Contact PyThru - Online Payments Service Provider</title> 
    <meta name="description" content="Get in touch with PyThru and discuss your business needs to get the best online payment gateway, payments and fintech services for your business.">
    <meta name="keywords" content="Payment Gateway Service Provider, Business Banking, Fintech Services, Free Online Payments, Payment Gateway for website">

    <?php include 'include/css.php';?>  
	<link rel="stylesheet" href="css/contact.css">
</head>
<body>

	<?php include 'include/header.php';?>

    <section class="page-header single-header bg_img oh" style="background-image: url('img/contact/page-header.png');">
        <div class="bottom-shape d-none d-md-block">
            <img src="img/contact/curv.png">
        </div>
    </section>

    <section class="contact-section padding-bottom">
        <div class="container">
            <div class="section-header mw-100 cl-white">
                <h1 class="title">Connect with our Team</h1>
                <p class="mb0">Let us know if you have any queries related to payments, orders, refunds, <br class="hidden-xs"> merchant account, etc. We are happy to answer you.</p>
            </div>
            <div class="row justify-content-center justify-content-lg-between">
                <div class="col-lg-6">
                    <div class="contact-wrapper">
                        <h4 class="title text-center mb-30">Get in Touch</h4>
                        <form class="contact-form" id="contact_form_submit">
                            <div class="form-group">
                                <label for="name">Your Full Name</label>
                                <input type="text" placeholder="Enter Your Full Name" id="name" required>
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone Number</label>
                                <input type="text" placeholder="Enter Your Phone Number" id="phone" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Your Email </label>
                                <input type="text" placeholder="Enter Your Email" id="email" required>
                            </div>
                            <div class="form-group">
                                <label for="subject">Your Subject</label>
                                <input type="text" placeholder="Enter Your Subject" id="subject" required>
                            </div>
                            <div class="form-group mb-0">
                                <label for="message">Your Message </label>
                                <textarea id="message" placeholder="Enter Your Message" required></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Send Message">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-6">
                    <!-- <div class="contact-content">
                        <div class="man d-lg-block d-none">
                            <img src="img/contact/hero.svg" alt="bg">
                        </div>                        
                    </div> -->
                    <div class="col-lg-offset-1 col-lg-11">
                        <div class="contact-area">
                            <div class="contact-item">
                                <div class="contact-thumb">
                                    <img src="img/contact/email.svg" alt="contact">
                                </div>
                                <div class="contact-contact">
                                    <h5 class="subtitle">Email Us</h5>
                                    <a href="Mailto:support@pythru.com">support@pythru.com</a>
                                </div>
                            </div>
                            <div class="contact-item">
                                <div class="contact-thumb">
                                    <img src="img/contact/call.svg" alt="contact">
                                </div>
                                <div class="contact-contact">
                                    <h5 class="subtitle">Call Us</h5>
                                    <a href="Tel:565656855">565656855</a>
                                </div>
                            </div>
                            <div class="contact-item">
                                <div class="contact-thumb">
                                    <img src="img/contact/visit.svg" alt="contact">
                                </div>
                                <div class="contact-contact">
                                    <h5 class="subtitle">Visit Us</h5>
                                    <p>Srijan Corporate Park, GP Block, <br class="hidden-xs">Sector V, Bidhannagar, Kolkata,<br class="hidden-xs"> West Bengal 700091</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sec-padding">
        <div class="container">
            <div class="row">
                <div class="text-center">
                    <h2 class="c-h2 pb15">Email Us</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <div class="col-md-4">
                        <div class="email-list">
                            <img src="img/contact/general.svg">
                            <h3>General</h3>
                            <p>support@pythru.com</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="email-list">
                            <img src="img/contact/sales.svg">
                            <h3>Sales</h3>
                            <p>sales@pythru.com</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="email-list">
                            <img src="img/contact/career.svg">
                            <h3>Careers</h3>
                            <p>careers@pythru.com</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sec-padding">
        <div class="container">
            <div class="row">
                <div class="text-center">
                    <h2 class="c-h2 pb15">PyThru Offices</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-2 col-md-10">
                    <div class="address">
                        <div class="add-img">
                            <img src="img/contact/reg-add.svg">
                        </div>
                        <div class="add-content">
                            <h3>Registered Address</h3>
                            <p>Billermath, Ranaghat, Nadia, West Bengal 741201</p>
                        </div>
                    </div>
                    <div class="address">
                        <div class="add-img">
                            <img src="img/contact/off-add.svg">
                        </div>
                        <div class="add-content">
                            <h3>Office Address</h3>
                            <p>Srijan Corporate Park, GP Block, Sector V, Bidhannagar, Kolkata, West Bengal 700091</p>
                            <b>We are also located in Bengaluru and Kolkata</b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sec-padding"></section>
	
	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>

</body>
</html>