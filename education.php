<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Payment Gateway for Education Platforms | PyThru</title>	
	<meta name="description" content="Accept and disburse online payments for your Education platforms. Accept fees from students and parents and pay to staff and vendors.">
	<meta name="keywords" content="Payment Gateway for Education, Payment Gateway, Payouts, online payments">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/solutions.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="hero">
						<div class="content">
							<h1 class="c-h1">Best Payments Solution for<br class="hidden-xs"><span> Education Platforms</span></h1>
							<p class="pt10 pb30">Get PyThru for your school, college, coaching, Edtech, <br class="hidden-xs">or any other educational institute and process <br class="hidden-xs">easy payments & disbursements</p>
						</div> 
						<?php include 'include/common-signup.php';?>
					</div>
				</div>
				<div class="col-md-offset-1 col-md-6">
					<div class="hero-graphic hidden-sm hidden-xs">
						<img src="img/education/hero.svg" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-bottom-padding">
		<div class="container">
			<div class="row">
				<div class="text-center ff-title">
					<h2><b>Online</b> Fee Collection is Effortless with PyThru</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<div class="ff">
						<img src="img/education/payment-mode.svg">
						<h3>Multiple Payment modes</h3>
						<p>Provide 100+ payment options including Card, Netbanking, UPI, Wallets, etc.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="ff">
						<img src="img/education/smart-payment-link.svg">
						<h3>Smart Payment links</h3>
						<p>Send payment links to your students, customers, or parents and accept fees.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="ff">
						<img src="img/education/merchant-dashboard.svg">
						<h3>Easy Merchant Dashboard</h3>
						<p>Smart analytics and data makes the payments and tracking simple and smooth.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row point-pad">
				<div class="left-right-section">
					<div class="col-md-6 col-sm-6">
						<div class="lrs-img">
							<img src="img/education/payment-reminder.svg" class="img-responsive">
						</div>
					</div>
					<div class="col-md-5 col-sm-6">
						<div class="lrs-pad m-center">
							<div>
								<span class="com-span">Vendor Payments</span>
								<h2 class="c-h2">Make Payments to Vendors and Staff</h2>	
								<p class="com-p pt10">Pay timely salaries and make payments to various parties connected with your education institute such as staff, vendors, and service providers.</p>	
							</div>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="ff-p-bg">
			<div class="container">
				<div class="row sec-padding">
					<div class="text-center ff-title1">
						<h2><b>Pricing</b> Plans</h2>
					</div>
					<div class="fp-main">
						<div class="col-md-4 col-sm-6">
							<div class="fp">
								<div class="fp-card b1">
									<div class="fp-detail">
										<div class="col-md-4">
											<b>2.5%</b>
										</div>
										<div class="col-md-8">
											<h4>Standard Pricing</h4>
											<p>Credit & Deibt card, UPI, wallets, Net banking & QR</p>
										</div>
										<div class="col-md-offset-2 col-md-9">
											<hr class="fp-line">
										</div>
										<div class="col-md-offset-2 col-md-4 col-xs-4">
											<div class="fp-price">
												<b>₹ 0.00</b>
												<p>Setup fees</p>
											</div>
										</div>
										<div class="col-md-6 col-xs-8">
											<div class="fp-price">
												<b>₹ 0.00</b>
												<p>No annual maintainance</p>
											</div>
										</div>
									</div>
								</div>	
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="fp">
								<div class="fp-card b2">
									<div class="fp-detail">
										<div class="col-md-4">
											<b>Talk</b>
											<span>with us</span>
										</div>
										<div class="col-md-8">
											<h4>Business Plan</h4>
											<p>To discuss the Customized Pricing </p>
										</div>
										<div class="col-md-offset-2 col-md-9">
											<hr class="fp-line">
										</div>
										<div class="fp-btn">
											<a href="">Contact Us</a>
										</div>
									</div>
								</div>	
							</div>
						</div>
						<div class="col-md-4 col-sm-12">
							<div class="fp-content">
								<h3>Best services with <br class="hidden-xs">two different pricing plans </h3>
								<p>Discuss yours as per <br class="hidden-xs">your business requirements</p>
								<div class="fp-btn1">
									<a href="">Know More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="text-center ff-title pb20">
					<h2><b>What’s </b> more with PyThru Payment Gateway to enhance Learning</h2>
				</div>
			</div>
			<div class="row">
				<div class="tick">
					<div class="col-md-offset-2 col-md-3 col-sm-4">
						<h4>Smooth Onboarding</h4>
					</div>
					<div class="col-md-3 col-sm-4">
						<h4>Fast Settlements</h4>
					</div>
					<div class="col-md-3 col-sm-4">
						<h4>Multi-Currency Support</h4>
					</div>
					<div class="col-md-offset-3 col-md-3 col-sm-offset-2 col-sm-4">
						<h4>Safe and Secure</h4>
					</div>
					<div class="col-md-3 col-sm-4">
						<h4>Best Customer Support</h4>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row bg-black">
				<div class="col-md-2 hidden-xs">
					<div class="top-img">
						<img src="img/education/lock.svg" class="img-responsive">
					</div>
				</div>
				<div class="col-md-8">
					<div class="ss">
						<h2>Secure and Protected</h2>
						<p>PyThru safety and security features <br class="hidden-xs">protect you and your clients</p>
					</div>
					<div class="ss-point">
						<div class="col-md-offset-2 col-md-4 col-sm-6">
							<h4>PCI DSS Compliant</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Encryption & SSL</h4>
						</div>
						<div class="col-md-offset-2 col-md-4 col-sm-6">
							<h4>Risk Verification</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Fraud Prevention</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-offset-1 col-md-4 col-sm-5">
	            	<div class="qute m-center">
	            		<img src="img/education/qute.svg">
	            		<h2>PyThru in our <br class="hidden-xs">Merchant’s words...</h2>
	            	</div>
	            </div>
	            <div class="col-md-6 col-sm-7">
	                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	                    <!-- Wrapper for slides -->
	                    <div class="carousel-inner">
	                        <div class="item active">
	                            <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="img/home/man.svg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                    	<div class="content">
                                    		<h3>Smooth and Hassle Free</h3>
	                                    	<p>Fees collection through our coaching institute website has become so much smooth and hassle-free. This has made our admission process easier and time-saving. We now have an easy automated solution for Salary payments to the teachers, peon, canteen-boy, etc.</p>
	                                    	<br>
	                                        <h4>Sonu Kumar</h4>
	                                        <span>Doon Cars</span>
	                                    </div>
                                    </div>	                               
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="img/home/man.svg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                    	<div class="content">
                                    		<h3>Great Customer Support</h3>
	                                    	<p>The PyThru team is knowledgeable and very helpful. Onboarding and Integration were easy and fast because of that. It has become easy to accept the fees from our students with the PyThru payment links through emails and Whatsapp.</p>
	                                        <h4>Sawan Kumar</h4>
	                                        <span>Etnasasta</span>
	                                    </div>
                                    </div>	                               
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="img/home/man.svg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                    	<div class="content">
                                    		<h3>Smart Merchant Dashboard</h3>
	                                    	<p>The dashboard of PyThru has made our data and functions easy and organized. The parents of the students make the payments through the school application via the integrated payment gateway of PyThru. It is running very smoothly and the transaction success rate is amazing.</p>
	                                        <h4>Sharwan Kumar</h4>
	                                        <span>The Various Store</span>
	                                    </div>
                                    </div>	                               
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="img/home/man.svg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                    	<div class="content">
                                    		<h3>Online classes enhanced</h3>
	                                    	<p>Online classes also require a good online payment solution. We got it with the PyThru payment gateway. The dashboard and payment links have made our operations simple and easy. Our parents and students also find it nice to pay from the comfort of their homes.</p>
	                                    	<br>
	                                        <h4>Bikram Biswas</h4>
	                                        <span>Velvery Private Limited</span>
	                                    </div>
                                    </div>	                               
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-md-offset-1 col-md-5">
		                <div class="controls testimonial_control pull-right">
		                    <a class="left bi bi-chevron-left btn-default testimonial_btn fw900" href="#carousel-example-generic" data-slide="prev"></a>

		                    <a class="right bi bi-chevron-right btn-default testimonial_btn fw900" href="#carousel-example-generic" data-slide="next"></a>
		                </div>
		            </div>
	            </div>
	        </div>
	    </div>
	</section>

	<section class="sec-padding">
	    <div class="container">
	        <div class="row">
	        	<div class="ff-box-main">
	        		<div class="col-md-offset-1 col-md-5 col-sm-6">
	        			<div class="ff-box">
	        				<div class="ff-box-img">
		        				<img src="img/education/red-circle.svg">
		        				<h3>Risk Management <br class="hidden-xs">software</h3>
	        				</div>
	        				view product
	        			</div>
	        		</div>
	        		<div class="col-md-5 col-sm-6">
	        			<div class="ff-box">
	        				<div class="ff-box-img">
		        				<img src="img/education/blue-circle.svg">
		        				<h3>Risk Management <br class="hidden-xs">software</h3>
	        				</div>
	        				view product
	        			</div>
	        		</div>
	        	</div>
	        </div>
	    </div>
	</section>

	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>

	
</body>
</html>