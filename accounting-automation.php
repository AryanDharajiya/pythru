<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Best Automated Accounting Software | PyThru Business Solution</title>	
	<meta name="description" content="Get the automated accounting solution with Pythru for the smooth management of business finances, accounting, bookkeeping, management of Receivables & Payables, etc.">
	<meta name="keywords" content="Automated accounting software, Receivables, Payables, Reporting, Reconciliation, e-Way bills, Bookkeeping">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/banking.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="main-bg">
		<div class="sec-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="hero sec-padding">
							<div class="content">
								<h1 class="c-h1">The Best <span>Automated Accounting</span> Services For your Business Finances</h1>
								<p class="pt10">Pythru GST compliant accounting software to automate your <br class="hidden-xs">business accounting for bookkeeping, invoices, accounts <br class="hidden-xs">reconciliation, and more.</p>
							</div> 
						</div>
					</div>
					<div class="col-md-6">
						<div class="hero-graphic hidden-sm hidden-xs">
							<img src="img/accounting-automation/hero.svg" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="bgfb">
		<div class="sec-padding">
			<div class="container">
				<div class="row">
					<div class="text-center">
						<div class="km m-tc">
							<h2>An all-in-one Accounting tool <br class="hidden-xs"> for your Business Needs</h2>
							<p>Simplest and easiest way to manage<br class="hidden-xs">  your business finances</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<div class="bf-main">
							<div class="bft">
								<img src="img/accounting-automation/invoice.svg">								
								<h3>Hasslefree <br class="hidden-xs">Invoicing</h3>
							</div>
							<div class="bfc">
								<p>Get paid easily and instantly with our easy to create GST compliant invoices</p>
							</div>
						</div>
					</div>
					<div class="col-sm-4"> 
						<div class="bf-main">
							<div class="bft">
								<img src="img/accounting-automation/payment.svg">								
								<h3>Account Receivables & <br class="hidden-xs">Payables made smoother</h3>
							</div>
							<div class="bfc">
								<p>Track and manage the receivables and payables seamlessly and effortlessly</p>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="bf-main">
							<div class="bft">
								<img src="img/accounting-automation/report.svg">								
								<h3>Insightful <br class="hidden-xs">Reporting</h3>
							</div>
							<div class="bfc">
								<p>Keep track of the business health with the detailed accounting reports and statements</p>
							</div>
						</div>
					</div>
					<div class="col-sm-offset-2 col-sm-4">
						<div class="bf-main">
							<div class="bft">
								<img src="img/accounting-automation/recon.svg">								
								<h3>Simple Reconciliation <br class="hidden-xs">of Payments</h3>
							</div>
							<div class="bfc">
								<p>Reconcile and keep a smart track of your payments and be up-to-date</p>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="bf-main">
							<div class="bft">
								<img src="img/accounting-automation/eway-bill.svg">								
								<h3>e-Way Bills in <br class="hidden-xs">a click</h3>
							</div>
							<div class="bfc">
								<p>Generate automated e-Way Bills with the invoice generation for your consignments</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class=" sec-padding">      
        <div class="bg-black">
            <div class="container sec-padding">
                <div class="row"> 
                    <div class="text-center">
                        <div class="s3c1">
                            <img src="img/payroll/dot-frame.svg" class="hidden-xs hidden-sm">
                            <h2 class="m-tc">Why choose Pythru’s <br class="hidden-xs">Automated Accounting?</h2>
                        </div>
                    </div> 
                </div>
                <div class="row"> 
                    <div class="col-md-3 col-sm-6">
                        <div class="aac">
                        	<img src="img/accounting-automation/easy-and-reliable.svg">
                        	<h3>Easy and Reliable</h3>
                        	<p>It is simple to set up & use, designed by experts, and safe and secure for your data protection</p>
                        </div>
                    </div> 
                    <div class="col-md-3 col-sm-6">
                        <div class="aac">
                        	<img src="img/accounting-automation/complete-solution.svg">
                        	<h3>Complete Solution</h3>
                        	<p>Pythru accounting handles complete accounting tasks from your business orders to invoices</p>
                        </div>
                    </div> 
                    <div class="col-md-3 col-sm-6">
                        <div class="aac">
                        	<img src="img/accounting-automation/gst-compliance.svg">
                        	<h3>GST compliance</h3>
                        	<p>Complete GST compliance from invoices, liability, tax returns to getting paid with this tool</p>
                        </div>
                    </div> 
                    <div class="col-md-3 col-sm-6">
                        <div class="aac">
                        	<img src="img/accounting-automation/customizable.svg">
                        	<h3>Customizable</h3>
                        	<p>A solution for all types of business from startup to an SME to manage your accounting needs</p>
                        </div>
                    </div>                                  
                </div>
            </div>
        </div>           
    </section>

    <section class="sec-padding">
    	<div class="container">
    		<div class="row">
    			<div class="text-center km">
    				<span>Features</span>
					<h2>More Enhanced Pythru <br class="hidden-xs">Accounting Tool Features</h2>
    			</div>
    		</div>
    		<div class="row">
    			<div class="s4-tab-main">
	    			<div class="col-md-offset-1 col-md-6 mt30">
	    				<div class="tab-content">
						    <div id="first" class="tab-pane fade in active">
						      <img src="img/accounting-automation/bookkeeping-and-reporting.svg" class="img-responsive">
						    </div>
						    <div id="second" class="tab-pane fade">
						      <img src="img/accounting-automation/invoicing.svg" class="img-responsive">
						    </div>
						    <div id="third" class="tab-pane fade">
						      <img src="img/accounting-automation/receivables-and-payables.svg" class="img-responsive">
						    </div>
						 </div>
	    			</div>
	    			<div class="col-md-5">
	    				<ul class="nav nav-tabs">
						    <li class="active">
						    	<a data-toggle="tab" href="#first">
						    		<h3>Bookkeeping and Reporting</h3>
						    		<p>Get robust Bookkeeping with integrated banking and the Tally plugin. Enable easy reconciliation and powerful reporting for cash flow, profit and loss statements, etc.</p>
						    	</a>
						    </li>
						    <li>
						    	<a data-toggle="tab" href="#second">
						    		<h3>Invoicing</h3>
						    		<p>Create GST-compliant invoices, track them efficiently, and get paid with various payment options instantly.Send payment links along with the invoices</p>
						    	</a>
						    </li>
						    <li>
						    	<a data-toggle="tab" href="#third">
						    		<h3>Receivables and Payables</h3>
						    		<p>Manage the account payables and receivables efficiently.Generate GST compliant invoices, get paid with multiple payment options, send the bills and estimates, and make the payouts.</p>
						    	</a>
						    </li>
						</ul>
	    			</div>
	    		</div>
    		</div>
    	</div>
    </section>

    <section class="sec-padding">
    	<div class="bg-blue">
    		<div class="container">
    			<div class="row">
    				<div class="cu-main">
    					<div class="col-md-5">
    						<div class="cu-main-text">
    							<h2>Pythru Accounting Automation</h2>
    							<a href="#">Contact Us <i class="bi bi-arrow-right"></i></a>
    						</div>
    					</div>
    					<div class="col-md-7">
    						<div class="cu-img hidden-xs">
    							<img src="img/payroll/image 37.svg" class="img-responsive">
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>

    <section class="sec-padding">
    	<div class="container">
    		<div class="row">
    			<div class="text-center km mb0">
    				<span>Lorem ipsum</span>
					<h2>Get Started with Pythru <br class="hidden-xs">Automated Accounting Software<h2>				
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-md-offset-1 col-md-10">
					<div class="tick">
						<div class="col-md-4 col-sm-6">
							<h4>Create invoices</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Multiple payment options </h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Pay vendor bills & expenses</h4>
						</div>
						<div class="col-md-offset-2 col-md-4 col-sm-6">
							<h4>Get Payment reminders</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Track Payments & Expenses</h4>
						</div>
					</div>
				</div>
			</div>
    	</div>
    </section>    

	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>

	
</body>
</html>