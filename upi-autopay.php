<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'include/meta.php';?>

    <title>UPI Autopay with PyThru Subscriptions | UPI Recurring Payments</title>
    <meta name="description" content=" Start the Recurring billing for your subscription payments with PyThru UPI AutoPay. Accept automatic scheduled recurring payments from your customers with UPI."/>
    <meta name="keywords" content="PyThru UPI AutoPay, UPI payments, recurring payments, Recurring Billing, PyThru Subscriptions, Subscription Payments Solution, Recurring Billing Platform in India"/>

    <?php include 'include/css.php';?>
    <link rel="stylesheet" href="css/upi-autopay.css" />
</head>
<body>
    <?php include 'include/header.php';?>

    <section>
        <div class="bg">
            <div class="container sec-padding">
                <div class="row sec-padding">
                    <div class="col-md-6">
                        <div class="hero">
                            <div class="content">
                                <h1 class="c-h1"><span>UPI AutoPay </span>for Subscription Payments With PyThru</h1>
                                <p class="pt10 pb20 content-p">Sign up now to accept the recurring payments easily and provide the best <br class="hidden-xs">experience to your customers with our enhanced subscription product.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-offset-1 col-md-5">
                        <div class="hero-graphic hidden-sm hidden-xs">
                            <img src="img/upi-autopay/hero.svg" class="img-responsive" />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="why-us why-us-pad">
                            <div class="col-md-6">
                                <div class="w-content">
                                    <h3>Get Started with PyThru Subscriptions</h3>
                                    <p>Accept online payments for your Subscription-based business with this modern and advanced payment technology.</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="w-btn">
                                    <a href="">
                                        <span>Sign up</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>

    <section class="sec-top-padding">
        <div class="container sec-padding">
            <div class="row">
                <div class="text-center">
                    <h2 class="c-h2 mt0">with PyThru’s UPI AutoPay</h2>   
                    <p class="com-p pt10">PyThru is on the go to make the recurring payments<br class="hidden-xs"> the easiest for you with these advanced features of UPI AutoPay</p> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="features">
                        <img src="img/upi-autopay/f1.svg">
                        <p>Track and Analyze Transactions</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="features">
                        <img src="img/upi-autopay/f2.svg">
                        <p>Collect instant payments anytime</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="features">
                        <img src="img/upi-autopay/f3.svg">
                        <p>Send Pre-debit <br class="hidden-xs">Notification</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="features">
                        <img src="img/upi-autopay/f4.svg">
                        <p>Update the Subscription Plan</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="features">
                        <img src="img/upi-autopay/f5.svg">
                        <p>Facilitate the Retry <br class="hidden-xs">Option</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="features">
                        <img src="img/upi-autopay/f6.svg">
                        <p>Auto-debit below <br class="hidden-xs">2000</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sec-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="lrs-img">
                        <img src="img/upi-autopay/features.svg" class="img-responsive"/>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="lrs-pad">
                        <div>
                            <span class="com-span">PyThru for U</span>
                            <h2 class="c-h2">Get started with this easiest solution for your recurring payments</h2>
                            <p class="com-p pt10">Accept them conveniently with the most popular payment mode, i.e. UPI, through smooth integration and with all major UPI Apps - BHIM, AXIS, ICICI, Paytm, and more. With Google Pay, Phonepe, etc coming soon in support.</p>
                        </div>
                        <div class="f-desc">
                            <h3>Send</h3>
                            <p>Send the customer the UPI mandate request</p>

                            <h3>Accept</h3>
                            <p>The customer authorizes the mandate request</p>

                            <h3>Start</h3>
                            <p>Get Started and accept recurring payments</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-lite">
        <div class="container">
            <div class="row sec-padding">
                <div class="text-center">
                    <h2 class="c-h2 pt0">Set up the UPI AutoPay and <br class="hidden-xs">Grow your Subscription Business</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="upi-content uc-float">
                        <h3>Financial Services</h3>
                        <img src="img/upi-autopay/financial.svg">
                    </div>
                    <div class="upi-content uc-pad">
                        <h3>Memberships/Content</h3>
                        <img src="img/upi-autopay/membership.svg">
                    </div>
                    <div class="upi-content uc-pad uc-float">
                        <h3>OTT/Entertainment</h3>
                        <img src="img/upi-autopay/entertainment.svg">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="upi-img">
                        <img src="img/upi-autopay/upi.svg" class="upi-img-pad img-responsive">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="upi-content mpt30">                        
                        <img src="img/upi-autopay/travelling.svg">
                        <h3>Travelling/Transport</h3>
                    </div>
                    <div class="upi-content uc-pad uc-float">                        
                        <img src="img/upi-autopay/education.svg">
                        <h3>Education/Fees</h3>
                    </div>
                    <div class="upi-content uc-pad">                        
                        <img src="img/upi-autopay/electricity.svg">
                        <h3>Mobile/Electricity Bills</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="">
        <div class="bg1">
            <div class="container">
                <div class="row sec-padding">
                    <div class="col-md-5">
                        <div class="tc">
                            <h2>
                                <span>Connect</span> with <br class="hidden-xs" />
                                our team
                            </h2>
                        </div>
                    </div>
                    <div class="col-md-7 pt30">
                        <div class="col-md-6">
                            <div class="tc-input">
                                <input type="email" name="email" placeholder="Enter email" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="tc-btn">
                                <a href="">Send mail</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="accordion-section clearfix mt-3 sec-padding" aria-label="Question Accordions">
        <div class="container">
            <div class="col-md-offset-1 col-md-10">
                <h2 class="text-center faq-title">FAQs</h2>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
                            <h3 class="panel-title">
                                <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
                                    What is PyThru UPI AutoPay?
                                </a>
                            </h3>
                        </div>
                        <div id="collapse0" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading0" aria-expanded="true">
                            <div class="panel-body px-3 mb-4">
                                <p>The UPI AutoPay with PyThru enables the merchant to receive UPI recurring payments with a single integration. The merchant can set the recurring e-mandates with the most popular payment method today i.e. UPI, resulting in the growth of the business. The maximum payment amount per transaction can be Rs 2000, as per the RBI rules.</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading p-3 mb-3" role="tab" id="heading1">
                            <h3 class="panel-title">
                                <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    What can be the duration or period of the subscription payment?
                                </a>
                            </h3>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                            <div class="panel-body px-3 mb-4">
                                <p>The merchant can set the debit for flexible periods such as daily, weekly, monthly, yearly, or customized.</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading p-3 mb-3" role="tab" id="heading2">
                            <h3 class="panel-title">
                                <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                    Do you need a bank account? What types of accounts are supported?
                                </a>
                            </h3>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                            <div class="panel-body px-3 mb-4">
                                <p>Yes, you can register the mandate on Savings, Current, or Overdraft Account.</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading p-3 mb-3" role="tab" id="heading3">
                            <h3 class="panel-title">
                                <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                    Is there a notification system for registration and payment deduction?
                                </a>
                            </h3>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                            <div class="panel-body px-3 mb-4">
                                <p>When the mandate is registered the notification is received in real-time and also, pre-debit notification is sent before 24 hours before deducting the due amount.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sec-padding"></section>

    <?php include 'include/footer.php';?>

    <?php include 'include/js.php';?>
</body>
</html>
