<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Easy and Quick Tax Payments | PyThru</title>	
	<meta name="description" content="Instant tax payments for TDS/TCS with automatic calculations and deductions, vendor payments, autosaved tax challans, and a smart dashboard">
	<meta name="keywords" content="Instant Tax Payment, TDS Payment">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/banking.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="main-bg">
		<div class="sec-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="hero sec-padding">
							<div class="content">
								<h1 class="c-h1">Easy and Quick <br class="hidden-xs"><span>Tax Payments </span></h1>
								<p class="pt10">Safe and Secure tax payments for all types of businesses <br class="hidden-xs">from SMEs, startups to large companies</p>
							</div> 
						</div>
					</div>
					<div class="col-md-6">
						<div class="hero-graphic hidden-sm hidden-xs">
							<img src="img/tax-payment/hero.png" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<div class="km m-tc">
						<h2>All-around versatile Tax Payment software <br class="hidden-xs"> for your smooth Business Finance</h2>
						<p class="pt5">Seamlessly pay your taxes with our multi-featured Tool</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5">
					<div class="tp-img">
						<img src="img/tax-payment/software.svg" class="img-responsive">
					</div>
				</div>
				<div class="col-md-7">
					<div class="col-md-6">
						<div class="tpc">
							<div class="tpc-img">
								<img src="img/tax-payment/tax-form.svg">
								<h3>Ready Tax Forms</h3>
							</div>
							<p>TDS/TCS payments in seconds with the already filled ready forms</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="tpc">
							<div class="tpc-img">
								<img src="img/tax-payment/tax-challan.svg">
								<h3>Saved Tax Challans</h3>
							</div>
							<p>All the challans are auto-saved and available on the dashboard</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="tpc">
							<div class="tpc-img">
								<img src="img/tax-payment/team-up.svg">
								<h3>Team-up with Accountant</h3>
							</div>
							<p>The owner and accountant can easily work together</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="tpc">
							<div class="tpc-img">
								<img src="img/tax-payment/auto-calculation.svg">
								<h3>Auto-calculation</h3>
							</div>
							<p>Automatic calculation and payments of tax for the payments to vendors</p>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</section>

	<section class=" sec-padding">      
        <div class="bg-black">
            <div class="container sec-padding">
                <div class="row"> 
                    <div class="text-center">
                        <div class="s3c1 s3c11">
                            <img src="img/payroll/dot-frame.svg" class="hidden-xs hidden-sm">
                            <h2 class="m-tc">Hasslefree Process <br class="hidden-xs">to Get Started</h2>
                        </div>
                    </div> 
                </div>
                <div class="row"> 
                    <div class="col-md-offset-1 col-md-3 col-sm-4">
                        <div class="aac aacc">
                        	<img src="img/tax-payment/signup-activation.svg">
                        	<h3>Signup & activation</h3>
                        	<p>Register and get your account activated in no time</p>
                        </div>
                    </div> 
                    <div class="col-md-offset-1 col-md-3 col-sm-4">
                        <div class="aac aacc">
                        	<img src="img/tax-payment/require-detail.svg">
                        	<h3>Add required details</h3>
                        	<p>Add TAN, address, and your <br class="hidden-xs">TDS details</p>
                        </div>
                    </div> 
                    <div class="col-md-offset-1 col-md-3 col-sm-4">
                        <div class="aac aacc">
                        	<img src="img/tax-payment/make-payment.svg">
                        	<h3>Make payment</h3>
                        	<p>Pay the tax or can save the <br class="hidden-xs">draft for later</p>
                        </div>
                    </div>                                  
                </div>
            </div>
        </div>           
    </section>

    <section class="sec-padding">
    	<div class="container">
    		<div class="row">
    			<div class="text-center km">
					<h2 class="mt0 pt0">Best Security Compliances for your <br class="hidden-xs">Business Finances with Pythru</h2>
    			</div>
    		</div>
    	</div>
    </section>

    <section class="sec-padding">
    	<div class="bg-blue">
    		<div class="container">
    			<div class="row">
    				<div class="cu-main">
    					<div class="col-md-5">
    						<div class="cu-main-text">
    							<h2>Pythru Accounting Automation</h2>
    							<a href="#">Contact Us <i class="bi bi-arrow-right"></i></a>
    						</div>
    					</div>
    					<div class="col-md-7">
    						<div class="cu-img hidden-xs">
    							<img src="img/payroll/image 37.svg" class="img-responsive">
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>

    <section class="sec-padding">
    	<div class="container">
    		<div class="row">
    			<div class="text-center km mb0">
    				<span>Lorem ipsum</span>
					<h2>Why choose Tax Payments <br class="hidden-xs">with Pythru?<h2>				
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-md-offset-1 col-md-10">
					<div class="tick">
						<div class="col-md-4 col-sm-6">
							<h4>Safe and secure</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>One-stop Dashboard</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Save time and labor</h4>
						</div>
						<div class="col-md-offset-2 col-md-4 col-sm-6">
							<h4>Robust APIs</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Seamless Payouts</h4>
						</div>
					</div>
				</div>
			</div>
    	</div>
    </section>   

    <section class="accordion-section clearfix mt0 sec-padding" aria-label="Question Accordions">
	  <div class="container">	
		  <div class="col-md-offset-1 col-md-10">  
			  <h2 class="text-center faq-title mt0">FAQs</h2>
			  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
						What is the use of Pythru’s tax payments?
					  </a>
					</h3>
				  </div>
				  <div id="collapse0" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading0" aria-expanded="true">
					<div class="panel-body px-3 mb-4">
					  <p>With this Pythru’s tax payment software, the merchants falling in various categories can pay the TDS easily in seconds.</p>
					  </ul>
					</div>
				  </div>
				</div>
				
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading1">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
						How to pay the taxes with this product of Pythru for Tax Payments?
					  </a>
					</h3>
				  </div>
				  <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
					<div class="panel-body px-3 mb-4">
					  <p>It is easy to pay taxes with our tax payment application. After the merchant signs up and gets the account activated, they can go to the dashboard and proceed with the TDS/TCS payment after adding the necessary details. TAN and address will be saved and used for all the TDS payments making it time-saving.</p>
					</div>
				  </div>
				</div>
				
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading2">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
						Where are the tax challans available?
					  </a>
					</h3>
				  </div>
				  <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
					<div class="panel-body px-3 mb-4">
					  <p>The tax challans are autosaved and available on the dashboard. The past and recent challans can be filtered and accessed with various filter options.</p>
					</div>
				  </div>
				</div>
				
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading3">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
						I need to team up with my accountant for tax payment, can I do that?
					  </a>
					</h3>
				  </div>
				  <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
					<div class="panel-body px-3 mb-4">
					  <p>Yes, the accountants can be assigned the role for the TDS/TCS entries and the owners can make their payments together. Payments for the multiple entries can be done together before the due date.</p>
					</div>
				  </div>
				</div>
			  </div>
		  </div>	  
	  </div>
	</section>

	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>
	
</body>
</html>