<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Business Banking with smart Cash Management Software | Pythru</title>	
	<meta name="description" content="Get started with PayThru Cash Management Solution for management of cash, accounts receivable, payables, bank reconciliation, accounting, expenses, and reimbursements.">
	<meta name="keywords" content="Cash Management Software, Cash Management System, Cash Management Service, Cash flow, Cash Management Software">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/banking.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="main-bg">
		<div class="sec-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						<div class="hero sec-padding">
							<div class="content">
								<h1 class="c-h1">The Advanced <br class="hidden-xs"><span>Cash Management </span>Tool With PyThru</h1>
								<p class="pt10">Advanced software to manage the liquidity and enhance the cash flow with the automation of the cash management</p>
							</div> 
						</div>
					</div>
					<div class="col-md-offset-1 col-md-6">
						<div class="hero-graphic hidden-sm hidden-xs">
							<img src="img/cash-management/hero.png" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">				
			<div class="row">
				<div class="km m-tc text-center">
					<span>Lorem ipsum</span>
					<h2>Why PyThru Cash <br class="hidden-xs">Management Software?</h2>
				</div>
			</div>				
			<div class="row">
				<div class="col-md-4">
					<div class="cm">
						<img src="img/cash-management/asset1.svg">
						<h3>Make cash position <br class="hidden-xs">stronger</h3>
					</div>
				</div>
				<div class="col-md-4">
					<div class="cm">
						<img src="img/cash-management/asset2.svg">
						<h3>Take productive financial decisions</h3>
					</div>
				</div>
				<div class="col-md-4">
					<div class="cm">
						<img src="img/cash-management/asset3.svg">
						<h3>Make hasslefree <br class="hidden-xs">payments</h3>
					</div>
				</div>
				<div class="col-md-4">
					<div class="cm">
						<img src="img/cash-management/asset4.svg">
						<h3>Improve operational <br class="hidden-xs">efficiency</h3>
					</div>
				</div>
				<div class="col-md-4">
					<div class="cm">
						<img src="img/cash-management/asset5.svg">
						<h3>Bring down the Operating Cost & Time</h3>
					</div>
				</div>
				<div class="col-md-4">
					<div class="cm">
						<img src="img/cash-management/asset6.svg">
						<h3>Shows all Transactions <br class="hidden-xs">in Real-time</h3>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class=" sec-padding">      
        <div class="bg-black">
            <div class="container sec-padding">
                <div class="row"> 
                    <div class="col-md-3 col-sm-12">
                        <div class="s3c">
                            <img src="img/payroll/dot-frame.svg" class="hidden-xs hidden-sm">
                            <h2 class="m-tc">Get Started!</h2>
                        </div>
                    </div>  
                    <div class="col-md-9">
                        <div class="col-md-4 col-sm-4">
                            <div class="cmgs">
                            	<div class="cmgsi">
                            		<img src="img/cash-management/step1.svg">
                            		<span></span>
                            		<h3>Step 1</h3>
                            	</div>
                            	<div class="cmgsc">
                            		<p>Sign up with the required basic details and Get the KYC done.</p>
                            	</div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="cmgs">
                            	<div class="cmgsi">
                            		<img src="img/cash-management/step2.svg">
                            		<span></span>
                            		<h3>Step 2</h3>
                            	</div>
                            	<div class="cmgsc">
                            		<p>Get a bank account with a credit card and Start transacting.</p>
                            	</div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="cmgs">
                            	<div class="cmgsi">
                            		<img src="img/cash-management/step3.svg">
                            		<span></span>
                            		<h3>Step 3</h3>
                            	</div>
                            	<div class="cmgsc">
                            		<p>Upgrade with higher limits and advanced features.</p>
                            	</div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>           
    </section>

    <section class="sec-padding">
		<div class="container">				
			<div class="row">
				<div class="km m-tc text-center">
					<span>Lorem ipsum</span>
					<h2>All-in-one Multiple Functions with <br class="hidden-xs">Our Cash Management System</h2>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-offset-1 col-md-10">
					<div class="col-md-4 col-sm-6">
						<div class="cms"> 
							<img src="img/cash-management/cms1.svg">
							<p>Automate account <br class="hidden-xs">payables and receivables <br class="hidden-xs">easily</p>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="cms">
							<img src="img/cash-management/cms2.svg">
							<p>Send invoices and <br class="hidden-xs">emails to customers and <br class="hidden-xs">others</p>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="cms">
							<img src="img/cash-management/cms3.svg">
							<p>Manage the import and <br class="hidden-xs">reconciliation of <br class="hidden-xs">transactions</p>
						</div>
					</div>
					<div class="col-md-offset-2 col-md-4 col-sm-6">
						<div class="cms">
							<img src="img/cash-management/cms4.svg">
							<p>Keep the balance <br class="hidden-xs">Cash-in-hand and bank <br class="hidden-xs">before you</p>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="cms">
							<img src="img/cash-management/cms5.svg">
							<p>Automate the creation of book-keeping and accounting</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="bg-light">
			<div class="container">
				<div class="row sec-padding">
					<div class="col-md-4">
						<div class="km m-tc">
							<span>Lorem ipsum</span>
							<h2>Accounting & Reconciliation</h2>
							<p class="pb0">An efficient tool with the smart features of Accounting and automatic reconciliation with Bank transactions</p>
						</div>
					</div>
					<div class="col-md-offset-1 col-md-7">
						<div class="points">
							<h4>Bookkeeping for Profit & Loss, Balance Sheet, Cash Statement, etc.</h4>
							<h4>Integrate the available accounting data with a third party software</h4>
							<h4>Add all the bank accounts and synchronize the bank transactions</h4>
							<h4>Track the customer payments made using the virtual accounts</h4>
							<h4>Automated accurate reconciliation of the cash</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="km m-tc text-center">
					<h2 class="pt0 mt0">Expense and Reimbursement</h2>
					<p>Cash payment, Expense management, Approval, and Reimbursements are <br class="hidden-xs">made easy with this all-around smart tool with PyThru</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="cm11">
						<img src="img/cash-management/track.svg">
						<p>Track and limit the expenses of the employees</p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="cm11">
						<img src="img/cash-management/credit-card.svg">
						<p>Obtain custom Credit Card for business</p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="cm11">
						<img src="img/cash-management/upload-file.svg">
						<p>Upload receipts and file the reports of the expenses efficiently</p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="cm11">
						<img src="img/cash-management/virtual-card.svg">
						<p>Get Virtual Card for employees expenses with limits</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>	
</body>
</html>