<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>PyThru - Best Payment Gateway and Business Finance Solution - India</title>
	<meta name="description" content="Get eCommerce Payment Gateway for secure Online Payments with multiple payment modes for Indian Businesses. Accept credit card payments">
    <meta name="keywords" content="Online Payment Gateway, Payment Gateway in India, Online Payment Solution">

	<?php include 'include/css.php';?> 

	<link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/home.css">
</head>
<body>
	<?php include 'include/header.php';?>		

	<section class="bg">
		<div class="container sec-padding">
			<div class="row">
				<div class="col-md-5">
					<div class="hero">
						<div class="fl">
							<span>AUTOMATE YOUR PAYMENTS WITH US</span>
						</div>
						<div class="content">
							<h1 class="c-h1">The Best Partner <br class="hidden-xs">for your <span>Business </span><br class="hidden-xs">Finances</h1>
							<p class="pt10 pb20">Accept Payments, Make Payouts, Easy Banking & Accounting</p>
						</div> 
						<div class="signup">
							<input type="email" name="email" placeholder="Email address" required>
							<a href=""><img src="img/home/Group 3.svg"></a>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="hero-graphic hidden-sm hidden-xs">
						<img src="img/home/hero-pythru.svg" class="img-responsive hero-img">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-cb-pad">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<span class="com-span">Big New Innovation</span>
					<h2 class="c-h2">Fintech with PyThru</h2>	
					<p class="com-p pt10">Join any or all of our product suite and let us help you <br class="hidden-xs">with your business needs</p>	
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<div class="f1 text-center">
						<div class="f1-img">
							<img src="img/home/payment.svg">
						</div>
						<div class="f1-content">
							<h3>PyThru Payment</h3>
							<p>Accept online payments easily from your customers</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="f1 text-center">
						<div class="f1-img">
							<img src="img/home/bank.svg">
						</div>
						<div class="f1-content">
							<h3>PyThru Banking</h3>
							<p>Smooth banking functions and automatic operations</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="f1 text-center">
						<div class="f1-img">
							<img src="img/home/payout.svg">
						</div>
						<div class="f1-content">
							<h3>PyThru Payout</h3>
							<p>Automate payments disbursement to third parties, clients, etc.</p>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-offset-2 col-md-3">
					<div class="f2-title">
						<h2 class="c-h2">Our <br class="hidden-xs hidden-sm">Products</h2>	
						<span class="f2-title-line"></span>
						<p class="com-p pt25 mpb25">Here are some of the PyThru Payment <br class="hidden-xs">products for your business <br class="hidden-xs"> payments as per your business <br class="hidden-xs">style and needs</p>
					</div>
				</div>
				<div class="col-md-7 col-sm-12 bg-purple pl0 pr0 f2-border-radious f2-c-pad">
					<div class="f2-main">
						<div class="col-md-offset-1 col-md-5 col-sm-6">
							<div class="f2">
								<div class="f2-img">
									<img src="img/home/payment-gateway.svg">
								</div>
								<div class="f2-content color-white">
									<h4 class="mt0">Payment Gateway</h4>
									<p>Receive online payments securely on your website or mobile app</p>
								</div>
							</div>
						</div>
						<div class="col-md-5 col-sm-6">
							<div class="f2">
								<div class="f2-img">
									<img src="img/home/payment-button.svg">
								</div>
								<div class="f2-content color-white">
									<h4 class="mt0">Payment Button</h4>
									<p>Add Pay button on your website without any technical know-how</p>
								</div>
							</div>
						</div>
						<div class="col-md-offset-1 col-md-5 col-sm-6">
							<div class="f2">
								<div class="f2-img">
									<img src="img/home/payment-link.svg">
								</div>
								<div class="f2-content color-white">
									<h4 class="mt0">Payment Link</h4>
									<p>Website or mobile app is not compulsory to accept online payments</p>
								</div>
							</div>
						</div>
						<div class="col-md-5 col-sm-6">
							<div class="f2">
								<div class="f2-img">
									<img src="img/home/payment-page.svg">
								</div>
								<div class="f2-content color-white">
									<h4 class="mt0">Payments Pages</h4>
									<p>Custom pages as your own online store to receive payments without a website</p>
								</div>
							</div>
						</div>
						<div class="col-md-offset-1 col-md-5 col-sm-6">
							<div class="f2">
								<div class="f2-img">
									<img src="img/home/invoice.svg">
								</div>
								<div class="f2-content color-white">
									<h4 class="mt0">Invoices</h4>
									<p>Create a GST return compliant invoice and accept fast payments with it</p>
								</div>
							</div>
						</div>
						<div class="col-md-5 col-sm-6">
							<div class="f2">
								<div class="f2-img">
									<img src="img/home/upi-auto.svg">
								</div>
								<div class="f2-content color-white">
									<h4 class="mt0">UPI AutoPay</h4>
									<p>Accept and disburse UPI recurring payments via various platforms</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="is-main">
					<div class="col-md-7">
						<div class="is-img mpb40">
							<img src="img/home/dashboard.png" class="img-responsive">
						</div>
					</div>
					<div class="col-md-5">
						<div>
							<span class="com-span">PyThru Banking</span>
							<h2 class="c-h2">All-in-one Banking Solution</h2>	
							<p class="com-p pt10">Get a digital current account with PyThruBanking and make automated instant disbursement of the payments to single and multiple parties and vendors with PyThruPayouts.</p>	
							<p class="com-p pt10 mb40">PyThruBanking offers solutions to enable automated Accounting and Bookkeeping. Stay tuned for Payroll and GST Return.</p>
							<a href="" class="btn1">Get Started <i class="bi bi-chevron-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-top-padding">
		<div class="pf-dashboard pf-dashboard-gradient-bg bg-rotate">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						<div class="sec-banner">
							<b>New !</b>
						</div>
						<div class="mt65">
							<span class="com-span">PyThru</span>
							<h2 class="c-h2 color-white">Best of the best features</h2>	
							<p class="com-p pt10 color-hw mb60">We provide world-class services to empower your business payments with our hassle-free online payment solutions. Our features include 100+ payment modes, the highest security compliances including PCI DSS, smart dashboard tool, easy integration, and much more.</p>
							<a href="" class="btn2">Get Started <i class="bi bi-chevron-double-right"></i></a>
						</div>
					</div>
					<div class="col-md-7">
						<div class="col-md-6 f-box-cp col-sm-6">
							<div class="f-box">
								<img src="img/home/pricing.svg">
								<h3>Transparent Pricing</h3>
								<p>Affordable and simple pricing plans as per your business needs.</p>
							</div>
							<div class="f-box">
								<img src="img/home/support.svg">
								<h3>Customer Support</h3>
								<p>Excellent 24x7 support services for business and its customers’ queries or concerns</p>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="f-box">
								<img src="img/home/onboarding.svg">
								<h3>Instant Onboarding</h3>
								<p>Get your account activated easily with a fully online onboarding process</p>
							</div>
							<div class="f-box">
								<img src="img/home/integration.svg">
								<h3>Easy Integration</h3>
								<p>The ready technical documentation and integration kits make integration easiest.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-12 col-sm-12">
	                <div class="owl-carousel testimonial" id="carousel">
	                    <div class="item">
	                        <div class="col-md-offset-3 col-md-6 col-sm-offset-1 col-sm-10">
	                            <div class="card">
	                            	<div class="row">
		                            	<div class="col-md-8 col-sm-8">
		                            		<h3>Payment Collection is indeed hasslefree</h3>
		                            	</div>
		                            	<div class="col-md-4 col-sm-4">
		                            		<img src="img/home/Frame99.svg" class="img-responsive">	
		                            	</div>
		                            </div>
	                                <p>“It is a great experience with the PyThru payment gateway services. The online payment acceptance is smooth and we faced no issues in integration. The activation process of our account was immediate”</p>
	                            </div>
	                            <div class="col-md-offset-5 col-sm-offset-4">
		                            <div class="card1">
		                            	<div class="person">
			                            	<div class="person-img">
			                            		<img src="img/home/man.svg">
			                            	</div>
			                            	<div class="person-detail">
			                            		<h4>Sonu Kumar</h4>
			                            		<p>Doon Cars</p>
			                            	</div>
			                            </div>
		                            </div>
		                        </div>
	                        </div>		        
	                    </div>
	                    <div class="item">
	                        <div class="col-md-offset-3 col-md-6 col-sm-offset-1 col-sm-10">
	                            <div class="card">
	                            	<div class="row">
		                            	<div class="col-md-8 col-sm-8">
		                            		<h3>Best Possible Services</h3>
		                            	</div>
		                            	<div class="col-md-4 col-sm-4">
		                            		<img src="img/home/Frame99.svg" class="img-responsive">	
		                            	</div>
		                            </div>
	                                <p>“The Payouts with PyThru and its payment link product made our payables and payment acceptance seamless. They offered us the best rates. We can make effortless payouts to our vendors and third parties”</p>
	                            </div>
	                            <div class="col-md-offset-5 col-sm-offset-4">
		                            <div class="card1">
		                            	<div class="person">
			                            	<div class="person-img">
			                            		<img src="img/home/man.svg">
			                            	</div>
			                            	<div class="person-detail">
			                            		<h4>Sawan Kumar</h4>
			                            		<p>Etnasasta</p>
			                            	</div>
			                            </div>
		                            </div>
		                        </div>
	                        </div>		        
	                    </div>
	                    <div class="item">
	                        <div class="col-md-offset-3 col-md-6 col-sm-offset-1 col-sm-10">
	                            <div class="card">
	                            	<div class="row">
		                            	<div class="col-md-8 col-sm-8">
		                            		<h3>Affordable and easy</h3>
		                            	</div>
		                            	<div class="col-md-4 col-sm-4">
		                            		<img src="img/home/Frame99.svg" class="img-responsive">	
		                            	</div>
		                            </div>
	                                <p>“We have a small business and did not have a website. PyThru Payment pages helped us fill the void of our online store and we now collect secure payments showcasing our brand”</p>
	                            </div>
	                            <div class="col-md-offset-5 col-sm-offset-4">
		                            <div class="card1">
		                            	<div class="person">
			                            	<div class="person-img">
			                            		<img src="img/home/man.svg">
			                            	</div>
			                            	<div class="person-detail">
			                            		<h4>Sharwan Kumar</h4>
			                            		<p>The Various Store</p>
			                            	</div>
			                            </div>
		                            </div>
		                        </div>
	                        </div>		        
	                    </div>
	                    <div class="item">
	                        <div class="col-md-offset-3 col-md-6 col-sm-offset-1 col-sm-10">
	                            <div class="card">
	                            	<div class="row">
		                            	<div class="col-md-8 col-sm-8">
		                            		<h3>Customer Experience improved</h3>
		                            	</div>
		                            	<div class="col-md-4 col-sm-4">
		                            		<img src="img/home/Frame99.svg" class="img-responsive">	
		                            	</div>
		                            </div>
	                                <p>“Our customers make easy payments through our website payment button that I created easily at PyThru. They provide a very high transaction success rate”</p>
	                            </div>
	                            <div class="col-md-offset-5 col-sm-offset-4">
		                            <div class="card1">
		                            	<div class="person">
			                            	<div class="person-img">
			                            		<img src="img/home/man.svg">
			                            	</div>
			                            	<div class="person-detail">
			                            		<h4>Bikram Biswas</h4>
			                            		<p>Velvery Private Limited</p>
			                            	</div>
			                            </div>
		                            </div>
		                        </div>
	                        </div>		        
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>

	<section>
		<div class="fs-bg edge--top--reverse">
			<div class="container">
				<div class="row sec-padding">
					<div class="text-center">
						<h2 class="c-h2 pb30 mt0">Our Partners</h2>	
					</div>
					<div class="client-img">
						<img src="img/home/banking-partners.svg" class="img-responsive">
					</div>
				</div>
				<div class="row sec-bottom-padding">
					<div class="col-md-offset-1 col-md-10 col-sm-12">
						<div class="dialog">
							<div class="col-md-offset-3 col-sm-offset-4">
								<div class="di-content">
									<h2 class="c-h2">Trust PyThru for your business finances</h2>
									<p class="com-p pt10">You need not worry about your business payments with us as your <br class="hidden-xs hidden-sm">payments partner. </p>
									<div class="row pt25 iml20">
										<div class="col-md-offset-1 col-md-4 col-sm-6">
											<h4>Robust APIs</h4>
											<h4>Easy Onboarding</h4>
										</div>
										<div class="col-md-6 col-sm-6">
											<h4>Best Payment Gateway</h4>
											<h4>Smooth Payouts</h4>
										</div>
									</div>
									<div class="row pt25 pb30">
										<div class="col-md-offset-1 col-md-8">
											<div class="signup iwunset">
												<input type="email" name="email" placeholder="Email address" required="">
												<a href=""><img src="img/home/Group 3.svg"></a>
											</div>
										</div>
									</div>
								</div>	
							</div>				
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include 'include/footer.php';?>

	<?php include 'include/js.php';?>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

	<script type="text/javascript">
	    $(function() {
			var current_pos;
			
			$('#carousel').owlCarousel({
				items: 1,
				responsiveClass: true,
				mouseDrag: false,
				touchDrag: false,
				autoplay: false,
				autoplayTimeout: 2000,
				nav: true,
				navText: ['', ''],
				dots: true,
				onInitialized: function(e) {
					current_pos = e.item.index;				
				},
				onTranslate: function(e) {
					var direction = e.item.index > current_pos? 1 : 0,
							indicator_c = direction? 'next' : 'prev',
							$dots = $(e.currentTarget).find('.owl-dots'),
							$current_dot = $dots.children().eq(current_pos);
								
					$current_dot.html('<div class="dot-indicator '+ indicator_c +'">');
					
					current_pos = e.item.index;
					
					setTimeout(function() {
						$current_dot.html('');
					}, 200);	
				}
			});
		});
    </script>
</body>
</html>