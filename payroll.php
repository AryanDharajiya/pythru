<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Best Payroll and HR Software in India | Pythru Payroll</title>	
	<meta name="description" content="Start with Pythru Payroll software and automate the calculation and payouts of the salary;  Payroll laws and compliances; and various HR activities.">
	<meta name="keywords" content="Pythru Payroll, Payroll Software, statutory compliances, payroll laws, track attendance, employee management, HR management">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/banking.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="main-bg">
		<div class="sec-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						<div class="hero sec-padding">
							<div class="content">
								<h1 class="c-h1">The Best <span>Payroll </span>System with PyThru</h1>
								<p class="pt10">Simplify and automate the salary calculation, its disbursement, Compliances, and other HR functions <br class="hidden-xs">with the PyThru Payroll</p>
							</div> 
						</div>
					</div>
					<div class="col-md-offset-1 col-md-6">
						<div class="hero-graphic hidden-sm hidden-xs">
							<img src="img/payroll/hero.svg" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-12 mt30">
					<div class="km m-tc">
						<span>PyThru Payroll</span>
						<h2>Why PyThru Payroll?</h2>
						<p>Get started with the PyThru Payroll now and let us run the important HR operations such as salaries, compliances, insurance, attendance, employee management, etc.</p>
						<a href="#">Know More <i class="bi bi-arrow-right"></i></a>
					</div>
				</div>
				<div class="col-md-offset-1 col-md-6 col-sm-12">
					<div class="row">						
						<div class="col-md-6 col-sm-6">
							<div class="km-box">
								<div class="km-box-content">
									<img src="img/payroll/salary.svg">
									<h3>Salary</h3>
									<p>Calculate and transfer the salary payouts to the employees directly in their bank account in just a few clicks</p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="km-box">
								<div class="km-box-content">
									<img src="img/payroll/compliance.svg">
									<h3>Compliance</h3>
									<p>Set and automate the calculations of the compliances & filing of Provident Fund, Professional Tax, ESI, Income Tax, etc. </p>
								</div>
							</div>
						</div>	
						<div class="col-md-6 col-sm-6">
							<div class="km-box">
								<div class="km-box-content">
									<img src="img/payroll/task-allocation.svg">
									<h3>Task Allocation</h3>
									<p>Provide the self-service for the functions such as leave application, manage pay, declare tax, details for insurance, etc.</p>
								</div>
							</div>
						</div>				
						<div class="col-md-6 col-sm-6">
							<div class="km-box">
								<div class="km-box-content">
									<img src="img/payroll/hr.svg">
									<h3>HR</h3>
									<p>Systematic & simplified records and management of the employees’ lifecycle from their recruitment.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class=" sec-padding">      
        <div class="bg-black">
            <div class="container sec-padding">
                <div class="row"> 
                    <div class="col-md-3 col-sm-12">
                        <div class="s3c">
                            <img src="img/payroll/dot-frame.svg" class="hidden-xs hidden-sm">
                            <h2 class="m-tc">How it works?</h2>
                        </div>
                    </div>  
                    <div class="col-md-9">
                        <div class="col-md-4 col-sm-4">
                            <div class="s3-content">
                                <img src="img/payroll/set-rates.svg">
                                <h3>Set Rates</h3>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="s3-content">
                                <img src="img/payroll/send-payment.svg">
                                <h3>Send Payment</h3>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="s3-content">
                                <img src="img/payroll/precise-accounting.svg">
                                <h3>Precise Accounting</h3>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>           
    </section>

    <section class="sec-padding">
    	<div class="container">
    		<div class="row">
    			<div class="text-center km">
    				<span>Features</span>
					<h2>More Enhanced Features</h2>
    			</div>
    		</div>
    		<div class="row">
    			<div class="s4-tab-main">
	    			<div class="col-md-7 mt30">
	    				<div class="tab-content">
						    <div id="first" class="tab-pane fade in active">
						      <img src="img/payroll/image 37.svg" class="img-responsive">
						    </div>
						    <div id="second" class="tab-pane fade">
						      <img src="img/payroll/image 37.svg" class="img-responsive">
						    </div>
						    <div id="third" class="tab-pane fade">
						      <img src="img/payroll/image 37.svg" class="img-responsive">
						    </div>
						 </div>
	    			</div>
	    			<div class="col-md-5">
	    				<ul class="nav nav-tabs">
						    <li class="active">
						    	<a data-toggle="tab" href="#first">
						    		<h3>Manage Employees</h3>
						    		<p>Manage the information of the employees concerning their payroll, and plan the onboarding to exit systematically.</p>
						    	</a>
						    </li>
						    <li>
						    	<a data-toggle="tab" href="#second">
						    		<h3>Employee Insurance</h3>
						    		<p>Best insurance plans covering all employees for all existing and future conditions from the very first day.</p>
						    	</a>
						    </li>
						    <li>
						    	<a data-toggle="tab" href="#third">
						    		<h3>Attendance</h3>
						    		<p>Setup the leave policies and keep track of the attendance ensuring the automatic calculation of the payroll</p>
						    	</a>
						    </li>
						</ul>
	    			</div>
	    		</div>
    		</div>
    	</div>
    </section>

    <section class="sec-padding">
    	<div class="container">
    		<div class="row">
    			<div class="text-center km">
    				<span>Lorem ipsum</span>
					<h2>Why PyThru Payroll?</h2>					
    			</div>
    			<p class="km-cp">Experience the best Payroll and HR features for startups and <br class="hidden-xs">other businesses with the PyThru Payroll Software</p>
    		</div>
    		<div class="row">
				<div class="tick">
					<div class="col-md-4 col-sm-6">
						<h4>Customize Salary Structure</h4>
					</div>
					<div class="col-md-4 col-sm-6">
						<h4>Generate the Payslips</h4>
					</div>
					<div class="col-md-4 col-sm-6">
						<h4>Detailed Payroll Reports</h4>
					</div>
					<div class="col-md-4 col-sm-6">
						<h4>Submit expenses for reimbursements</h4>
					</div>
					<div class="col-md-4 col-sm-6">
						<h4>Tax Declarations</h4>
					</div>
					<div class="col-md-4 col-sm-6">
						<h4>Free Trial</h4>
					</div>
				</div>
			</div>
    	</div>
    </section>

    <section class="sec-padding">
    	<div class="bg-blue">
    		<div class="container">
    			<div class="row">
    				<div class="cu-main">
    					<div class="col-md-5">
    						<div class="cu-main-text">
    							<h2>Experience the <br class="hidden-xs">software now!</h2>
    							<a href="#">Contact Us <i class="bi bi-arrow-right"></i></a>
    						</div>
    					</div>
    					<div class="col-md-7">
    						<div class="cu-img hidden-xs">
    							<img src="img/payroll/image 37.svg" class="img-responsive">
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
    
    <section class="accordion-section clearfix mt0 sec-padding" aria-label="Question Accordions">
	  <div class="container">	
		  <div class="col-md-offset-1 col-md-10">  
			  <h2 class="text-center faq-title">FAQs</h2>
			  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
						Why choose PyThru Payroll?
					  </a>
					</h3>
				  </div>
				  <div id="collapse0" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading0" aria-expanded="true">
					<div class="panel-body px-3 mb-4">
					  <p>PyThru Payroll automates various payroll functions. It not only processes the salaries but also the statutory compliance and payroll laws. It also includes various HR features.</p>
					  </ul>
					</div>
				  </div>
				</div>
				
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading1">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
						What is included in the PyThru Payroll?
					  </a>
					</h3>
				  </div>
				  <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
					<div class="panel-body px-3 mb-4">
					  <p>It is a single tool that includes various operations such as-</p>
					  <ul>
					  	<li>Payroll and Salary Management</li>
					  	<li>HR Management </li>
					  	<li>Employees Management</li>
					  	<li>Leave and Attendance</li>
					  	<li>Expenses and Reimbursement</li>
					  </ul>
					  <p>And others.</p>
					</div>
				  </div>
				</div>
				
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading2">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
						What are the pricing plans of PyThru Payroll?
					  </a>
					</h3>
				  </div>
				  <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
					<div class="panel-body px-3 mb-4">
					  <p>(pending)</p>
					</div>
				  </div>
				</div>
			  </div>
		  </div>	  
	  </div>
	</section>

	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>

	
</body>
</html>