<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

    <title>Generate Free Online GST Compliant Invoices | PyThru Billing</title> 
    <meta name="description" content="Create custom GST compliant Invoices with PyThru and accept fast online payments from your customers. Provide a smooth payment experience.">
    <meta name="keywords" content="Invoice, GST-compliant Invoice, PyThru Invoice, Invoicing, Online Payments, Billing Software, Payment Gateway">

    <?php include 'include/css.php';?>  
	<link rel="stylesheet" href="css/invoice.css">
</head>
<body>

	<?php include 'include/header.php';?>

    <div class="bg-c">
        <div class="bg">
            <div class="container sec-padding">
                <div class="row sec-padding">
                    <div class="col-md-5">
                        <div class="hero">
                            <div class="content">
                                <h1 class="c-h1">Send GST Compliant <br class="hidden-xs"><span>Invoice </span>and Get Paid </h1>
                                <p class="pt10 pb20 content-p">Accept quick secure payments through PyThru GST-compliant invoices. Create them easily and send them to your vendors and customers.</p>
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="hero-graphic hidden-sm hidden-xs">
                            <img src="img/invoice/hero.svg" class="img-responsive">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="why-us why-us-pad">
                            <div class="col-md-6">
                                <div class="w-content">
                                    <h3>Why opt for Instant Settlements?</h3>
                                    <p>To enjoy the liquidity advantage same as the cash inflow.<br class="hidden-xs">Now you do not need to wait for days for your own payments.</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="w-btn">
                                    <a href="">
                                        <span>Sign up</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="container sec-padding">
            <div class="row mpt30p">
                <div class="col-md-4 col-sm-4">
                    <div class="rb">
                        <img src="img/invoice/create-invoice.svg">
                        <h3>Create Invoice</h3>
                        <p>Create customized invoices from your smart dashboard with no technical knowledge required.</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="rb">
                        <img src="img/invoice/get-paid.svg">
                        <h3>Get Paid</h3>
                        <p>Service customer with cash in hand or save on interest</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="rb">
                        <img src="img/invoice/manage-invoice.svg">
                        <h3>Manage Invoices</h3>
                        <p>Service customer with cash in hand or save on interest</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="sec-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="lrs-img">
                        <img src="img/invoice/desk.svg" class="img-responsive">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="lrs-pad">
                        <div>
                            <span class="com-span">Affordable</span>
                            <h2 class="c-h2">Highlights of the Best PyThru Invoice features</h2>   
                            <p class="com-p pt10">Enable detailed invoicing with the elaborated breakup of items, pricing, shipping, <br class="hidden-xs">discounts, taxes, etc showcasing your business brand.</p>   
                        </div>
                        <div class="col-md-6">
                            <div class="points">
                                <h4>Multi currency</h4>
                                <h4>Save & Reuse</h4>
                                <h4>Records & Reports</h4>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="points mpt0">
                                <h4>Downloadable Invoice</h4>
                                <h4>Analysis & Insights</h4>
                                <h4>Recurring Payments</h4>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </section>

    <section class="sec-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="s4">
                        <div class="row mt-center">
                            <div class="col-md-offset-1 col-md-7 pt20">
                                <span class="com-span">Features</span>
                                <h2>Our GST Compliant invoices for your business online payments. Get paid easily!</h2>
                            </div>
                            <div class="col-md-4 vd-btn-pad">
                                <a href="" class="vd-btn">View Invoice Demo</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sec-ct-padding">
        <div class="bg1">
            <div class="bg-black">
                <div class="container sec-padding">
                    <div class="row"> 
                        <div class="col-md-3 col-sm-12">
                            <div class="s3c">
                                <img src="img/invoice/dot-frame.svg" class="hidden-xs hidden-sm">
                                <h2>3 Major <br class="hidden-xs hidden-sm"> Benefits of  <br class="hidden-xs hidden-sm">PyThru Invoice </h2>
                            </div>
                        </div>  
                        <div class="col-md-9">
                            <div class="col-md-4 col-sm-4">
                                <div class="s3-content">
                                    <img src="img/invoice/gst-complaint.svg">
                                    <h3>GST compliant</h3>
                                    <p>A ready invoice with GST and other calculations such as discounts & shipping</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="s3-content">
                                    <img src="img/invoice/customizes-invoice.svg">
                                    <h3>Customized Invoice</h3>
                                    <p>Displays your branding and logo that gives a reliable look and confidence to pay</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="s3-content">
                                    <img src="img/invoice/multi-currency.svg">
                                    <h3>Multi-Currency</h3>
                                    <p>Supports all major global currencies and enables automatic currency conversion.</p>
                                </div>
                            </div>
                        </div>                    
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row sec-cb-pad">
                    <div class="col-md-5">
                        <div class="tc">
                            <h2><span>Connect</span> with <br class="hidden-xs">our team</h2>
                        </div>
                    </div>
                    <div class="col-md-7 pt30">
                        <div class="col-md-6">
                            <div class="tc-input">
                                <input type="email" name="email" placeholder="Enter email">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="tc-btn">
                                <a href="">Send mail</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </section>

    <section class="sec-padding"></section>   

    <?php include 'include/footer.php';?> 

    <?php include 'include/js.php';?>

</body>
</html>