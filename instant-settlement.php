<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

    <title>PyThru Instant Settlements | Enable Business Growth</title> 
    <meta name="description" content="Instant Settlements, On-Demand & Same-day Settlements enable smooth cash flow in your business with PyThru Payment Gateway in India.">
    <meta name="keywords" content="Instant Settlements, Payment Gateway in India, Same day settlements, On-demand settlements, online payments">

    <?php include 'include/css.php';?>  
	<link rel="stylesheet" href="css/instant-settlement.css">
</head>
<body>

	<?php include 'include/header.php';?>

    <div class="bg-c">
        <div class="bg">
            <div class="container sec-padding">
                <div class="row sec-padding">
                    <div class="col-md-5">
                        <div class="hero">
                            <div class="content">
                                <h1 class="c-h1">Get the Cash <br class="hidden-xs">Flowing with <br class="hidden-xs"><span>Instant Settlements </span></h1>
                                <p class="pt10 pb20 content-p">Receive the settlements of your online payments to your bank account in minutes and manage the cash flow and the business operations without any constraints.</p>
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-offset-1 col-md-6">
                        <div class="hero-graphic hidden-sm hidden-xs">
                            <img src="img/instant-settlement/hero.svg" class="img-responsive">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="why-us why-us-pad">
                            <div class="col-md-6">
                                <div class="w-content">
                                    <h3>Why opt for Instant Settlements?</h3>
                                    <p>To enjoy the liquidity advantage same as the cash inflow.<br class="hidden-xs">Now you do not need to wait for days for your own payments.</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="w-btn">
                                    <a href="">
                                        <span>Sign up</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="container sec-padding">
            <div class="row mpt30p">
                <div class="text-center ff-title">
                    <h2><b> Choose</b> what your business requires…</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="rb">
                        <p>Get your settlements instantly <br class="hidden-xs hidden-sm">within minutes anywhere, anytime <br class="hidden-xs hidden-sm">even on bank holidays</p>
                        <h3>Instant</h3>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="rb">
                        <p>Receive your money to you <br class="hidden-xs hidden-sm">bank account on the same <br class="hidden-xs hidden-sm">day as the transaction</p>
                        <h3>Same-Day</h3>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="rb">
                        <p>Opt for the on-demand <br class="hidden-xs hidden-sm">settlements of specific transactions <br class="hidden-xs hidden-sm">multiple times on the same day</p>
                        <h3>Customized</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="sec-padding">
        <div class="container">
            <div class="row point-pad">
                <div class="left-right-section">
                    <div class="col-md-6 col-sm-6">
                        <div class="lrs-img">
                            <img src="img/instant-settlement/feature.svg" class="img-responsive">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="lrs-pad m-center">
                            <span class="com-span">Features</span>
                            <h2 class="c-h2">Smooth Business Payments with PyThru Payment Gateway</h2>  
                            <p class="com-p pt10">Accept effortless payments with multiple settlement options,
                            <br class="hidden-xs">easy integration, 100+ payment modes, and best security.</p>   
                        </div>
                    </div>                  
                </div>
            </div>
        </div>
    </section>

    <section class="sec-ct-padding">
        <div class="bg1">
            <div class="bg-black">
                <div class="container sec-padding">
                    <div class="row"> 
                        <div class="col-md-3 col-sm-12">
                            <div class="s3c">
                                <img src="img/instant-settlement/dot-frame.svg" class="hidden-xs hidden-sm">
                                <h2>Benefits of <br class="hidden-xs hidden-sm">Instant or <br class="hidden-xs hidden-sm">Same-Day <br class="hidden-xs hidden-sm">Settlements</h2>
                            </div>
                        </div>  
                        <div class="col-md-9">
                            <div class="col-md-4 col-sm-4">
                                <div class="s3-content">
                                    <img src="img/instant-settlement/business-growth.svg">
                                    <h3>Business Growth</h3>
                                    <p>Utilizing your earnings instantly for further business and stay a step ahead</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="s3-content">
                                    <img src="img/instant-settlement/better-operation.svg">
                                    <h3>Better Operations</h3>
                                    <p>Meet your expenses and dues with the smooth and planned cash flow in place</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="s3-content">
                                    <img src="img/instant-settlement/affordable-pricing.svg">
                                    <h3>Affordable Pricing</h3>
                                    <p>Choose and avail your settlement type with plain, transparent, and affordable pricing</p>
                                </div>
                            </div>
                        </div>                    
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row sec-cb-pad">
                    <div class="col-md-5">
                        <div class="tc">
                            <h2><span>Connect</span> with <br class="hidden-xs">our team</h2>
                        </div>
                    </div>
                    <div class="col-md-7 pt30">
                        <div class="col-md-6">
                            <div class="tc-input">
                                <input type="email" name="email" placeholder="Enter email">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="tc-btn">
                                <a href="">Send mail</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </section>

    <section class="sec-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-1 col-md-4 col-sm-5">
                    <div class="qute m-center">
                        <img src="img/freelancer/qute.svg">
                        <h2>Happy words from <br class="hidden-xs">our Merchants…</h2>
                    </div>
                </div>
                <div class="col-md-6 col-sm-7">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="https://i.imgur.com/lE89Aey.jpg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                        <div class="content">
                                            <p>Getting online payments instantly helps our business operations flow smoothly due to the instant and easy availability of our funds from the sales made.</p>
                                            <h4>Jack Andreson</h4>
                                            <span>CEO, WazirX</span>
                                        </div>
                                    </div>                                 
                                </div>
                            </div>
                            <div class="item">
                                <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="https://i.imgur.com/lE89Aey.jpg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                        <div class="content">
                                            <p>The online payments are acting as cash for our business with PyThru customized settlements. That too, at such affordable pricing plans.</p>
                                            <h4>Jack Andreson</h4>
                                            <span>CEO, WazirX</span>
                                        </div>
                                    </div>                                 
                                </div>
                            </div>
                            <div class="item">
                                <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="https://i.imgur.com/lE89Aey.jpg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                        <div class="content">
                                            <p>Best overall experience with PyThru payment gateway. I am able to get the instant settlements and meet my finance requirements on a real-time basis.</p>
                                            <h4>Jack Andreson</h4>
                                            <span>CEO, WazirX</span>
                                        </div>
                                    </div>                                 
                                </div>
                            </div>
                            <div class="item">
                                <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="https://i.imgur.com/lE89Aey.jpg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                        <div class="content">
                                            <p>Easy onboarding, affordable pricing, smooth integration, and same-day settlements make PyThru the best payment gateway for our business.</p>
                                            <h4>Jack Andreson</h4>
                                            <span>CEO, WazirX</span>
                                        </div>
                                    </div>                                 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-offset-1 col-md-5">
                        <div class="controls testimonial_control pull-right">
                            <a class="left bi bi-chevron-left btn-default testimonial_btn fw900" href="#carousel-example-generic" data-slide="prev"></a>

                            <a class="right bi bi-chevron-right btn-default testimonial_btn fw900" href="#carousel-example-generic" data-slide="next"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="accordion-section clearfix mt-3 sec-padding" aria-label="Question Accordions">
      <div class="container">   
          <div class="col-md-offset-1 col-md-10">  
              <h2 class="text-center faq-title">FAQs</h2>
              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                  <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
                    <h3 class="panel-title">
                      <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
                        How to activate the instant settlements?
                      </a>
                    </h3>
                  </div>
                  <div id="collapse0" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading0" aria-expanded="true">
                    <div class="panel-body px-3 mb-4">
                      <p>Kindly <a href="contact">contact</a> our PyThru customer support team and discuss your requirements.</p>
                      </ul>
                    </div>
                  </div>
                </div>
                
                <div class="panel panel-default">
                  <div class="panel-heading p-3 mb-3" role="tab" id="heading1">
                    <h3 class="panel-title">
                      <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                        What are customized settlements?
                      </a>
                    </h3>
                  </div>
                  <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                    <div class="panel-body px-3 mb-4">
                      <p>As per your business requirements, you can get the settlements multiple times a day as per your custom request.</p>
                    </div>
                  </div>
                </div>
                
                <div class="panel panel-default">
                  <div class="panel-heading p-3 mb-3" role="tab" id="heading2">
                    <h3 class="panel-title">
                      <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                        Can I get the settlements on the holidays?
                      </a>
                    </h3>
                  </div>
                  <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                    <div class="panel-body px-3 mb-4">
                      <p>Yes, you can get the settlements in real-time, on the same day, or as per customized selection even on the holidays.</p>
                    </div>
                  </div>
                </div>
                
                <div class="panel panel-default">
                  <div class="panel-heading p-3 mb-3" role="tab" id="heading3">
                    <h3 class="panel-title">
                      <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
                        Where can I see the receipt of the instant settlements?
                      </a>
                    </h3>
                  </div>
                  <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                    <div class="panel-body px-3 mb-4">
                      <p>All types of settlements appear in the settlement records in your dashboard as well as the settlement reports. </p>
                    </div>
                  </div>
                </div>
                
                <div class="panel panel-default">
                  <div class="panel-heading p-3 mb-3" role="tab" id="heading4">
                    <h3 class="panel-title">
                      <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">
                        How is the fee deducted for the opted settlements?
                      </a>
                    </h3>
                  </div>
                  <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                    <div class="panel-body px-3 mb-4">
                      <p>For the instant or same-day settlements, the fees are deducted from the settlement amount itself. It will be added to the payment gateway fee as per your selected settlement.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>      
      </div>
    </section>
	
	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>

</body>
</html>