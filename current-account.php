<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Open Current Account Online | PyThru Banking Solution</title>	
	<meta name="description" content="Get started with a free Current account with PyThru for Online Business Banking to automate payments and smoothly manage the business finances and accounts">
	<meta name="keywords" content="Current Account, Online Current Account, Business Banking">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/banking.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="main-bg">
		<div class="sec-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="hero sec-padding">
							<div class="content cpt">
								<h1 class="c-h1">Current Account For <br class="hidden-xs">your<span> Business Finances</span></h1>
								<p class="pt10">Smooth management of your finances <br class="hidden-xs">with our Current Account that has integrated tools <br class="hidden-xs">for easy finance, expenses, and accounting <br class="hidden-xs">management.</p>
							</div> 
						</div>
					</div>
					<div class="col-md-6">
						<div class="hero-graphic hidden-sm hidden-xs">
							<img src="img/current-account/hero.svg" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="bgfb">
		<div class="sec-padding">
			<div class="container">
				<div class="row">
					<div class="text-center">
						<div class="km m-tc">
							<h2>Seamless Current Account <br class="hidden-xs">Opening with Pythru</h2>
							<p>Pythru’s Business Banking is here to take care of your <br class="hidden-xs">business finance requirements most efficiently</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<div class="bf-main">
							<div class="bft">
								<img src="img/current-account/min-balance.svg">								
								<h3>Minimum monthly <br class="hidden-xs">balance</h3>
							</div>
							<div class="bfc">
								<p>No burdening average monthly <br class="hidden-xs">balance. Minimum requirement of <br class="hidden-xs">₹ 25000 for account opening</p>
							</div>
						</div>
					</div>
					<div class="col-sm-4"> 
						<div class="bf-main">
							<div class="bft">
								<img src="img/current-account/min-balance.svg">								
								<h3>Transaction <br class="hidden-xs">Fees</h3>
							</div>
							<div class="bfc">
								<p>Zero charges on NEFT & RTGS <br class="hidden-xs">payments and affordable fees for <br class="hidden-xs">other modes</p>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="bf-main">
							<div class="bft">
								<img src="img/current-account/min-balance.svg">								
								<h3>Add Beneficiary <br class="hidden-xs">Instantly</h3>
							</div>
							<div class="bfc">
								<p>Instantly add the beneficiary and <br class="hidden-xs">start making the payments anytime, <br class="hidden-xs">anywhere</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-12 mt30">
					<div class="km m-tc">
						<span>PyThru Current Account</span>
						<h2>Smart Current Account <br class="hidden-xs">for Your Smart Business</h2>
						<p>Pythru’s Business Banking is here to take <br class="hidden-xs">care of your business finance requirements <br class="hidden-xs">most efficiently</p>
						<a href="#">Know More <i class="bi bi-arrow-right"></i></a>
					</div>
				</div>
				<div class="col-md-offset-1 col-md-6 col-sm-12">
					<div class="row">						
						<div class="col-md-6 col-sm-6">
							<div class="km-box">
								<div class="km-box-content">
									<img src="img/current-account/add-beni.svg">
									<h3>Add Beneficiary</h3>
									<p>Instantly add the beneficiary and start making the payments anytime, anywhere</p>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="km-box">
								<div class="km-box-content">
									<img src="img/current-account/smooth-payout.svg">
									<h3>Smooth Payouts</h3>
									<p>Automate 24x7 bulk payouts to vendors and employees with bank transfer, UPI, etc</p>
								</div>
							</div>
						</div>	
						<div class="col-md-6 col-sm-6">
							<div class="km-box">
								<div class="km-box-content">
									<img src="img/current-account/manage-transaction.svg">
									<h3>Manage Transactions</h3>
									<p>Hasslefree managing and tracking of payments and receipts with a seamless search</p>
								</div>
							</div>
						</div>				
						<div class="col-md-6 col-sm-6">
							<div class="km-box">
								<div class="km-box-content">
									<img src="img/current-account/tax-payments.svg">
									<h3>Easy Tax Payments</h3>
									<p>Automate calculation and payment of taxes to vendors & for GST; also, manage tax challans</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="bg1 sec-padding">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<span class="com-span bg-white">Future of Banking</span>
					<h2 class="c-h2 pb10">All-in-one Pythru Banking Solution</h2>	
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/current-account/pg.svg">
						<h3>Payment Gateway</h3>
						<p>Multiple products and integration options to accept smooth payments from customers</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/current-account/business-banking.svg">
						<h3>Business Banking</h3>
						<p>Make disbursements, bulk payouts, manage Payroll and HR, GST Invoices, and much more</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="why-use">
						<img src="img/current-account/automated-accounting.svg">
						<h3>Automated Accounting</h3>
						<p>Automate accounts and bookkeeping for your business while you enable business growth</p>
					</div>
				</div>
			</div>
		</div>
	</section>

    <section class="sec-padding">
    	<div class="container">
    		<div class="row">
    			<div class="text-center km">
    				<span>Features</span>
					<h2>More Enhanced Current <br class="hidden-xs">Account Features</h2>
    			</div>
    		</div>
    		<div class="row">
    			<div class="s4-tab-main">
	    			<div class="col-md-7 mt30">
	    				<div class="tab-content">
						    <div id="first" class="tab-pane fade in active">
						      <img src="img/current-account/image 37.svg" class="img-responsive">
						    </div>
						    <div id="second" class="tab-pane fade">
						      <img src="img/current-account/image 37.svg" class="img-responsive">
						    </div>
						    <div id="third" class="tab-pane fade">
						      <img src="img/current-account/image 37.svg" class="img-responsive">
						    </div>
						 </div>
	    			</div>
	    			<div class="col-md-5">
	    				<ul class="nav nav-tabs">
						    <li class="active">
						    	<a data-toggle="tab" href="#first">
						    		<h3>Security</h3>
						    		<p>Enable 2 Factor Authentication Login & payments and also assign user roles for different accesses for security</p>
						    	</a>
						    </li>
						    <li>
						    	<a data-toggle="tab" href="#second">
						    		<h3>24 Hour Banking</h3>
						    		<p>Check balance, make payments, approve payments, get custom reports, analytics, Financial Insights, real-time cashflow Summaries, etc.</p>
						    	</a>
						    </li>
						    <li>
						    	<a data-toggle="tab" href="#third">
						    		<h3>Notification and Records</h3>
						    		<p>Get instant notification of transaction and the passbook updated in real-time </p>
						    	</a>
						    </li>
						</ul>
	    			</div>
	    		</div>
    		</div>
    	</div>
    </section>

    <section class="sec-padding">
    	<div class="container">
    		<div class="row">
    			<div class="text-center km">
    				<span>Lorem ipsum</span>
					<h2>Why PayThru Current Account?</h2>					
    			</div>
    			<p class="km-cp">Pythru has the best, smart and secure banking solution for <br class="hidden-xs">your overall business needs</p>
    		</div>
    		<div class="row">
    			<div class="col-md-offset-1 col-md-10">
					<div class="tick">
						<div class="col-md-4 col-sm-6">
							<h4>Robust APIs</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Seamless Payouts</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Easy Dashboard</h4>
						</div>
						<div class="col-md-offset-2 col-md-4 col-sm-6">
							<h4>Secure Infrastructure</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Modern Technology</h4>
						</div>
					</div>
				</div>
			</div>
    	</div>
    </section>

    <section class="sec-padding">
    	<div class="bg-blue">
    		<div class="container">
    			<div class="row">
    				<div class="cu-main">
    					<div class="col-md-5">
    						<div class="cu-main-text">
    							<h2>Trust Pythru <br class="hidden-xs">Banking</h2>
    							<a href="#">Contact Us <i class="bi bi-arrow-right"></i></a>
    						</div>
    					</div>
    					<div class="col-md-7">
    						<div class="cu-img hidden-xs">
    							<img src="img/payroll/image 37.svg" class="img-responsive">
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
    
    <section class="accordion-section clearfix mt0 sec-padding" aria-label="Question Accordions">
	  <div class="container">	
		  <div class="col-md-offset-1 col-md-10">  
			  <h2 class="text-center faq-title">FAQs</h2>
			  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
						Why choose PyThru Payroll?
					  </a>
					</h3>
				  </div>
				  <div id="collapse0" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading0" aria-expanded="true">
					<div class="panel-body px-3 mb-4">
					  <p>PyThru Payroll automates various payroll functions. It not only processes the salaries but also the statutory compliance and payroll laws. It also includes various HR features.</p>
					  </ul>
					</div>
				  </div>
				</div>
				
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading1">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
						What is included in the PyThru Payroll?
					  </a>
					</h3>
				  </div>
				  <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
					<div class="panel-body px-3 mb-4">
					  <p>It is a single tool that includes various operations such as-</p>
					  <ul>
					  	<li>Payroll and Salary Management</li>
					  	<li>HR Management </li>
					  	<li>Employees Management</li>
					  	<li>Leave and Attendance</li>
					  	<li>Expenses and Reimbursement</li>
					  </ul>
					  <p>And others.</p>
					</div>
				  </div>
				</div>
				
				<div class="panel panel-default">
				  <div class="panel-heading p-3 mb-3" role="tab" id="heading2">
					<h3 class="panel-title">
					  <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
						What are the pricing plans of PyThru Payroll?
					  </a>
					</h3>
				  </div>
				  <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
					<div class="panel-body px-3 mb-4">
					  <p>(pending)</p>
					</div>
				  </div>
				</div>
			  </div>
		  </div>	  
	  </div>
	</section>

	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>

	
</body>
</html>