<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Multibank Connectivity | PyThru Smart Neo Banking Solution</title>	
	<meta name="description" content="Smart neo banking solution with multibank connectivity. Manage the business banking and accounting with our innovative neo banking solution">
	<meta name="keywords" content="Neo Banking, Online Banking Solutions, Accounting, Invoicing, Auto Reconciliation, HostBooks Neo Banking, Neo Bank">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/banking.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="main-bg">
		<div class="sec-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="hero sec-padding">
							<div class="content">
								<h1 class="c-h1">Pythru <span>Multi-Bank</span> Connectivity</h1>
								<p class="pt10">The modern and smart way for your <br class="hidden-xs">business banking and accounting</p>
							</div> 
						</div>
					</div>
					<div class="col-md-6">
						<div class="hero-graphic hidden-sm hidden-xs">
							<img src="img/multibank/hero.svg" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<div class="km m-tc">
						<h2>Neo Banking for simplification of <br class="hidden-xs">your financial operations</h2>
						<p class="pt5">A comprehensive & innovative solution <br class="hidden-xs">to enable business growth</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="tpc">
						<div class="tpc-img db">
							<img src="img/multibank/account-opening.svg">
							<h3 class="ml0">Easy account opening</h3>
						</div>
						<p>The easy online process to open your current account, with no physical visit required</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="tpc">
						<div class="tpc-img db">
							<img src="img/multibank/reporting.svg">
							<h3 class="ml0">Realtime Reporting & Dashboard</h3>
						</div>
						<p>Get updated and monitor everything from account, cheque deposits to transactions 24x7 in real-time</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="tpc">
						<div class="tpc-img db">
							<img src="img/multibank/accept-payments.svg">
							<h3 class="ml0">Accept smooth Payments</h3>
						</div>
						<p>Collect fast online payments with payment gateway integration from customers, vendors, etc.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="tpc">
						<div class="tpc-img db">
							<img src="img/multibank/make-payout.svg">
							<h3 class="ml0">Make hasslefree Payouts</h3>
						</div>
						<p>Easy and fast bank transfer for individual or multiple payouts to vendors and other parties</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="tpc">
						<div class="tpc-img db">
							<img src="img/multibank/all-in-one-solution.svg">
							<h3 class="ml0">All-in-one solution</h3>
						</div>
						<p>Banking, accounting, invoicing, & cash flow tracking everything in one place</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="tpc">
						<div class="tpc-img db">
							<img src="img/multibank/support.svg">
							<h3 class="ml0">Excellent Support</h3>
						</div>
						<p>Get 24x7 support and get your queries or issues resolved at any time</p>
					</div>
				</div>			
			</div>
		</div>
	</section>

	<section class=" sec-padding">      
        <div class="bg-black">
            <div class="container sec-padding">
                <div class="row"> 
                    <div class="col-md-3 col-sm-12">
                        <div class="s3c">
                            <img src="img/payroll/dot-frame.svg" class="hidden-xs hidden-sm">
                            <h2 class="m-tc">Get Started!</h2>
                        </div>
                    </div>  
                    <div class="col-md-9">
                        <div class="col-md-4 col-sm-4">
                            <div class="cmgs">
                            	<div class="cmgsi">
                            		<img src="img/multibank/signup.svg">
                            		<span></span>
                            		<h3>Step 1</h3>
                            	</div>
                            	<div class="cmgsc">
                            		<p>Sign up</p>
                            	</div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="cmgs">
                            	<div class="cmgsi">
                            		<img src="img/multibank/share-detail.svg">
                            		<span></span>
                            		<h3>Step 2</h3>
                            	</div>
                            	<div class="cmgsc">
                            		<p>Share required details</p>
                            	</div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="cmgs">
                            	<div class="cmgsi">
                            		<img src="img/multibank/get-started.svg">
                            		<span></span>
                            		<h3>Step 3</h3>
                            	</div>
                            	<div class="cmgsc">
                            		<p>Get Started</p>
                            	</div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>           
    </section>

    <section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<div class="km m-tc">
						<h2>Working of Pythru Neo Banking </h2>
						<p class="pt5">One platform - Multiple Benefits</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="gf">
						<div class="gfi">
							<img src="img/multibank/accounting-opening.svg">
						</div>
						<div class="gfc">
							<h3>Account opening</h3>
							<p>Digital banking with the secure <br class="hidden-xs">and transparent bank <br class="hidden-xs">account </p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="gf">
						<div class="gfi">
							<img src="img/multibank/multiple-bank-account.svg">
						</div>
						<div class="gfc">
							<h3>Multiple bank accounts</h3>
							<p>Link Existing bank accounts and easy control of all of them in one place</p>
						</div>
					</div>
				</div>	
				<div class="col-md-4">
					<div class="gf">
						<div class="gfi">
							<img src="img/multibank/records-and-reconciliation.svg">
						</div>
						<div class="gfc">
							<h3>Records & Reconciliation</h3>
							<p>Transaction reconciliation for all bank accounts and their accounting and book-keeping</p>
						</div>
					</div>
				</div>	
				<div class="col-md-offset-2 col-md-4">
					<div class="gf">
						<div class="gfi">
							<img src="img/multibank/create-invoices.svg">
						</div>
						<div class="gfc">
							<h3>Create invoices</h3>
							<p>Automatic invoice generation and sending them directly to the customers</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="gf">
						<div class="gfi">
							<img src="img/multibank/payout-management.svg">
						</div>
						<div class="gfc">
							<h3>Payout management</h3>
							<p>Single and multiple Bank account transfers, wallet, UPI, or card payments</p>
						</div>
					</div>					
				</div>		
			</div>
		</div>
	</section>

	<section class="sec-padding">
    	<div class="bg-blue">
    		<div class="container">
    			<div class="row">
    				<div class="cu-main">
    					<div class="col-md-5">
    						<div class="cu-main-text">
    							<h2 class="pb0">Modern Banking Solution with Pythru</h2>
    							<p class="text-white pb30">Best one in India for smart neo-banking </p>
    							<a href="#">Get Started <i class="bi bi-arrow-right"></i></a>
    						</div>
    					</div>
    					<div class="col-md-7">
    						<div class="cu-img hidden-xs">
    							<img src="img/payroll/image 37.svg" class="img-responsive">
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>

    <section class="sec-padding">
    	<div class="container">
    		<div class="row">
    			<div class="text-center km mb0">
    				<span>Lorem ipsum</span>
					<h2>Why choose Pythru for <br class="hidden-xs">Multi-Bank Connect?</h2><h2>				
    			</h2></div>
    		</div>
    		<div class="row">
    			<div class="col-md-offset-1 col-md-10">
					<div class="tick">
						<div class="col-md-4 col-sm-6">
							<h4>Online Banking Solution</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Easy Dashboard</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Link multiple bank accounts</h4>
						</div>
						<div class="col-md-offset-2 col-md-4 col-sm-6">
							<h4>Real-time reporting</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Monitor Cash flow</h4>
						</div>
					</div>
				</div>
			</div>
    	</div>
    </section>

	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>
	
</body>
</html>