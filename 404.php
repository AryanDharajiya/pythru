<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>404 Not Found</title>	
	<?php include 'include/css.php';?> 
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="sec-padding">
		<div class="container sec-padding">
			<div class="row">
				<div class="col-md-4 col-sm-5 mt20 ccc-mb">
					<span class="com-span">Page Not Found</span>
					<h2 class="ccc-h2">Oh No! Error 404</h2>	
					<p class="com-p pt10 pb20">We suggest you go to homepage <br class="hidden-xs">while we fixing the problem...</p>
					<div class="ccc-a mt20">
						<a href="/"><span>BACK TO HOMEPAGE</span></a>
					</div>	
				</div>		
				<div class="col-md-7 col-sm-7">
					<div class="404-img">
						<img src="img/common/404.svg" class="img-responsive">
					</div>
				</div>	
			</div>
		</div>
	</section>

	<?php include 'include/footer.php';?>

	<?php include 'include/js.php';?>
	
</body>
</html>