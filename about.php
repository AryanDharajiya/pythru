<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>About PyThru Fintech Services</title>	
	<meta name="description" content="Secure Online Payment Solution for your business payments and finances with easy onboarding, robust APIs and integration services. Get started now!">
	<meta name="keywords" content="Online Payment Gateway, eCommerce Payment Gateway, Easy Online Payments, Payouts, Business Banking Solution">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/about.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="sb-first-bg-img">
		<div class="ab-first-bg ab-first-height">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="ab-first-text text-center">						
							<h1>About PyThru</h1>
						</div>					
					</div>
				</div>				
			</div>
		</div>
	</section>

	<div class="container">
		<div class="row ab-f-pad">
			<div class="col-md-offset-2 col-md-8">
				<div class="ab-first-box">
					<h2>The Fastest Growing Digital Network</h2>
					<p>The Future of India’s fastest-growing <br class="hidden-xs">Digital Payments & Banking</p>
				</div>
			</div>
		</div>
	</div>

	<div class="clearfix"></div>

	<section class="sec-padding sec-mar">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<div class="ab-s-img wc">
						<img src="img/about/empowering-business.svg" class="img-responsive">
					</div>
				</div>
				<div class="col-md-5 col-sm-12">
					<div class="ab-s-content wc">
						<h2>Empowering all kinds of Businesses</h2>
						<p class="pt10">We provide a customized portfolio with all kinds of services for every business kinds as per their specific requirements. We cater to the needs of small startups to the largest companies.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<h2 class="c-h2 pb15">Why PayThru?</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-offset-2 col-md-10">
					<div class="points">
						<div class="col-md-4 col-sm-4 col-xs-6">
							<h4>Easy</h4>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6">
							<h4>Reliable</h4>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6">
							<h4>Modern</h4>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6">
							<h4>Secure</h4>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6">
							<h4>Affordable</h4>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6">
							<h4>Comprehensive</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="sec-t">
					<div class="col-md-4 col-sm-4">
						<div class="box">
							<img src="img/about/banking.svg">
							<h3>Banking</h3>
							<p>Current Account, Payouts, Cash Management, Payroll, etc.</p>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="box">
							<img src="img/about/accounting.svg">
							<h3>Accounting</h3>
							<p>Accounting and Bookkeeping, e-way bill, GST Return, Invoices, etc.</p>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="box">
							<img src="img/about/pg.svg">
							<h3>Payment Gateway</h3>
							<p>Online Payments, Payment links, Payment Button, Subscriptions, etc.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="frame">
					<img src="img/about/color-frame.svg">
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="abt-cont-img">
						<img src="img/about/about-content.svg">
					</div>
				</div>
				<div class="col-md-8">
					<div class="abt-cont">
						<h3>About Us</h3>
						<h4>PayThru technology has been developed to provide complete payments solutions</h4>
						<p>including the smooth transacting of the receipts and payments of business funds.We have a sheer dedication for startups & MSMEs and provide a tailored payment solution to large enterprises.</p>
					</div>
				</div>
			</div>
			<div class="row pt10">
				<div class="col-md-12">
					<div class="abt-cont">
						<p>Any business type including Freelancers, Offline businesses, eCommerce, Education, NGOs, Travel, Gaming, SaaS, etc. would get a tailored solution as per their customer demands and business needs.</p>
						<p>Taking the advantage of a modern & smooth checkout with easy integration options with the PayThru payment gateway, the merchants can collect easy online payments via website, mobile application, and payment links with multiple payment modes after a smooth registration and activation process.</p>
						<p>The PayThru Banking and Accounting Products are the feathers in the wings of PayThru along with the Payment Gateway.</p>
						<p>Thus, it would be a one-stop-place with a complete payments portfolio for Indian business finances.</p>						
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="frame frame-float">
					<img src="img/about/lite-frame.svg">
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="sec-add">
					<div class="col-md-7">
						<img src="img/about/about-add.svg" class="img-responsive">
					</div>
					<div class="col-md-3">
						<h4>Registered Address</h4>
						<p>Billermath, Ranaghat, Nadia, West Bengal 741201</p>

						<h4 class="h4-pad">Office Address</h4>
						<p>Srijan Corporate Park, GP Block, Sector V, Bidhannagar, Kolkata, West Bengal 700091</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h3 class="pt15">Our offices are located in Kolkatta & Bengluru, India</h3>
				</div>
			</div>
		</div>
	</section>


	<div class="team-graphic">
		<div class="team-graphic1 hidden-xs">
			<img src="img/about/home.svg">	
		</div>
		<div class="team-bg">			
			<div class="container">
				 <div class="row sec-padding">
				 	<div class="text-center">
						<h2 class="c-h2">PyThru Team</h2>
					</div>
				 </div>
				 <div class="row">
				 	<div class="col-md-6">
				 		<div class="team-box">
				 			<div class="d-flex">
					 			<div class="team-img">
					 				<img src="img/about/dipankar.svg">
					 			</div>
					 			<div class="team-content">
					 				<h4>Dipankar Biswas</h4>
					 				<p>Co-founder & Director</p>
					 			</div>
					 		</div>
				 			<div class="team-social">
				 				<a href="">
				 					<img src="img/about/linkedin.svg">
				 				</a>
				 			</div>
				 		</div>
				 	</div>
				 	<div class="col-md-6 it-mar">
				 		<div class="team-box">
				 			<div class="d-flex">
					 			<div class="team-img">
					 				<img src="img/about/rana-sarkar.jpeg">
					 			</div>
					 			<div class="team-content">
					 				<h4>Rana Sarkar</h4>
					 				<p>Director</p>
					 			</div>
					 		</div>
				 			<div class="team-social">
				 				<a href="">
				 					<img src="img/about/linkedin.svg">
				 				</a>
				 			</div>
				 		</div>
				 	</div>
				 </div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container sec-top-padding">
			<div class="row">
				<div class="col-md-12">
					<div class="text-center">
						<span class="com-span">PyThru Goals</span>
						<h2 class="c-h2">Our Mission</h2>
						<p class="com-p color1 pt10">Our goal is to empower businesses with payments & <br class="hidden-xs">tech led innovative solutions</p>
					</div>
				</div>
			</div>
		</div>
	</section>


	<?php include 'include/footer.php';?>

	<?php include 'include/js.php';?>
	
</body>
</html>