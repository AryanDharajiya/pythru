<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>PyThru Online Payment Gateway Pricing | Payment Gateway Charges</title>	
	<meta name="description" content="Get PyThru at affordable payment gateway pricing. No other online payment gateway charges applied. Zero setup fees & zero maintenance charge.">
	<meta name="keywords" content="payment gateway charges, payment gateway pricing India, payment gateway pricing">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/pricing.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="main-bg">
		<div class="sec-padding ph-bg">
			<div class="container sec-padding">
				<div class="row">
					<div class="text-center">
						<span class="com-span fw800">Pricing</span>
						<h1 class="text-white pt10 fw800">Why PyThru Payment Gateway?</h1>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="container text-center sec-padding pb0">              
		<h2 class="c-h2">Affordable Pricing and Packages <br class="hidden-xs">Choose your Best One</h2>       
	</div>
	<section class="package-section background-shape-right sec-padding hidden-xs hidden-sm">
        <div class="container-fluid">            
            <div class="row justify-content-center prpad">
            	<div class="col-md-4 pnp ppr">
            		<div class="ptp">
            			<h3>Flat TDR <br>1.95%</h3>
            		</div>
            		<div class="card-body cbmar">
                        <ul class="list-unstyled text-sm pricing-feature-list">
                            <li>Open Account</li>
                            <li>Online Bank Account Number & UPI ID</li>
                            <li>Current Account</li>
                            <li>Connect Multiple Bank Accounts</li>
                            <li class="ulbg">
                            	<span class="sw">Collect Payments</span><br>
                            	<span>Collect payments online via credit/debit card, netbanking, UPI, NEFT / IMPS / RTGS</span>
                            </li>
                            <li>Virtual Accounts</li>
                            <li>
                            	<span class="sw">Payment Gateway</span><br>
                            	<span>(via Credit & Debit cards, Netbanking, UPI & Wallets)</span>
                            </li>
                            <li>Amex & Diners Card, International Cards & Corporate Credit Cards</li>
                            <li>Cross Border</li>
                            <li>UPI AutoPay</li>
                            <li>E-mandate</li>
                            <li class="ulbg">
                            	<span class="sw">Vendor & Salary Payouts</span><br>
                            	<span>Pay via NEFT / IMPS / RTGS / UPI / digital cheque / cash deposit</span>
                            </li>
                            <li>
                            	<span class="sw">IMPS & UPI</span><br>
                            	<span>0 to ₹ 1000</span>
                            </li>
                            <li>₹ 1000 to ₹ 25,000</li>
                            <li>₹ 25,000+</li>
                            <li>
                            	<span class="sw">NEFT & RTGS</span><br>
                            	<span>Unlimited</span>
                            </li>
                            <li>
                            	<span class="sw">Digital Cheque</span><br>
                            	<span>Unlimited</span>
                            </li>
                            <li class="ulbg">
                            	<span class="sw">Accounting</span>
                            </li>
                            <li>GST compliant invoices</li>
                            <li>
                            	<span>Account Receivables & Payables</span><br>
                            	<span>(Estimate, Credit & Debit Note, Cash Memo, etc)</span>
                            </li>
                            <li>
                            	<span>Basic Accounting Reports</span><br>
                            	<span>(Cashflow, Profit & Loss, Balance Sheet)</span>
                            </li>
                            <li>
                            	<span>Advanced Accounting Reports</span><br>
                            	<span>(General Ledger, Trial Balance, Aged Receivables & Payables)</span>
                            </li>
                        </ul>
                    </div>
            	</div>
            	<div class="col-md-8 pnp ppl">
            		<div class="col-md-4 pnp ppl">
                        <div class="card text-center single-pricing-pack">
                        	<div class="p-5">
                        		<h3>Basic</h3>
	                            <h4>Free</h4>
	                            <div class="pricing-img mt-3 mb-4">
	                                <img src="https://appco.themetags.com/img/basic.svg" alt="pricing img" class="img-fluid">
	                            </div>
	                        </div>

                            <div class="card-body p-0">
                                <ul class="list-unstyled text-sm pricing-feature-list">
                                    <li>Limited</li>
                                    <li><img src="https://open.money/assets/images/check@1.5x.svg"></li>
                                    <li>Optional</li>
                                    <li>5 A/c</li>
                                    <li class="ulpad ulbg"></li>
                                    <li>₹ 5</li>
                                    <li class="ulpad1">1.95%</li>
                                    <li>3.75%</li>
                                    <li>0.90%</li>
                                    <li>₹ 18</li>
                                    <li>₹ 18</li>
                                    <li class="ulpad ulbg"></li>
                                    <li class="ulpad1">₹ 3</li>
                                    <li>₹ 4</li>
                                    <li>₹ 7</li>
                                    <li class="ulpad1">₹ 3</li>
                                    <li class="ulpad1">₹ 3</li>
                                    <li class="ulpad2 ulbg"></li>
                                    <li>Unlimited</li>
                                    <li class="ulpad1">Unlimited</li>
                                    <li class="ulpad1">Unlimited</li>
                                    <li class="ulpad1">Unlimited</li>
                                </ul>
                                <a href="#" class="btn outline-btn btn-mar" target="_blank">Get Started</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 pnp">
                        <div class="card text-center single-pricing-pack">
                        	<div class="p-5">
                        		<h3>Standard</h3>
	                            <h4>₹2999<span>/year</span></h4>
	                            <div class="pricing-img mt-3 mb-4">
	                                <img src="https://appco.themetags.com/img/standard.svg" alt="pricing img" class="img-fluid">
	                            </div>
	                        </div>

                            <div class="card-body p-0">
                                <ul class="list-unstyled text-sm pricing-feature-list">
                                    <li>Advanced</li>
                                    <li><img src="https://open.money/assets/images/check@1.5x.svg"></li>
                                    <li>Optional</li>
                                    <li>10 A/c</li>
                                    <li class="ulpad ulbg"></li>
                                    <li>₹ 3</li>
                                    <li class="ulpad1">1.85%</li>
                                    <li>3.50%</li>
                                    <li>0.90%</li>
                                    <li>₹ 10</li>
                                    <li>₹ 10</li>
                                    <li class="ulpad ulbg"></li>
                                    <li class="ulpad1">₹ 2.5</li>
                                    <li>₹ 3.5</li>
                                    <li>₹ 6.5</li>
                                    <li class="ulpad1">₹ 2.5</li>
                                    <li class="ulpad1">₹ 2.5</li>
                                    <li class="ulpad2 ulbg"></li>
                                    <li>Unlimited</li>
                                    <li class="ulpad1">Unlimited</li>
                                    <li class="ulpad1">Unlimited</li>
                                    <li class="ulpad1">Unlimited</li>
                                </ul>
                                <a href="#" class="btn outline-btn btn-mar" target="_blank">Get Started</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 pnp">
                        <div class="card text-center popular-price single-pricing-pack">
                        	<div class="p-5">
                        		<h3>Unlimited</h3>
	                            <h4>₹5999<span>/year</span></h4>
	                            <div class="pricing-img mt-3 mb-4">
	                                <img src="https://appco.themetags.com/img/unlimited.svg" alt="pricing img" class="img-fluid">
	                            </div>
	                        </div>

                            <div class="card-body p-0">
                                <ul class="list-unstyled text-sm pricing-feature-list">
                                    <li>Full Access</li>
                                    <li><img src="https://open.money/assets/images/check@1.5x.svg"></li>
                                    <li>Optional</li>
                                    <li>15 A/c</li>
                                    <li class="ulpad ulbg"></li>
                                    <li>₹ 2</li>
                                    <li class="ulpad1">1.75%</li>
                                    <li>2.90%</li>
                                    <li>0.90%</li>
                                    <li>₹ 8</li>
                                    <li>₹ 8</li>
                                    <li class="ulpad ulbg"></li>
                                    <li class="ulpad1">₹ 1</li>
                                    <li>₹ 3</li>
                                    <li>₹ 6</li>
                                    <li class="ulpad1">₹ 2</li>
                                    <li class="ulpad1">₹ 2</li>
                                    <li class="ulpad2 ulbg"></li>
                                    <li>Unlimited</li>
                                    <li class="ulpad1">Unlimited</li>
                                    <li class="ulpad1">Unlimited</li>
                                    <li class="ulpad1">Unlimited</li>
                                </ul>
                                <a href="#" class="btn solid-btn btn-mar" target="_blank">Get Started</a>
                            </div>
                        </div>
                    </div>
            	</div>                    
            </div>
        </div>
    </section>

    <section class="sec-padding visible-xs visible-sm">
    	<div class="container">
			<div class="card single-pricing-pack p-5 mpcmt">
				<div>
	        		<h3>Basic</h3>
	        		<h4>Free</h4>	                
	            </div>
	            <div class="mt5">
	            	<a href="#" class="btn solid-btn" data-toggle="modal" data-target="#myModal1">Know More <i class="bi bi-caret-right"></i></a>
	            	<a href="#" class="btn outline-btn float-right" target="_blank">Get Started</a>
	            </div>
			</div>
			<div class="card single-pricing-pack p-5 mpcmt">
				<div>
	        		<h3>Standard</h3>
	        		<h4>₹2999<span>/year</span></h4>                
	            </div>
	            <div class="mt5">
	            	<a href="#" class="btn solid-btn" data-toggle="modal" data-target="#myModal2">Know More <i class="bi bi-caret-right"></i></a>
	            	<a href="#" class="btn outline-btn float-right" target="_blank">Get Started</a>
	            </div>
			</div>
			<div class="card popular-price single-pricing-pack p-5 mpcmt">
				<div>
	        		<h3>Unlimited</h3>
	        		<h4>₹5999<span>/year</span></h4>	                
	            </div>
	            <div class="mt5">
	            	<a href="#" class="btn outline-btn" data-toggle="modal" data-target="#myModal3">Know More <i class="bi bi-caret-right"></i></a>
	            	<a href="#" class="btn solid-btn float-right" target="_blank">Get Started</a>
	            </div>
			</div>
    	</div>
    </section>

	<section class="sec-padding">
		<div class="pr-bg">
			<div class="container sec-padding">
				<div class="row">
					<div class="col-md-12">
						<div class="c-plan text-center">
							<span class="com-span">Business Plan</span>
							<h3>Want to customize pricing <br class="hidden-xs">for your business??</h3>
							<a href="">Talk with us</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<div>
						<span class="com-span">Benifits of us</span>
						<h2 class="c-h2">PyThru is the best choise</h2>	
						<p class="com-p pt10">PyThru offers multiple payment mode options. <br class="hidden-xs">Let your customers pay with their preferred payment method</p>	
					</div>
				</div>
				<div>
					<div class="col-md-12 col-sm-12">
						<div class="col-md-offset-3 col-md-3 col-sm-offset-2 col-sm-4">
							<div class="points">
								<h4>No setup fees</h4>
								<h4>Dedicated Manager</h4>
							</div>
						</div>
						<div class="col-md-offset-1 col-md-3 col-sm-offset-1 col-sm-4">
							<div class="points">
								<h4>24x7 Support</h4>
								<h4>Easy to Integrate</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Modal 1-->
	<div class="modal fade" id="myModal1" role="dialog">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header border-0">
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	               <div class="card border-0">
		               	<h3>Basic</h3>
		               	<h4 class="pb0 mb0">Free</h4>
	               </div>
	            </div>
	            <div class="modal-body">
	            	<div class="table-responsive">          
					  <table class="table table-bordered">
					    <tbody>
					      <tr>
					        <td>Open Account</td>
					        <td>Limited</td>
					      </tr>
					      <tr>
					        <td>Online Bank Account Number & UPI ID</td>
					        <td><img src="https://open.money/assets/images/check@1.5x.svg"></td>
					      </tr>
					      <tr>
					      	<td>Current Account</td>
					      	<td>Optional</td>
					      </tr>
					      <tr>
					      	<td>Connect Multiple Bank Accounts</td>
					      	<td>5 A/c</td>
					      </tr>
					      <tr class="ulbg">
					      	<td>
					      		<span class="sw">Collect Payments</span><br>
					      		<span>Collect payments online via <br>credit/debit card, netbanking, UPI,  <br>NEFT / IMPS / RTGS</span>
					      	</td>
					      	<td></td>
					      </tr>
					      <tr>
					      	<td>Virtual Accounts</td>
					      	<td>₹ 5</td>
					      </tr>
					      <tr class="ulbg">
					      	<td>
					      		<span class="sw">Payment Gateway</span><br>
					      		<span>(via Credit & Debit cards,  <br>Netbanking, UPI & Wallets)</span>
					      	</td>
					      	<td>1.95%</td>
					      </tr>
					      <tr>
					      	<td>Amex & Diners Card, International  <br>Cards & Corporate Credit Cards</td>
					      	<td>3.75%</td>
					      </tr>
					      <tr>
					      	<td>Cross Border</td>
					      	<td>0.90%</td>
					      </tr>
					      <tr>
					      	<td>UPI AutoPay</td>
					      	<td>₹ 18</td>
					      </tr>
					      <tr>
					      	<td>E-mandate</td>
					      	<td>₹ 18</td>
					      </tr>
					      <tr class="ulbg">
					      	<td>
					      		<span class="sw">Vendor &amp; Salary Payouts</span><br>
					      		<span>Pay via NEFT / IMPS / RTGS / <br>UPI / digital cheque / cash deposit</span>
					      	</td>
					      	<td></td>
					      </tr>
					      <tr>
					      	<td>
					      		<span class="sw">IMPS &amp; UPI</span><br>
					      		<span>0 to ₹ 1000</span>
					      	</td>
					      	<td>₹ 3</td>
					      </tr>
					      <tr>
					      	<td>₹ 1000 to ₹ 25,000</td>
					      	<td>₹ 4</td>
					      </tr>
					      <tr>
					      	<td>₹ 25,000+</td>
					      	<td>₹ 7</td>
					      </tr>
					      <tr>
					      	<td>
                            	<span class="sw">NEFT &amp; RTGS</span><br>
                            	<span>Unlimited</span>
					      	</td>
					      	<td>₹ 3</td>
					      </tr>
					      <tr>
					      	<td>
                            	<span class="sw">Digital Cheque</span><br>
                            	<span>Unlimited</span>
					      	</td>
					      	<td>₹ 3</td>
					      </tr>
					      <tr class="ulbg">
					      	<td>Accounting</td>
					      	<td></td>
					      </tr>
					      <tr>
					      	<td>GST compliant invoices</td>
					      	<td>Unlimited</td>
					      </tr>
					      <tr>
					      	<td>
					      		<span>Account Receivables &amp; Payables</span><br>
                            	<span>(Estimate, Credit &amp; Debit Note,<br> Cash Memo, etc)</span>
					      	</td>
					      	<td>Unlimited</td>
					      </tr>
					      <tr>
					      	<td>
					      		<span>Basic Accounting Reports</span><br>
                            	<span>(Cashflow, Profit &amp; Loss, <br>Balance Sheet)</span>
					      	</td>
					      	<td>Unlimited</td>
					      </tr>
					      <tr>
					      	<td>
					      		<span>Advanced Accounting Reports</span><br>
                            	<span>(General Ledger, Trial Balance, <br>Aged Receivables &amp; Payables)</span>
					      	</td>
					      	<td>Unlimited</td>
					      </tr>
					    </tbody>
					  </table>
					</div>
	            </div>
	        </div>
	    </div>
	</div>

	<!-- Modal 2-->
	<div class="modal fade" id="myModal2" role="dialog">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header border-0">
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	               <div class="card border-0">
		               	<h3>Standard</h3>
		               	<h4 class="pb0 mb0">₹2999/year</h4>
	               </div>
	            </div>
	            <div class="modal-body">
	            	<div class="table-responsive">          
					  <table class="table table-bordered">
					    <tbody>
					      <tr>
					        <td>Open Account</td>
					        <td>Advanced</td>
					      </tr>
					      <tr>
					        <td>Online Bank Account Number & UPI ID</td>
					        <td><img src="https://open.money/assets/images/check@1.5x.svg"></td>
					      </tr>
					      <tr>
					      	<td>Current Account</td>
					      	<td>Optional</td>
					      </tr>
					      <tr>
					      	<td>Connect Multiple Bank Accounts</td>
					      	<td>10 A/c</td>
					      </tr>
					      <tr class="ulbg">
					      	<td>
					      		<span class="sw">Collect Payments</span><br>
					      		<span>Collect payments online via <br>credit/debit card, netbanking, UPI,  <br>NEFT / IMPS / RTGS</span>
					      	</td>
					      	<td></td>
					      </tr>
					      <tr>
					      	<td>Virtual Accounts</td>
					      	<td>₹ 3</td>
					      </tr>
					      <tr class="ulbg">
					      	<td>
					      		<span class="sw">Payment Gateway</span><br>
					      		<span>(via Credit & Debit cards,  <br>Netbanking, UPI & Wallets)</span>
					      	</td>
					      	<td>1.85%</td>
					      </tr>
					      <tr>
					      	<td>Amex & Diners Card, International  <br>Cards & Corporate Credit Cards</td>
					      	<td>3.50%</td>
					      </tr>
					      <tr>
					      	<td>Cross Border</td>
					      	<td>0.90%</td>
					      </tr>
					      <tr>
					      	<td>UPI AutoPay</td>
					      	<td>₹ 10</td>
					      </tr>
					      <tr>
					      	<td>E-mandate</td>
					      	<td>₹ 10</td>
					      </tr>
					      <tr class="ulbg">
					      	<td>
					      		<span class="sw">Vendor &amp; Salary Payouts</span><br>
					      		<span>Pay via NEFT / IMPS / RTGS / <br>UPI / digital cheque / cash deposit</span>
					      	</td>
					      	<td></td>
					      </tr>
					      <tr>
					      	<td>
					      		<span class="sw">IMPS &amp; UPI</span><br>
					      		<span>0 to ₹ 1000</span>
					      	</td>
					      	<td>₹ 2.5</td>
					      </tr>
					      <tr>
					      	<td>₹ 1000 to ₹ 25,000</td>
					      	<td>₹ 3.5</td>
					      </tr>
					      <tr>
					      	<td>₹ 25,000+</td>
					      	<td>₹ 6.5</td>
					      </tr>
					      <tr>
					      	<td>
                            	<span class="sw">NEFT &amp; RTGS</span><br>
                            	<span>Unlimited</span>
					      	</td>
					      	<td>₹ 2.5</td>
					      </tr>
					      <tr>
					      	<td>
                            	<span class="sw">Digital Cheque</span><br>
                            	<span>Unlimited</span>
					      	</td>
					      	<td>₹ 2.5</td>
					      </tr>
					      <tr class="ulbg">
					      	<td>Accounting</td>
					      	<td></td>
					      </tr>
					      <tr>
					      	<td>GST compliant invoices</td>
					      	<td>Unlimited</td>
					      </tr>
					      <tr>
					      	<td>
					      		<span>Account Receivables &amp; Payables</span><br>
                            	<span>(Estimate, Credit &amp; Debit Note,<br> Cash Memo, etc)</span>
					      	</td>
					      	<td>Unlimited</td>
					      </tr>
					      <tr>
					      	<td>
					      		<span>Basic Accounting Reports</span><br>
                            	<span>(Cashflow, Profit &amp; Loss, <br>Balance Sheet)</span>
					      	</td>
					      	<td>Unlimited</td>
					      </tr>
					      <tr>
					      	<td>
					      		<span>Advanced Accounting Reports</span><br>
                            	<span>(General Ledger, Trial Balance, <br>Aged Receivables &amp; Payables)</span>
					      	</td>
					      	<td>Unlimited</td>
					      </tr>
					    </tbody>
					  </table>
					</div>
	            </div>
	        </div>
	    </div>
	</div>

	<!-- Modal 3-->
	<div class="modal fade" id="myModal3" role="dialog">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header border-0">
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	               <div class="card border-0">
		               	<h3>Unlimited</h3>
		               	<h4 class="pb0 mb0">₹5999/year</h4>
	               </div>
	            </div>
	            <div class="modal-body">
	            	<div class="table-responsive">          
					  <table class="table table-bordered">
					    <tbody>
					      <tr>
					        <td>Open Account</td>
					        <td>Full Access</td>
					      </tr>
					      <tr>
					        <td>Online Bank Account Number & UPI ID</td>
					        <td><img src="https://open.money/assets/images/check@1.5x.svg"></td>
					      </tr>
					      <tr>
					      	<td>Current Account</td>
					      	<td>Optional</td>
					      </tr>
					      <tr>
					      	<td>Connect Multiple Bank Accounts</td>
					      	<td>15 A/c</td>
					      </tr>
					      <tr class="ulbg">
					      	<td>
					      		<span class="sw">Collect Payments</span><br>
					      		<span>Collect payments online via <br>credit/debit card, netbanking, UPI,  <br>NEFT / IMPS / RTGS</span>
					      	</td>
					      	<td></td>
					      </tr>
					      <tr>
					      	<td>Virtual Accounts</td>
					      	<td>₹ 2</td>
					      </tr>
					      <tr class="ulbg">
					      	<td>
					      		<span class="sw">Payment Gateway</span><br>
					      		<span>(via Credit & Debit cards,  <br>Netbanking, UPI & Wallets)</span>
					      	</td>
					      	<td>1.75%</td>
					      </tr>
					      <tr>
					      	<td>Amex & Diners Card, International  <br>Cards & Corporate Credit Cards</td>
					      	<td>2.90%</td>
					      </tr>
					      <tr>
					      	<td>Cross Border</td>
					      	<td>0.90%</td>
					      </tr>
					      <tr>
					      	<td>UPI AutoPay</td>
					      	<td>₹ 8</td>
					      </tr>
					      <tr>
					      	<td>E-mandate</td>
					      	<td>₹ 8</td>
					      </tr>
					      <tr class="ulbg">
					      	<td>
					      		<span class="sw">Vendor &amp; Salary Payouts</span><br>
					      		<span>Pay via NEFT / IMPS / RTGS / <br>UPI / digital cheque / cash deposit</span>
					      	</td>
					      	<td></td>
					      </tr>
					      <tr>
					      	<td>
					      		<span class="sw">IMPS &amp; UPI</span><br>
					      		<span>0 to ₹ 1000</span>
					      	</td>
					      	<td>₹ 1</td>
					      </tr>
					      <tr>
					      	<td>₹ 1000 to ₹ 25,000</td>
					      	<td>₹ 3</td>
					      </tr>
					      <tr>
					      	<td>₹ 25,000+</td>
					      	<td>₹ 6</td>
					      </tr>
					      <tr>
					      	<td>
                            	<span class="sw">NEFT &amp; RTGS</span><br>
                            	<span>Unlimited</span>
					      	</td>
					      	<td>₹ 2</td>
					      </tr>
					      <tr>
					      	<td>
                            	<span class="sw">Digital Cheque</span><br>
                            	<span>Unlimited</span>
					      	</td>
					      	<td>₹ 2</td>
					      </tr>
					      <tr class="ulbg">
					      	<td>Accounting</td>
					      	<td></td>
					      </tr>
					      <tr>
					      	<td>GST compliant invoices</td>
					      	<td>Unlimited</td>
					      </tr>
					      <tr>
					      	<td>
					      		<span>Account Receivables &amp; Payables</span><br>
                            	<span>(Estimate, Credit &amp; Debit Note,<br> Cash Memo, etc)</span>
					      	</td>
					      	<td>Unlimited</td>
					      </tr>
					      <tr>
					      	<td>
					      		<span>Basic Accounting Reports</span><br>
                            	<span>(Cashflow, Profit &amp; Loss, <br>Balance Sheet)</span>
					      	</td>
					      	<td>Unlimited</td>
					      </tr>
					      <tr>
					      	<td>
					      		<span>Advanced Accounting Reports</span><br>
                            	<span>(General Ledger, Trial Balance, <br>Aged Receivables &amp; Payables)</span>
					      	</td>
					      	<td>Unlimited</td>
					      </tr>
					    </tbody>
					  </table>
					</div>
	            </div>
	        </div>
	    </div>
	</div>


	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>

	
</body>
</html>