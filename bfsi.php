<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Payment Gateway for Financial Services and Products | PyThru</title>	
	<meta name="description" content="Enable a complete online payments solution for your banking and financial platforms with the PyThru products suite.">
	<meta name="keywords" content="Payment Gateway for Financial Services, Online payments, Payouts, Payment Gateway, Payment links, Payment Pages, Payment Button">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/solutions.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="hero">
						<div class="content">
							<h1 class="c-h1">PyThru Payments for <span> Financial </span><br class="hidden-xs">Services Platforms</h1>
							<p class="pt10 pb30">Get PyThru, a one-stop customized payments solution for your <br class="hidden-xs">funds, loans, lending, wealth management, and insurance platform</p>
						</div> 
						<?php include 'include/common-signup.php';?>
					</div>
				</div>
				<div class="col-md-offset-1 col-md-6">
					<div class="hero-graphic hidden-sm hidden-xs">
						<img src="img/bfsi/hero.svg" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-bottom-padding">
		<div class="container">
			<div class="row">
				<div class="text-center ff-title">
					<h2><b>BFSI </b>centered PyThru Features</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<div class="ff">
						<img src="img/bfsi/payment-gateway.svg">
						<h3>Payment Gateway</h3>
						<p>Accept easy payments with multiple payment modes for premiums, SIPs, loan repayments, etc</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="ff">
						<img src="img/bfsi/disbursement.svg">
						<h3>Disbursement</h3>
						<p>Automate the payouts of the loans and advances for various financing needs in single or bulk</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="ff">
						<img src="img/bfsi/banking-solutions.svg">
						<h3>Banking Solutions</h3>
						<p>Manage the flow of payments from various parties such as customers, investors, etc with our all-in-one Banking Solution</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row point-pad">
				<div class="left-right-section">
					<div class="col-md-6 col-sm-6">
						<div class="lrs-img">
							<img src="img/bfsi/payment-link.svg" class="img-responsive">
						</div>
					</div>
					<div class="col-md-5 col-sm-6">
						<div class="lrs-pad m-center">
							<div>
								<span class="com-span">High Payment Success Rate</span>
								<h2 class="c-h2">Retry option and Payment Links</h2>	
								<p class="com-p pt10">Reduce the failed transaction rates with our high transaction success rate and payment retry option. Recover the failed/left payments by sending the payment links</p>	
							</div>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="ff-p-bg">
			<div class="container">
				<div class="row sec-padding">
					<div class="text-center ff-title1">
						<h2><b>Pricing</b> Plans</h2>
					</div>
					<div class="fp-main">
						<div class="col-md-4 col-sm-6">
							<div class="fp">
								<div class="fp-card b1">
									<div class="fp-detail">
										<div class="col-md-4">
											<b>2.5%</b>
										</div>
										<div class="col-md-8">
											<h4>Standard Pricing</h4>
											<p>Credit & Deibt card, UPI, wallets, Net banking & QR</p>
										</div>
										<div class="col-md-offset-2 col-md-9">
											<hr class="fp-line">
										</div>
										<div class="col-md-offset-2 col-md-4 col-xs-4">
											<div class="fp-price">
												<b>₹ 0.00</b>
												<p>Setup fees</p>
											</div>
										</div>
										<div class="col-md-6 col-xs-8">
											<div class="fp-price">
												<b>₹ 0.00</b>
												<p>No annual maintainance</p>
											</div>
										</div>
									</div>
								</div>	
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="fp">
								<div class="fp-card b2">
									<div class="fp-detail">
										<div class="col-md-4">
											<b>Talk</b>
											<span>with us</span>
										</div>
										<div class="col-md-8">
											<h4>Business Plan</h4>
											<p>To discuss the Customized Pricing </p>
										</div>
										<div class="col-md-offset-2 col-md-9">
											<hr class="fp-line">
										</div>
										<div class="fp-btn">
											<a href="">Contact Us</a>
										</div>
									</div>
								</div>	
							</div>
						</div>
						<div class="col-md-4 col-sm-12">
							<div class="fp-content">
								<h3>Best services with <br class="hidden-xs">two different pricing plans </h3>
								<p>Discuss yours as per <br class="hidden-xs">your business requirements</p>
								<div class="fp-btn1">
									<a href="">Know More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="text-center ff-title pb20">
					<h2><b>What’s </b> more with PyThru for your Financing Services</h2>
				</div>
			</div>
			<div class="row">
				<div class="tick">
					<div class="col-md-offset-2 col-md-3 col-sm-4">
						<h4>Smooth Onboarding</h4>
					</div>
					<div class="col-md-3 col-sm-4">
						<h4>Best Customer Support</h4>
					</div>
					<div class="col-md-3 col-sm-4">
						<h4>Resourceful Dashboard</h4>
					</div>
					<div class="col-md-offset-3 col-md-3 col-sm-offset-2 col-sm-4">
						<h4>Multi-Currency Support</h4>
					</div>
					<div class="col-md-3 col-sm-4">
						<h4>Recurring Subscriptions</h4>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row bg-black">
				<div class="col-md-2 hidden-xs">
					<div class="top-img">
						<img src="img/bfsi/lock.svg" class="img-responsive">
					</div>
				</div>
				<div class="col-md-8">
					<div class="ss">
						<h2>Secure and Protected</h2>
						<p>PyThru safety and security features <br class="hidden-xs">protect you and your clients</p>
					</div>
					<div class="ss-point">
						<div class="col-md-offset-2 col-md-4 col-sm-6">
							<h4>PCI DSS Compliant</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Encryption & SSL</h4>
						</div>
						<div class="col-md-offset-2 col-md-4 col-sm-6">
							<h4>Risk Verification</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Fraud Prevention</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-offset-1 col-md-4 col-sm-5">
	            	<div class="qute m-center">
	            		<img src="img/bfsi/qute.svg">
	            		<h2>What our Merchants <br class="hidden-xs">have to say…</h2>
	            	</div>
	            </div>
	            <div class="col-md-6 col-sm-7">
	                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	                    <!-- Wrapper for slides -->
	                    <div class="carousel-inner">
	                        <div class="item active">
	                            <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="img/home/man.svg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                    	<div class="content">
                                    		<h3>Smooth Onboarding to Integration</h3>
	                                    	<p>They provide an easy onboarding process and do not take much time for account activation. Then the integration was also effortless with its ready technical documentation and plugins. The experience with PyThru has been smooth.</p>
	                                    	<br>
	                                        <h4>Sonu Kumar</h4>
	                                        <span>Doon Cars</span>
	                                    </div>
                                    </div>	                               
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="img/home/man.svg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                    	<div class="content">
                                    		<h3>Best Banking Solutions</h3>
	                                    	<p>Fast and smooth transactions with a likewise checkout process. Our task has become way simpler and time-saving because of the automated reconciliation provided by its banking and accounting solutions.</p>
	                                        <h4>Sawan Kumar</h4>
	                                        <span>Etnasasta</span>
	                                    </div>
                                    </div>	                               
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="img/home/man.svg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                    	<div class="content">
                                    		<h3>PyThru suite of Products</h3>
	                                    	<p>The payments and disbursements are handled seamlessly with the PyThru products of payment gateway, payouts, and the banking suite. The smart dashboard for the respective functions has made the payment flow management simple.</p>
											<h4>Sharwan Kumar</h4>
											<span>The Various Store</span>
	                                    </div>
                                    </div>	                               
	                            </div>
	                        </div>
	                        <div class="item">
	                            <div class="row pt20"> 
                                    <div class="col-md-2">
                                        <img src="img/home/man.svg" class="img-responsive">
                                    </div>
                                    <div class="col-md-10">
                                    	<div class="content">
                                    		<h3>High Transaction Success Rate</h3>
	                                    	<p>PyThru has a very high transaction success rate that is very beneficial for our business operations. The decrease in the failed transactions has increased our revenue. The PyThru Payment links make payment acceptance easy through Whatsapp, email, etc.</p>
	                                        <h4>Bikram Biswas</h4>
											<span>Velvery Private Limited</span>
	                                    </div>
                                    </div>	                               
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-md-offset-1 col-md-5">
		                <div class="controls testimonial_control pull-right">
		                    <a class="left bi bi-chevron-left btn-default testimonial_btn fw900" href="#carousel-example-generic" data-slide="prev"></a>

		                    <a class="right bi bi-chevron-right btn-default testimonial_btn fw900" href="#carousel-example-generic" data-slide="next"></a>
		                </div>
		            </div>
	            </div>
	        </div>
	    </div>
	</section>

	<section class="sec-padding">
	    <div class="container">
	        <div class="row">
	        	<div class="ff-box-main">
	        		<div class="col-md-offset-1 col-md-5 col-sm-6">
	        			<div class="ff-box">
	        				<div class="ff-box-img">
		        				<img src="img/bfsi/red-circle.svg">
		        				<h3>Risk Management <br class="hidden-xs">software</h3>
	        				</div>
	        				view product
	        			</div>
	        		</div>
	        		<div class="col-md-5 col-sm-6">
	        			<div class="ff-box">
	        				<div class="ff-box-img">
		        				<img src="img/bfsi/blue-circle.svg">
		        				<h3>Risk Management <br class="hidden-xs">software</h3>
	        				</div>
	        				view product
	        			</div>
	        		</div>
	        	</div>
	        </div>
	    </div>
	</section>

	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>

	
</body>
</html>