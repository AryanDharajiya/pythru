<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Hiring Now | Work with Pythru</title>	
	<meta name="description" content="Work with Pythru Payment gateway with robust APIs and affordable pricing. Check out the job openings & know why Pythru is the best company to work with.">
	<meta name="keywords" content="Payment Gateway, PyThru jobs, Hiring, job openings, Join Now, Join Pythru">

	<?php include 'include/css.php';?> 
	<link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/career.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="main-bg">
		<div class="sec-padding ph-bg">
			<div class="container sec-padding">
				<div class="row">
					<div class="text-center">
						<span class="com-span fw900">Career</span>
						<h1 class="text-white pt10 fw900">We are looking for you!</h1>
						<p class="head-p">Pythru develops the best fintech and payments products for all types <br class="hidden-xs">of businesses. With the eyes on empowering these businesses by <br class="hidden-xs">extending the reach of financial services, we are moving ahead and we <br class="hidden-xs">want you to be a part of this journey.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="bg1 bg1-height">
		<div class="sec-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="wcc">
							<div class="owl-carousel testimonial" id="carousel">
			                    <div class="item">
			                        <div class="wcc-img">
			                        	<img src="img/career/slide1.svg" class="img-responsive">
			                        </div>        
			                    </div>
			                    <div class="item">
			                        <div class="wcc-img">
			                        	<img src="img/career/slide2.svg" class="img-responsive">
			                        </div>        
			                    </div>
			                    <div class="item">
			                        <div class="wcc-img">
			                        	<img src="img/career/slide3.svg" class="img-responsive">
			                        </div>        
			                    </div>
			                    <div class="item">
			                        <div class="wcc-img">
			                        	<img src="img/career/slide4.svg" class="img-responsive">
			                        </div>        
			                    </div>
			                </div>
						</div>
					</div>
					<div class="col-md-offset-1 col-md-5">
						<div class="sc">
							<h2>Hard Work with Fun!</h2>
							<p>Hard Work and Fun go hand-in-hand at Pythru. Because we believe that all work and no fun is bad for yours and our growth. We make sure that you enjoy the work here.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-6"></div>
				<div class="col-md-offset-1 col-md-5">
					<div class="why">
						<h2>Who should apply?</h2>
						<p>If you have the excitement, passion, and power; unite with us and help us bring the change to the Payments and Finance platform. There’s a lot to do and we need your mind, heart, and all your hands on deck! We are looking for you…</p>
					</div>					
				</div>
				<div class="col-md-12">
					<div class="why">
						<p>We want fresh minds for different operations even if you do not have a background in the finance platform but are learners and can flourish performing skillfully. We also want the specialists and experts who have already earned the growth in the selected segment. We want to provide you with an environment where your abilities and capabilities grow multifold.</p>
					</div>
				</div>				
			</div>
		</div>
	</section>

	<section class="bg-lite">
		<div class="sec-padding">
			<div class="container">
				<div class="row">
					<div class="apply-now">
						<div class="col-md-offset-2 col-md-5">
							<h2>Let us Grow together! </h2>
							<p>Check out the Openings</p>
						</div>
						<div class="col-md-4">
							<div class="btn">
								<a href="#">Apply Now</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sub-bg">
		<div class="sec-padding">
			<div class="container">
				<div class="row">
					<div class="sub-main">
						<div class="col-md-5">
							<div class="sub-pad">
								<div class="sub">
									<h2>Competitive Perks and Benefits</h2>
									<p>Along with the opportunity to work for the payments and finance products for the businesses and to grow in the Fintech field; we offer various perks and benefits too:</p>
								</div>
							</div>
						</div>
						<div class="col-md-7">
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="sub-feature">
										<div class="sub-feature-i">
											<img src="img/career/sports.svg">
											<h3>Outings and Sports</h3>
										</div>
										<p>Company & team outings, offsites, sports activities, and events are kind of our thing</p>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="sub-feature">
										<div class="sub-feature-i">
											<img src="img/career/insurance.svg">
											<h3>Insurance</h3>
										</div>
										<p>Health and Group insurance cover and medical benefits because we care for our employees</p>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="sub-feature">
										<div class="sub-feature-i">
											<img src="img/career/canteen.svg">
											<h3>Canteen</h3>
										</div>
										<p>Clean and hygienic canteen and Positive Workspace; a great combination, right?</p>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="sub-feature">
										<div class="sub-feature-i">
											<img src="img/career/paid-leaves.svg">
											<h3>Paid Leaves</h3>
										</div>
										<p>Get paid leaves because we <br class="hidden-xs">understand you have social <br class="hidden-xs">responsibilities too</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="bg-lite">
		<div class="sec-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-offset-1 col-md-4 col-sm-6">
						<div class="work">
							<h2>We work hard <br class="hidden-xs">and we have <br class="hidden-xs">fun too…</h2>
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="workc">
							<div class="workcc">
								<h3 class="pb10">Follow us and <br class="hidden-xs">Get Updates on</h3>
								<div class="ft-social">
									<a href="https://www.facebook.com/pythru">
										<img src="img/social-media/facebook-black.svg">
									</a>
									<a href="https://www.linkedin.com/company/pythru">
										<img src="img/social-media/linkedin-black.svg">
									</a>
									<a href="https://www.twitter.com/pythru">
										<img src="img/social-media/twitter-black.svg">
									</a>
									<a href="https://www.youtube.com/c/pythru">
										<img src="img/social-media/youtube-black.svg">
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="bg-black">
		<div class="sec-padding">
			<div class="container">
				<div class="sj">
					<div class="row">					
						<div class="col-md-4 col-sm-12">
							<h2>Search open positions</h2>
						</div>
						<div class="col-md-3  col-sm-5">
							<div class="dropdown">
							    <button class="dropdown-toggle" type="button" data-toggle="dropdown">Select location<i class="bi bi-chevron-down"></i></button>
							    <ul class="dropdown-menu">
							      <li><a href="#">Kolkata</a></li>
							    </ul>
							 </div>
						</div>
						<div class="col-md-3 col-sm-5">
							<div class="dropdown">
							    <button class="dropdown-toggle" type="button" data-toggle="dropdown">Position department<i class="bi bi-chevron-down"></i></button>
							    <ul class="dropdown-menu">
							      <li><a href="#">Data & Engineering</a></li>
							      <li><a href="#">Finance & Legal</a></li>
							      <li><a href="#">Operations</a></li>
							      <li><a href="#">Product, Design & Research</a></li>
							      <li><a href="#">Sales & Marketing</a></li>
							      <li><a href="#">Design</a></li>
							    </ul>
							 </div>
						</div>
						<div class="col-md-2 col-sm-2">
							<div class="go-btn">
								<a href="#">Go</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="hiring-title">
					<h2>We are <span>Hiring</span>… Open Roles…</h2>
					<p>Go for the opportunity, check out the below roles, <br class="hidden-xs">and apply with the suitable one immediately!</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="hiring-box">
						<h2 class="text-center">Empty Section</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="hiring-help">
						<p>Do not see your role on the list? Apply anyway. Send your resume to <a href="mailto:jobs@pythru.com">jobs@pythru.com</a> <br class="hidden-xs hidden-sm">We will get back to you.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding"></section>


	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
	<script type="text/javascript">
	    $(function() {
			var current_pos;
			
			$('#carousel').owlCarousel({
				items: 1,
				responsiveClass: true,
				mouseDrag: false,
				touchDrag: false,
				autoplay: false,
				autoplayTimeout: 2000,
				nav: true,
				navText: ['', ''],
				dots: true,
				onInitialized: function(e) {
					current_pos = e.item.index;				
				},
				onTranslate: function(e) {
					var direction = e.item.index > current_pos? 1 : 0,
							indicator_c = direction? 'next' : 'prev',
							$dots = $(e.currentTarget).find('.owl-dots'),
							$current_dot = $dots.children().eq(current_pos);
								
					$current_dot.html('<div class="dot-indicator '+ indicator_c +'">');
					
					current_pos = e.item.index;
					
					setTimeout(function() {
						$current_dot.html('');
					}, 200);	
				}
			});
		});
    </script>	
</body>
</html>