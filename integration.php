<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Payment Gateway Integration |  Web, Mobile, eCommerce platforms</title>	
	<meta name="description" content="Easy and quick Payment Gateway Integration with developer-friendly APIs and plugins & SDKs supporting all major platforms for Magento, Wordpress, Java, PHP, Python, Android, iOS, Ruby, .NET, etc.">
	<meta name="keywords" content="online payment gateway integration, payment gateway integration, payment gateway integration services, API, web integration, eCommerce integration, mobile integration">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/integration.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="sb-first-bg-img">
		<div class="ab-first-bg ab-first-height">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="ab-first-text text-center">						
							<h1>Integration</h1>
						</div>	
						<div class="ab-first-img">
							<img src="img/integration/hero.svg" class="img-responsive">
						</div>				
					</div>
				</div>				
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="ig-title">
					<h2><span class="sp1">Web</span> Integration</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="ig">
						<div class="ig-content">
							<div class="ig-img">
								<img src="img/integration/java.svg">
							</div>
							<h3>Java</h3>
						</div>
						<div class="ig-link">
							<a href="" class="lg-mar ig-link-btn">Docs</a>
							<a href="" class="lg-mar1 ig-link-btn">Download</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="ig">
						<div class="ig-content">
							<div class="ig-img">
								<img src="img/integration/node-js.svg">
							</div>
							<h3>Node.js</h3>
						</div>
						<div class="ig-link">
							<a href="" class="lg-mar ig-link-btn">Docs</a>
							<a href="" class="lg-mar1 ig-link-btn">Download</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="ig">
						<div class="ig-content">
							<div class="ig-img">
								<img src="img/integration/php.svg">
							</div>
							<h3>PHP</h3>
						</div>
						<div class="ig-link">
							<a href="" class="lg-mar ig-link-btn">Docs</a>
							<a href="" class="lg-mar1 ig-link-btn">Download</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="ig">
						<div class="ig-content">
							<div class="ig-img">
								<img src="img/integration/python.svg">
							</div>
							<h3>Python</h3>
						</div>
						<div class="ig-link">
							<a href="" class="lg-mar ig-link-btn">Docs</a>
							<a href="" class="lg-mar1 ig-link-btn">Download</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="ig">
						<div class="ig-content">
							<div class="ig-img">
								<img src="img/integration/dotnet.svg">
							</div>
							<h3>ASP.Net</h3>
						</div>
						<div class="ig-link">
							<a href="" class="lg-mar ig-link-btn">Docs</a>
							<a href="" class="lg-mar1 ig-link-btn">Download</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="ig">
						<div class="ig-content">
							<div class="ig-img">
								<img src="img/integration/ruby-rails.svg">
							</div>
							<h3>Ruby on Rails</h3>
						</div>
						<div class="ig-link">
							<a href="" class="lg-mar ig-link-btn">Docs</a>
							<a href="" class="lg-mar1 ig-link-btn">Download</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="ig">
						<div class="ig-content">
							<div class="ig-img">
								<img src="img/integration/perl.svg">
							</div>
							<h3>Perl</h3>
						</div>
						<div class="ig-link">
							<a href="" class="lg-mar ig-link-btn">Docs</a>
							<a href="" class="lg-mar1 ig-link-btn">Download</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="ig">
						<div class="ig-content">
							<div class="ig-img">
								<img src="img/integration/go.svg">
							</div>
							<h3>Go</h3>
						</div>
						<div class="ig-link">
							<a href="" class="lg-mar ig-link-btn">Docs</a>
							<a href="" class="lg-mar1 ig-link-btn">Download</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="ig-bg">
		<div class="sec-padding">
			<div class="container">
				<div class="row">
					<div class="ig-title">
						<h2><span class="sp2">Mobile</span> Integration</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 col-sm-6">
						<div class="ig">
							<div class="ig-content">
								<div class="ig-img">
									<img src="img/integration/android.svg">
								</div>
								<h3>Android SDK</h3>
							</div>
							<div class="ig-link">
								<a href="" class="lg-mar ig-link-btn">Docs</a>
								<a href="" class="lg-mar1 ig-link-btn">Download</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="ig">
							<div class="ig-content">
								<div class="ig-img">
									<img src="img/integration/ios.svg">
								</div>
								<h3>iOS SDk</h3>
							</div>
							<div class="ig-link">
								<a href="" class="lg-mar ig-link-btn">Docs</a>
								<a href="" class="lg-mar1 ig-link-btn">Download</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="ig">
							<div class="ig-content">
								<div class="ig-img">
									<img src="img/integration/reactjs.svg">
								</div>
								<h3>React Native SDK</h3>
							</div>
							<div class="ig-link">
								<a href="" class="lg-mar ig-link-btn">Docs</a>
								<a href="" class="lg-mar1 ig-link-btn">Download</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="ig">
							<div class="ig-content">
								<div class="ig-img">
									<img src="img/integration/flutter.svg">
								</div>
								<h3>Flutter SDK</h3>
							</div>
							<div class="ig-link">
								<a href="" class="lg-mar ig-link-btn">Docs</a>
								<a href="" class="lg-mar1 ig-link-btn">Download</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="ig">
							<div class="ig-content">
								<div class="ig-img">
									<img src="img/integration/cordova.svg">
								</div>
								<h3>Cordova SDk</h3>
							</div>
							<div class="ig-link">
								<a href="" class="lg-mar ig-link-btn">Docs</a>
								<a href="" class="lg-mar1 ig-link-btn">Download</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="ig">
							<div class="ig-content">
								<div class="ig-img">
									<img src="img/integration/xamarin.svg">
								</div>
								<h3>Xamarin SDK</h3>
							</div>
							<div class="ig-link">
								<a href="" class="lg-mar ig-link-btn">Docs</a>
								<a href="" class="lg-mar1 ig-link-btn">Download</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="ig-title">
					<h2><span class="sp1">E-commerce</span> Plugins</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="ig">
						<div class="ig-content">
							<div class="ig-img">
								<img src="img/integration/shopify.svg">
							</div>
							<h3>Shopify</h3>
						</div>
						<div class="ig-link">
							<a href="" class="lg-mar ig-link-btn">Docs</a>
							<a href="" class="lg-mar1 ig-link-btn">Download</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="ig">
						<div class="ig-content">
							<div class="ig-img">
								<img src="img/integration/woocommerce.svg">
							</div>
							<h3>Woo-Commerce</h3>
						</div>
						<div class="ig-link">
							<a href="" class="lg-mar ig-link-btn">Docs</a>
							<a href="" class="lg-mar1 ig-link-btn">Download</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="ig">
						<div class="ig-content">
							<div class="ig-img">
								<img src="img/integration/magento.svg">
							</div>
							<h3>Magento</h3>
						</div>
						<div class="ig-link">
							<a href="" class="lg-mar ig-link-btn">Docs</a>
							<a href="" class="lg-mar1 ig-link-btn">Download</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="ig">
						<div class="ig-content">
							<div class="ig-img">
								<img src="img/integration/opencart.svg">
							</div>
							<h3>Opencart</h3>
						</div>
						<div class="ig-link">
							<a href="" class="lg-mar ig-link-btn">Docs</a>
							<a href="" class="lg-mar1 ig-link-btn">Download</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="ig">
						<div class="ig-content">
							<div class="ig-img">
								<img src="img/integration/prestashop.svg">
							</div>
							<h3>PrestaShop</h3>
						</div>
						<div class="ig-link">
							<a href="" class="lg-mar ig-link-btn">Docs</a>
							<a href="" class="lg-mar1 ig-link-btn">Download</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="ig">
						<div class="ig-content">
							<div class="ig-img">
								<img src="img/integration/whmcs.svg">
							</div>
							<h3>WHMCS</h3>
						</div>
						<div class="ig-link">
							<a href="" class="lg-mar ig-link-btn">Docs</a>
							<a href="" class="lg-mar1 ig-link-btn">Download</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="ig">
						<div class="ig-content">
							<div class="ig-img">
								<img src="img/integration/wordpress.svg">
							</div>
							<h3>WordPress</h3>
						</div>
						<div class="ig-link">
							<a href="" class="lg-mar ig-link-btn">Docs</a>
							<a href="" class="lg-mar1 ig-link-btn">Download</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="ig">
						<div class="ig-content">
							<div class="ig-img">
								<img src="img/integration/cs-cart.svg">
							</div>
							<h3>CS-Cart</h3>
						</div>
						<div class="ig-link">
							<a href="" class="lg-mar ig-link-btn">Docs</a>
							<a href="" class="lg-mar1 ig-link-btn">Download</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include 'include/footer.php';?>

	<?php include 'include/js.php';?>
	
</body>
</html>