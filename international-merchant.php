<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'include/meta.php';?>

    <title>PyThru International Payments | With Multiple Currencies Support</title>
    <meta name="description" content="Accept international payments with credit cards and debit cards in 100+ currencies from around the world. Get settlements in INR."/>
    <meta name="keywords" content="PayThru International Payments, International Payment Gateway, Multi-Currency Support, Global payments, International card payments"/>

    <?php include 'include/css.php';?>
    <link rel="stylesheet" href="css/international-merchant.css"/>
</head>
<body>
    <?php include 'include/header.php';?>

    <div class="bg-c">
        <div class="bg">
            <div class="container sec-padding">
                <div class="row sec-padding">
                    <div class="col-md-5">
                        <div class="hero">
                            <div class="content">
                                <h1 class="c-h1">Go <span>International </span>with PyThru Payments</h1>
                                <p class="pt10 pb20 content-p">
                                    Choose PyThru international payment gateway to accept payments <br class="hidden-xs" />
                                    for your services and products in 100+ currencies globally.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="hero-graphic hidden-sm hidden-xs">
                            <img src="img/international/hero.svg" class="img-responsive wc" />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="why-us why-us-pad">
                            <div class="col-md-6">
                                <div class="w-content">
                                    <h3>Why PyThru International?</h3>
                                    <p>Accept hassle-free international payments at affordable rates. Reach millions of customers with PyThru and sell your Indian business products and services all over the world.</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="w-btn">
                                    <a href="">
                                        <span>Sign up</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="container sec-padding">
            <div class="row mpt30p">
                <div class="text-center">
                    <h2 class="c-h2 pb10">Taking your business Global with us</h2>
                    <p class="ex-p pb10">
                        Allow your customers to make payments in their own currency <br class="hidden-xs" />
                        and provide the best customer experience.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="why-use">
                        <img src="img/international/multi-currency-support.svg" />
                        <h3>Multi-currency support</h3>
                        <p>100+ Currencies and all major countries supported to accept international payments from your customers</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="why-use">
                        <img src="img/international/International-options.svg" />
                        <h3>International options</h3>
                        <p>Multiple international payment options like American Express, MasterCard, Visa, other international cards</p>
                    </div>
                </div>
                <div class="clearfix visible-sm"></div>
                <div class="col-md-4 col-sm-6">
                    <div class="why-use">
                        <img src="img/international/easy-international-activation.svg" />
                        <h3>Easy International Activation</h3>
                        <p>We provide an easy onboarding process and the activation of the international services on your merchant account</p>
                    </div>
                </div>
                <div class="col-md-offset-2 col-md-4 col-sm-6">
                    <div class="why-use">
                        <img src="img/international/currency-conversion.svg" />
                        <h3>Currency Conversion</h3>
                        <p>Real-time currency conversion on the same day of the transaction. The foreign currency is converted to INR in real-time.</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="why-use">
                        <img src="img/international/smooth-checkout.svg" />
                        <h3>Smooth Checkout</h3>
                        <p>Let your customers buy in their home currency with a smooth checkout, customized as per your business brand</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="sec-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="lrs-img">
                        <img src="img/international/features.svg" class="img-responsive" />
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 mpt30p">
                    <div class="lrs-pad">
                        <div>
                            <span class="com-span">Affordable</span>
                            <h2 class="c-h2">
                                Expand your business <br class="hidden-xs" />
                                internationally
                            </h2>
                            <p class="com-p pt10">
                                Customize the PyThru products as per your business <br class="hidden-xs" />
                                needs and combine them with our extensive products <br class="hidden-xs" />
                                suite. Go global with PyThru International Payments.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <div class="points">
                                <h4>Affordable Pricing</h4>
                                <h4>Zero Other Charges</h4>
                                <h4>Easy Integration</h4>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="points mpt0">
                                <h4>Fast Settlements</h4>
                                <h4>Real-time Analytics</h4>
                                <h4>Excellent Support</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sec-top-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-5 set-pad mmpt0">
                    <div>
                        <span class="com-span">Industries</span>
                        <h2 class="c-h2">
                            We support <br class="hidden-xs" />
                            Industries like...
                        </h2>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-6">
                            <div class="bp">
                                <h3>SaaS</h3>
                                <h3>eCommerce</h3>
                                <h3>BFSI</h3>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <div class="bp">
                                <h3>Education</h3>
                                <h3>Entertainment</h3>
                                <h3>Gaming</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-6">
                    <div class="set-pad">
                        <div class="set-box">
                            <div class="text-center">
                                <h2 class="c-h2">Settlements in Indian Rupees</h2>
                                <p class="com-p pt10">Accept the payments from all major supported countries in 100+ foreign currency. Get your settlements in INR</p>
                                <div class="set-img">
                                    <img src="img/international/settlement.svg" class="img-responsive" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sec-bottom-padding">
        <div class="bg11">
            <div class="container">
                <div class="row sec-padding sec-c-margin">
                    <div class="col-md-5">
                        <div class="tc">
                            <h2>
                                <span>Get in touch</span> with <br class="hidden-xs" />
                                our team
                            </h2>
                        </div>
                    </div>
                    <div class="col-md-7 pt30">
                        <div class="col-md-6">
                            <div class="tc-input">
                                <input type="email" name="email" placeholder="Enter email" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="tc-btn">
                                <a href="">Send mail</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="accordion-section clearfix mt-3 sec-padding" aria-label="Question Accordions">
        <div class="container">
            <div class="col-md-offset-1 col-md-10">
                <h2 class="text-center faq-title">FAQs</h2>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading p-3 mb-3" role="tab" id="heading0">
                            <h3 class="panel-title">
                                <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
                                    What is the process to start the international services?
                                </a>
                            </h3>
                        </div>
                        <div id="collapse0" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading0" aria-expanded="true">
                            <div class="panel-body px-3 mb-4">
                                <p>You may talk with our Sales team <a href="contact">here </a> in order to discuss your business requirements. In general, international payments can be started after an easy onboarding process:</p>
                                <ol>
                                    <li>The merchant submits required documents</li>
                                    <li>A verification process is done by PyThru</li>
                                    <li>Sent for approval from the bankside</li>
                                </ol>
                                <p>If you already have an activated account, you can go to Settings and click on Enable International Request.</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading p-3 mb-3" role="tab" id="heading1">
                            <h3 class="panel-title">
                                <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                    What payment methods and currencies are supported for International payments <br class="hidden-xs" />
                                    at PyThru?
                                </a>
                            </h3>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                            <div class="panel-body px-3 mb-4">
                                <p>PyThru supports all major international credit and debit cards. It supports 100+ currencies including the Indian rupee, US Dollars, Euros, etc. See the list here.</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading p-3 mb-3" role="tab" id="heading2">
                            <h3 class="panel-title">
                                <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                    How is the international payment settled? What about currency conversion?
                                </a>
                            </h3>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                            <div class="panel-body px-3 mb-4">
                                <p>The currency conversion is done by PyThru and the settlements are made in Indian rupees. The exchange rate is the same as the rate on the transaction date.</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading p-3 mb-3" role="tab" id="heading3">
                            <h3 class="panel-title">
                                <a class="collapsed" role="button" title="" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                    What is the PyThru fee for international services?
                                </a>
                            </h3>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                            <div class="panel-body px-3 mb-4">
                                <p>There is a per-transaction charge (TDR) of flat 3% plus GST. There are no other charges apart from that.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sec-padding"></section>

    <?php include 'include/footer.php';?>

    <?php include 'include/js.php';?>
</body>
</html>
