<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/meta.php';?> 

	<title>Open Virtual Accounts Online with PyThru</title>	
	<meta name="description" content="Create Virtual Accounts and Virtual UPI IDs for automatic reconciliation of Business Payments and to track your customer payments">
	<meta name="keywords" content="Virtual Account, Virtual UPI IDs, automatic reconciliation">

	<?php include 'include/css.php';?> 
	<link rel="stylesheet" href="css/banking.css">
</head>
<body>

	<?php include 'include/header.php';?>

	<section class="main-bg">
		<div class="sec-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="hero sec-padding">
							<div class="content">
								<h1 class="c-h1">A Smart <span>Virtual Account </span>with Pythru</h1>
								<p class="pt10">Manage payments, expenses, banking, and accounting <br class="hidden-xs">reconciliation with the virtual accounts and virtual UPI IDs</p>
							</div> 
						</div>
					</div>
					<div class="col-md-6">
						<div class="hero-graphic hidden-sm hidden-xs">
							<img src="img/virtual-account/hero.png" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<div class="km m-tc">
						<h2>Enable your Business Growth with Our<br class="hidden-xs"> Multi-featured Virtual Account</h2>
						<p class="pt5">Seamless acceptance of the customer payments and the <br class="hidden-xs">Automatic Reconciliation with virtual accounts or UPI</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-offset-1 col-md-10">
					<div class="col-md-6">
						<div class="gf">
							<div class="gfi">
								<img src="img/virtual-account/free-account.svg">
							</div>
							<div class="gfc">
								<h3>Create Free Accounts</h3>
								<p>Create and assign Virtual accounts and UPI IDs to every customer in your Brand name at no cost</p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="gf">
							<div class="gfi">
								<img src="img/virtual-account/payment-modes.svg">
							</div>
							<div class="gfc">
								<h3>Various Payment Modes</h3>
								<p>Payment acceptance through various methods such as NEFT, RTGS, IMPS, and UPI</p>
							</div>
						</div>
					</div>	
					<div class="col-md-6">
						<div class="gf">
							<div class="gfi">
								<img src="img/virtual-account/reconciliation.svg">
							</div>
							<div class="gfc">
								<h3>Automatic Reconciliation</h3>
								<p>Get real-time notifications and does the auto-reconciliation of payments with no errors</p>
							</div>
						</div>
					</div>	
					<div class="col-md-6">
						<div class="gf">
							<div class="gfi">
								<img src="img/virtual-account/verified-accounts.svg">
							</div>
							<div class="gfc">
								<h3>Verified Accounts</h3>
								<p>Enable verified Payment acceptance through the KYC process of the accounts</p>
							</div>
						</div>
					</div>	
				</div>		
			</div>
		</div>
	</section>

	<section class=" sec-padding">      
        <div class="bg-black">
            <div class="container sec-padding">
                <div class="row"> 
                    <div class="col-md-3 col-sm-12">
                        <div class="s3c">
                            <img src="img/payroll/dot-frame.svg" class="hidden-xs hidden-sm">
                            <h2 class="m-tc">Get Started!</h2>
                        </div>
                    </div>  
                    <div class="col-md-9">
                        <div class="col-md-4 col-sm-4">
                            <div class="cmgs">
                            	<div class="cmgsi">
                            		<img src="img/virtual-account/sign-up.svg">
                            		<span></span>
                            		<h3>Step 1</h3>
                            	</div>
                            	<div class="cmgsc1">
                            		<h3>Get Registered</h3>
                            		<p>Sign up and share the basic business details and documents</p>
                            	</div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="cmgs">
                            	<div class="cmgsi">
                            		<img src="img/virtual-account/kyc.svg">
                            		<span></span>
                            		<h3>Step 2</h3>
                            	</div>
                            	<div class="cmgsc1">
                            		<h3>Complete KYC</h3>
                            		<p>Get the KYC done and start transacting with your online bank account with Pythru</p>
                            	</div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="cmgs">
                            	<div class="cmgsi">
                            		<img src="img/virtual-account/get-started.svg" class="gsci">
                            		<span></span>
                            		<h3>Step 3</h3>
                            	</div>
                            	<div class="cmgsc1">
                            		<h3>Get Started</h3>
                            		<p>Start banking with the Virtual Accounts and manage the payments, reconciliation, and much more</p>
                            	</div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>           
    </section>

    <section class="sec-padding">
		<div class="container">
			<div class="row">
				<div class="text-center">
					<div class="km m-tc">
						<h2 class="pt0 mt0">Designed to give your business <br class="hidden-xs">a smooth banking experience</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="tpc">
						<div class="tpc-img tpc-img11">
							<img src="img/virtual-account/customize.svg">
							<h3>Customize</h3>
						</div>
						<p>Create and Customise the virtual account and provide them with your brand name</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="tpc">
						<div class="tpc-img tpc-img11">
							<img src="img/virtual-account/robust-api.svg">
							<h3>Robust APIs</h3>
						</div>
						<p>Developer-friendly APIs for automation, creation, assigning, deactivation of the accounts, refund.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="tpc">
						<div class="tpc-img tpc-img11">
							<img src="img/virtual-account/pricing.svg">
							<h3>Pricing</h3>
						</div>
						<p>Affordable pricing and charged only when the transaction takes place</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="sec-padding">
    	<div class="bg-blue">
    		<div class="container">
    			<div class="row">
    				<div class="cu-main">
    					<div class="col-md-5">
    						<div class="cu-main-text">
    							<h2>Trust Pythru Banking for your business finances</h2>
    							<a href="#">Get Started <i class="bi bi-arrow-right"></i></a>
    						</div>
    					</div>
    					<div class="col-md-7">
    						<div class="cu-img hidden-xs">
    							<img src="img/payroll/image 37.svg" class="img-responsive">
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>

    <section class="sec-padding">
    	<div class="container">
    		<div class="row">
    			<div class="text-center km mb0">
					<h2 class="pt0">Why choose Pythru for your<br class="hidden-xs">  Virtual Accounts?</h2>
				</div>
    		</div>
    		<div class="row">
    			<div class="col-md-offset-1 col-md-10">
					<div class="tick">
						<div class="col-md-4 col-sm-6">
							<h4>Powerful APIs</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>For all business needs</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Easy Dashboard</h4>
						</div>
						<div class="col-md-offset-2 col-md-4 col-sm-6">
							<h4>Secure Infrastructure</h4>
						</div>
						<div class="col-md-4 col-sm-6">
							<h4>Modern Technology</h4>
						</div>
					</div>
				</div>
			</div>
    	</div>
    </section> 

	<?php include 'include/footer.php';?> 

	<?php include 'include/js.php';?>
	
</body>
</html>